﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using Dymo;
using System.Runtime.InteropServices;

namespace V3ProductionTest
{
    public enum LabelPrintReturnCodes
    {
        NO_ERROR = 0,
        LABEL_NOT_CREATED,
    }

    public class failureLabel
    {
        private string _testID;

        public string TestID
        {
            get { return _testID; }
            set { _testID = value; }
        }
        private string _testResult;

        public string TestResult
        {
            get { return _testResult; }
            set { _testResult = value; }
        }
        private string _testUL;

        public string TestUL
        {
            get { return _testUL; }
            set { _testUL = value; }
        }
        private string _testLL;

        public string TestLL
        {
            get { return _testLL; }
            set { _testLL = value; }
        }
        private string _jigIdent;

        public string JigIdent
        {
            get { return _jigIdent; }
            set { _jigIdent = value; }
        }

        private string _unitID;

        public string UnitID
        {
            get { return _unitID; }
            set { _unitID = value; }
        }

        public failureLabel()
        {
            _testID = "";
            _testResult = "";
            _testUL = "";
            _testLL = "";
            _jigIdent = "";
            _unitID = "Not Read";
        }
        public failureLabel(string testId, string testResult, string testUL, string testLL, string jigIdent, string unitID)
        {
            _testID = testId;
            _testResult = testResult;
            _testUL = testUL;
            _testLL = testLL;
            _jigIdent = jigIdent;
            _unitID = unitID;
        }


        public LabelPrintReturnCodes Print()
        {
            try
            {
                DymoAddInClass _dymoAddin = new DymoAddInClass();
                DymoLabelsClass _dymoLabel = new DymoLabelsClass();

                // open the to-do-list label we created earlier
                if (_dymoAddin.Open(Application.StartupPath + @"\failure.label"))
                {
                    // this call returns a list of objects on the label
                    string objNames = _dymoLabel.GetObjectNames(false);

                    _dymoLabel.SetField("testId", "TID: " + _testID);
                    _dymoLabel.SetField("testValue", "Res: " + _testResult);
                    _dymoLabel.SetField("testUL", "UL: " + _testUL);
                    _dymoLabel.SetField("testLL", "LL: " + _testLL);
                    _dymoLabel.SetField("jigId", "" + _jigIdent);
                    _dymoLabel.SetField("unitId", "PcbID:" + _unitID);
                    // let's print to the first LabelWriter available
                    // on the pc, if the printer is a TwiTurbo, print
                    // to the left tray
                    string[] printers = _dymoAddin.GetDymoPrinters().Split(new char[] { '|' });
                    if (printers.GetLength(0) > 0)
                    {
                        if (_dymoAddin.SelectPrinter(printers[0]))
                        {
                            if (_dymoAddin.IsTwinTurboPrinter(printers[0]))
                            {
                                _dymoAddin.Print2(1, false, 0);
                            }
                            else
                            {
                                _dymoAddin.Print(1, false);
                            }
                        }
                    }


                }
                return LabelPrintReturnCodes.NO_ERROR;
            }
            catch (Exception err)
            {
                return LabelPrintReturnCodes.LABEL_NOT_CREATED;
            }

        }

    }

    public class voidLabel
    {
        private string _softwareVersion;

        public string SoftwareVersion
        {
            get { return _softwareVersion; }
            set { _softwareVersion = value; }
        }
        private string _comment;

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }


        public voidLabel()
        {
            _softwareVersion = "";
            _comment = "";

        }
        public voidLabel(string softwareVersion, string comment)
        {
            _softwareVersion = softwareVersion;
            _comment = comment;

        }


        public LabelPrintReturnCodes Print()
        {
            try
            {
                DymoAddInClass _dymoAddin = new DymoAddInClass();
                DymoLabelsClass _dymoLabel = new DymoLabelsClass();

                // open the to-do-list label we created earlier
                if (_dymoAddin.Open(Application.StartupPath + @"\void.label"))
                {
                    // this call returns a list of objects on the label
                    string objNames = _dymoLabel.GetObjectNames(false);

                    _dymoLabel.SetField("software", _softwareVersion);
                    _dymoLabel.SetField("comment", _comment);

                    // let's print to the first LabelWriter available
                    // on the pc, if the printer is a TwiTurbo, print
                    // to the left tray
                    string[] printers = _dymoAddin.GetDymoPrinters().Split(new char[] { '|' });
                    if (printers.GetLength(0) > 0)
                    {
                        if (_dymoAddin.SelectPrinter(printers[0]))
                        {
                            if (_dymoAddin.IsTwinTurboPrinter(printers[0]))
                            {
                                _dymoAddin.Print2(1, false, 0);
                            }
                            else
                            {
                                _dymoAddin.Print(1, false);
                            }
                        }
                    }


                }
                return LabelPrintReturnCodes.NO_ERROR;
            }
            catch (Exception err)
            {
                return LabelPrintReturnCodes.LABEL_NOT_CREATED;
            }

        }

    }

    public class voidLabelZebra
    {
        private string _softwareVersion;

        public string SoftwareVersion
        {
            get { return _softwareVersion; }
            set { _softwareVersion = value; }
        }
        private string _comment;

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private string _stockCode;

        public string StockCode
        {
            get { return _stockCode; }
            set { _stockCode = value; }
        }


        public voidLabelZebra()
        {
            _softwareVersion = "";
            _comment = "";
            _stockCode = "3000000000";

        }
        public voidLabelZebra(string softwareVersion, string comment)
        {
            _softwareVersion = softwareVersion;
            _comment = comment;
            _stockCode = "3000000000";
        }
        public voidLabelZebra(string softwareVersion, string comment, string stock)
        {
            _softwareVersion = softwareVersion;
            _comment = comment;
            _stockCode = stock;
        }

        /// <summary>
        /// Loads the graphics binary data.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private bool loadGraphicsBinaryData(BinaryWriter w, string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);

            try
            {
                // Read data from Test.data.
                for (int i = 0; i < r.BaseStream.Length; i++)
                {
                    w.Write((byte)r.ReadByte());
                }
                w.Write((byte)0x0D);
                w.Write((byte)0x0A);
                r.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="data">The data.</param>
        private void writeLine(BinaryWriter w, string data)
        {
            int count;
            char[] c;

            c = data.ToCharArray();

            for (count = 0; count < c.GetLength(0); count++)
            {
                w.Write((byte)Convert.ToByte(c[count]));
            }
            w.Write((byte)0x0D);
            w.Write((byte)0x0A);
        }

        public LabelPrintReturnCodes Print(bool stockCode)
        {
            try
            {
                string printerName = "ZDesigner GC420T (EPL)";

                FileStream fileStream = new FileStream("zebraVoidTemp.lab", FileMode.Create);
                BinaryWriter binaryWriter = new BinaryWriter(fileStream);


                //loadGraphicsBinaryData(binaryWriter, "remote.prn");
                if (stockCode == false)
                {
                    loadGraphicsBinaryData(binaryWriter, "zebraVoid.prn");

                    this.writeLine(binaryWriter, @"A19,62,0,4,1,1,N,""" + _comment + @"""");
                    this.writeLine(binaryWriter, @"A20,87,0,4,1,1,N,""" + _softwareVersion + @"""");
                    this.writeLine(binaryWriter, @"A94,123,0,2,1,1,N,""" + DateTime.Now.ToString("dd/MM/yyyy") + @"""");
                    this.writeLine(binaryWriter, @"A22,123,0,2,1,1,N,""" + DateTime.Now.ToString("HH:mm") + @"""");
                    this.writeLine(binaryWriter, "P1");
                }
                else
                {
                    loadGraphicsBinaryData(binaryWriter, "zebraVoidStockcode.prn");

                    this.writeLine(binaryWriter, @"B12,48,0,1A,2,4,30,N,""" + _stockCode + @"""");
                    this.writeLine(binaryWriter, @"A225,128,0,2,1,1,N,""" + _comment + @"""");                    
                    this.writeLine(binaryWriter, @"A12,92,0,4,1,1,N,""" + _softwareVersion + @"""");
                    this.writeLine(binaryWriter, @"A84,128,0,2,1,1,N,""" + DateTime.Now.ToString("dd/MM/yyyy") + @"""");
                    this.writeLine(binaryWriter, @"A12,128,0,2,1,1,N,""" + DateTime.Now.ToString("HH:mm") + @"""");
                    this.writeLine(binaryWriter, "P1");
                }
                binaryWriter.Close();
                fileStream.Close();

                //StreamReader st = new StreamReader("zebraVoid.prn");

                //string temp = st.ReadToEnd();
                //temp = temp.Replace("DESC", _comment);
                //temp = temp.Replace("SOFT", _softwareVersion);
                //temp = temp.Replace("DATE", DateTime.Now.ToString("dd/MM/yyyy"));
                //temp = temp.Replace("TIME", DateTime.Now.ToString("HH:mm"));

                //st.Close();
                //StreamWriter sr = new StreamWriter("zebraVoidTemp.lab", false);
                //sr.Write(temp);
                //sr.Flush();
                //sr.Close();

                RawPrinterHelper.SendFileToPrinter(printerName, "zebraVoidTemp.lab");

                return LabelPrintReturnCodes.NO_ERROR;
            }
            catch (Exception err)
            {
                return LabelPrintReturnCodes.LABEL_NOT_CREATED;
            }

        }

    }

    public class RawPrinterHelper
    {
        // Structure and API declarions:
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        // SendBytesToPrinter()
        // When the function is given a printer name and an unmanaged array
        // of bytes, the function sends those bytes to the print queue.
        // Returns true on success, false on failure.
        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Assume failure unless you specifically succeed.

            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Open the printer.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Start a document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Start a page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Write your bytes.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // If you did not succeed, GetLastError may give more information
            // about why not.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            // Open the file.
            FileStream fs = new FileStream(szFileName, FileMode.Open);
            // Create a BinaryReader on the file.
            BinaryReader br = new BinaryReader(fs);
            // Dim an array of bytes big enough to hold the file's contents.
            Byte[] bytes = new Byte[fs.Length];
            bool bSuccess = false;
            // Your unmanaged pointer.
            IntPtr pUnmanagedBytes = new IntPtr(0);
            int nLength;

            nLength = Convert.ToInt32(fs.Length);
            // Read the contents of the file into the array.
            bytes = br.ReadBytes(nLength);
            // Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
            // Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
            // Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
            // Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes);

            br.Close();
            fs.Close();
            return bSuccess;
        }
        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;
            // How many characters are in the string?
            dwCount = szString.Length;
            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }
    }

    class Label
    {
        public delegate void LabelStatusHandler(string text, System.Drawing.Color colour);
        public event LabelStatusHandler StatusChanged;

        /// <summary>
        /// Prints a label with the specified serial number.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="testDate">The test date.</param>
        /// <param name="hardwareVersion">The hardware version.</param>
        /// <param name="operatingSystemVersion">The operating system version.</param>
        /// <param name="delphiYDTNumber">The delphi YDT number.</param>
        /// <returns></returns>
        public LabelPrintReturnCodes Print(string serialNumber, string testDate, string hardwareVersion, string softwareVersion, string partNumber, string phoneNumber, string labelPrnFile, string logPath)
        {
            
            try
            {
                File.Delete(string.Format(@"{0}\{1}.lab", logPath + @"\Label_Files", serialNumber));
                // Create the writer for data.
                FileStream fileStream = new FileStream(string.Format(@"{0}\{1}.lab", logPath + @"\Label_Files", serialNumber), FileMode.CreateNew);
                BinaryWriter binaryWriter = new BinaryWriter(fileStream);


                loadGraphicsBinaryData(binaryWriter, labelPrnFile);

                if (labelPrnFile == "amrV3_5_2019_right.prn")
                {
                    this.writeLine(binaryWriter, "1911A0800760184" + testDate);
                    this.writeLine(binaryWriter, "1911A1401380078" + serialNumber);
                    this.writeLine(binaryWriter, "1a3102201170095" + serialNumber);
                    this.writeLine(binaryWriter, "1911A0800980057" + partNumber);
                    this.writeLine(binaryWriter, "1911A0800760056" + phoneNumber);
                    this.writeLine(binaryWriter, "1911A0800550184" + hardwareVersion);
                    this.writeLine(binaryWriter, "1911A0800550056" + softwareVersion);
                    this.writeLine(binaryWriter, "1Y1100000290010gfx0");
                }
                else if (labelPrnFile == "pfV3_5_2019_right.prn")
                {
                    this.writeLine(binaryWriter, "1911A0800760184" + testDate);
                    this.writeLine(binaryWriter, "1a3102201170095" + serialNumber);
                    this.writeLine(binaryWriter, "1911A1401380079" + serialNumber);
                    this.writeLine(binaryWriter, "1911A0800980058" + partNumber);
                    this.writeLine(binaryWriter, "1911A0800760056" + phoneNumber);
                    this.writeLine(binaryWriter, "1911A0800550185" + hardwareVersion);
                    this.writeLine(binaryWriter, "1911A0800550056" + softwareVersion);
                    this.writeLine(binaryWriter, "1Y1100000290015gfx0");
                }
                

                this.writeLine(binaryWriter, "Q0001");
                this.writeLine(binaryWriter, "E");

                this.write(binaryWriter, "<xpml></page></xpml>");
                this.writeLineSTX(binaryWriter, "xCGgfx0");
                this.writeLineSTX(binaryWriter, "zC");
                this.write(binaryWriter, "<xpml><end/></xpml>");

                binaryWriter.Close();
                fileStream.Close();

                //for (retryLoop = 0; retryLoop < 10; retryLoop++)
                //{
                if (printLabelUsb(serialNumber, logPath) == true)
                {
                    //        reprintLabel = MessageBox.Show("Do you want to reprint the label?", "", MessageBoxButtons.YesNo);
                    //        if (reprintLabel == DialogResult.No)
                    //        {
                    if (StatusChanged != null)
                        StatusChanged("Label Printed", Color.LightGreen);
                    //            break;
                    //        }
                    //        else
                    //        {
                    //            if (StatusChanged != null)
                    //                StatusChanged("Label reprint in progress", Color.Red);
                    //        }
                }
                else
                {
                    if (StatusChanged != null)
                        StatusChanged("Error printing label" + Environment.NewLine + "Automatic retry in progress", Color.Red);
                }
                //}

                return LabelPrintReturnCodes.NO_ERROR;
            }
            catch
            {
                return LabelPrintReturnCodes.LABEL_NOT_CREATED;
            }
        }


        /// <summary>
        /// Prints the label.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns></returns>
        private bool printLabel(string serialNumber, string logPath)
        {
            string path;
            string arg;
            //path = string.Format(@"{0}\{1}.lab", Directory.GetCurrentDirectory() + @"\Label_Files", serialNumber);
            path = string.Format(@"{0}\{1}.lab", logPath + @"\Label_Files", serialNumber);
            arg = string.Format("/c copy \"{0}\" \"{1}\"", path, "LPT1");
            try
            {
                switch (Environment.OSVersion.Platform.ToString())
                {
                    case "Win32NT":
                        Process.Start("cmd", arg);
                        break;

                    default:
                        Process.Start("command", arg);
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                LogFile.WriteError("Label::printLabel(" + serialNumber + ")\nError(" + e.Message);
                return false;
            }

        }

        private bool printLabelUsb(string serialNumber, string logPath)
        {
            string path;
            string arg;
            //path = string.Format(@"{0}\{1}.lab", Directory.GetCurrentDirectory() + @"\Label_Files", serialNumber);
            path = string.Format(@"{0}\{1}.lab", logPath + @"\Label_Files", serialNumber);
            
            try
            {
                //RawPrinterHelper.SendFileToPrinter("Datamax E-4304", path);
                RawPrinterHelper.SendFileToPrinter("Datamax-O'Neil E-4204B Mark III", path); 
                return true;
            }
            catch (Exception e)
            {
                LogFile.WriteError("Label::printLabel(" + serialNumber + ")\nError(" + e.Message);
                return false;
            }

        }
        
        /// <summary>
        /// Loads the graphics binary data.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private bool loadGraphicsBinaryData(BinaryWriter w, string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);

            try
            {
                // Read data from Test.data.
                for (int i = 0; i < r.BaseStream.Length; i++)
                {
                    w.Write((byte)r.ReadByte());
                }
                w.Write((byte)0x0D);
                w.Write((byte)0x0A);
                r.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Writes the data no line feed
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="data">The data.</param>
        private void write(BinaryWriter w, string data)
        {
            int count;
            char[] c;

            c = data.ToCharArray();

            for (count = 0; count < c.GetLength(0); count++)
            {
                w.Write((byte)Convert.ToByte(c[count]));
            }            
        }

        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="data">The data.</param>
        private void writeLine(BinaryWriter w, string data)
        {
            int count;
            char[] c;

            c = data.ToCharArray();

            for (count = 0; count < c.GetLength(0); count++)
            {
                w.Write((byte)Convert.ToByte(c[count]));
            }
            w.Write((byte)0x0D);
            w.Write((byte)0x0A);
        }


        /// <summary>
        /// Writes the line STX.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="data">The data.</param>
        private void writeLineSTX(BinaryWriter w, string data)
        {
            int count;
            char[] c;

            c = data.ToCharArray();

            w.Write((byte)0x02);

            for (count = 0; count < c.GetLength(0); count++)
            {
                w.Write((byte)Convert.ToByte(c[count]));
            }
            w.Write((byte)0x0D);
            w.Write((byte)0x0A);
        }


        /// <summary>
        /// Writes the line SOH.
        /// </summary>
        /// <param name="w">The w.</param>
        /// <param name="data">The data.</param>
        private void writeLineSOH(BinaryWriter w, string data)
        {
            int count;
            char[] c;

            c = data.ToCharArray();

            w.Write((byte)0x01);

            for (count = 0; count < c.GetLength(0); count++)
            {
                w.Write((byte)Convert.ToByte(c[count]));
            }
            w.Write((byte)0x0D);
            w.Write((byte)0x0A);
        }
    }
}
