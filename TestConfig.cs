﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace V3ProductionTest
{
    public class ApnSetting
    {
        public string description;
        public string apn;
        public string user;
        public string password;

        public ApnSetting()
        {
            description = "";
            apn = "";
            user = "";
            password = "";
        }
    }

    public class ApnConfiguration
    {
        public List<ApnSetting> apnList;
    }

    public class TestParameter
    {
        public string description;
        public string value;

        public TestParameter()
        {
            description = "";
            value = "";
        }
    }
    public class TestItem
    {
        public Tests.TestID id;
        public bool runTest;
        public List<TestParameter> testParams;

        public bool getParamValue(string desc, ref string value)
        {
            bool retVal = false;

            foreach (TestParameter tp in testParams)
            {
                if (tp.description == desc)
                {
                    value = tp.value;
                    retVal = true;
                    break;
                }
            }

            return retVal;
        }
    }
    public class TestConfig
    {
        //public string partNumber;
        //public string softwareVersion;
        //public string hardwareVersion;
        public string labelTemplate;
        //public string modemType;
        //public string modemFirmware;
        public ReleaseConfig rc;
        public List<TestItem> testSequence;

        TestConfig()
        {
            rc = new ReleaseConfig();
            labelTemplate = "";
        }
    }

    public class ReleaseConfig
    {
        public bool smsOnly;

        public string smsServerPhoneNumber;
        public int gprsPortNumber;
        public string gprsServerIP;

        public string gprsApn;
        public string gprsNetworkUserID;
        public string gprsNetworkPassword;

        public string partNumber;
        public string hardwareVersion;

        public string firmwareId;//0x0d
        public string bootloaderVersion;//0x18
        public string firmwareFileName;//0x19  
        public string ServerTelNumberSecondary;
        public string GprsPrimaryServer;
        public string GprsSecondaryServer;
        public string GprsEngineeringServer;
        public string GprsAUXServer;
        public int simIdType;
        public int simNumberLength;
        public int phoneNumberLength;
        public bool manualPhoneNum;
        public bool sendTestSms;

        public ReleaseConfig()
        {
            smsOnly = false;
            smsServerPhoneNumber = "";
            gprsPortNumber = 0;
            gprsServerIP = "";

            gprsApn = "";
            gprsNetworkUserID = "";
            gprsNetworkPassword = "";

            partNumber = "";
            hardwareVersion = "";

            firmwareId = "";
            bootloaderVersion = "";
            firmwareFileName = "";

            ServerTelNumberSecondary = "";       
            GprsPrimaryServer = "";
            GprsSecondaryServer = "";
            GprsEngineeringServer = "";
            GprsAUXServer = "";

            simIdType = 14;
            simNumberLength = 20;
            phoneNumberLength = 11;
            manualPhoneNum = false;
            sendTestSms = false;
        }

    }

    public class TestControlVariables
    {
        public TestConfig tc;
        public Tests test;
        public Dictionary<string, string> results;
        public BackgroundWorker bgw;
        public bool _largeMicroOnly;

        public TestControlVariables(TestConfig Tconfig, Tests tests, Dictionary<string, string> res, BackgroundWorker id, bool largeMicroOnly)
        {
            tc = Tconfig;
            test = tests;
            results = res;
            bgw = id;
            _largeMicroOnly = largeMicroOnly;
        }
    }
}
