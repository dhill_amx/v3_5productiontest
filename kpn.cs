﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.jaspersystems.api;
using V3ProductionTest.com.jasperwireless.kpnapi;

namespace V3ProductionTest
{
    public static class kpn
    {
        private static string apiLicence = "18e3f558-8069-49ca-ad63-ee0befbdd0ca";
        private static string apiUser = "APIonly";
        private static string apiPass = "Airmax.1";

        public static string getPhoneNumber(string iccid)
        {
            string phoneNum = "";

            SecurityHeader header = new SecurityHeader();
            header.UsernameToken.SetUserPass(apiUser, apiPass, PasswordOption.SendPlainText);

            TerminalService service = new TerminalService();
            service.securityHeader = header;

            GetTerminalDetailsRequest req = new GetTerminalDetailsRequest();
            req.licenseKey = apiLicence;
            req.version = "1.0";
            req.messageId = "message-1";
            req.iccids = new string[1] { iccid };

            try
            {
                GetTerminalDetailsResponse res = service.GetTerminalDetails(req);
                if (res.terminals.Length > 0)
                {
                    phoneNum = res.terminals[0].msisdn;
                    if (phoneNum.StartsWith("31") == true)
                    {
                        phoneNum = "0" + phoneNum.Substring(2);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return phoneNum;
        }
    }
}
