﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace V3ProductionTest
{
    public enum PsuType
    {
        GwInstek = 0,
        Tenma722545
    }

    class PSUControl
    {
        private SerialPort _sp;
        private PsuType _psu = PsuType.GwInstek;

        public PSUControl(PsuType psu)
        {
            _psu = psu;
        }

        ~PSUControl()
        {
            try
            {                
                _sp.Close();
            }
            catch (Exception err)
            { System.Diagnostics.Debug.WriteLine(err.Message); }
        }

        public void init( int comPort)
        {
            if (_sp == null)
            {
                _sp = new SerialPort();

                try
                {
                    if (_sp.IsOpen == true)
                    {
                        _sp.Close();
                    }

                    switch (_psu)
                    {

                        case PsuType.GwInstek:
                            {
                                _sp.BaudRate = 2400;
                                _sp.Parity = Parity.None;
                                _sp.DataBits = 8;
                                _sp.StopBits = StopBits.One;
                                _sp.RtsEnable = true;
                                _sp.DtrEnable = true;
                                _sp.Handshake = Handshake.None;
                                break;
                            }
                        case PsuType.Tenma722545:
                            {
                                _sp.BaudRate = 9600;
                                _sp.Parity = Parity.None;
                                _sp.DataBits = 8;
                                _sp.StopBits = StopBits.One;
                                _sp.RtsEnable = false;
                                _sp.DtrEnable = false;
                                _sp.Handshake = Handshake.None;
                                break;
                            }
                    }

                    
                    _sp.PortName = "COM" + comPort.ToString();
                    _sp.Open();
                }
                catch (Exception err)
                { System.Diagnostics.Debug.WriteLine(err.Message); }

            }

            //_sp.Write(new byte[] { 0x4C, 0x0D }, 0, 2);

            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        System.Diagnostics.Debug.WriteLine(sendAndReceive("L"));
            
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        break;
                    }
            }
            
            
            System.Threading.Thread.Sleep(250);
            output(false);
            System.Threading.Thread.Sleep(250);
            setOutputVoltageLimit(14);
            System.Threading.Thread.Sleep(1000);
            System.Windows.Forms.Application.DoEvents();
            setOutputCurrentLimit(2.00);
            System.Threading.Thread.Sleep(250);            
            setOutputVoltage(12.5);
            System.Threading.Thread.Sleep(250);

            
        }

        public void output(bool enable)
        {
            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        if (enable == true)
                        {
                            send("KOE");
                        }
                        else
                        {
                            send("KOD");
                        }
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        if (enable == true)
                        {
                            send("OUT1");
                        }
                        else
                        {
                            send("OUT0");
                        }
                        break;
                    }
            }

            
        }

        public void setOutputVoltage(Double volts)
        {
            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        send("SV " + volts.ToString("00.00"));  
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        send("VSET1:" + volts.ToString("00.0"));  
                        break;
                    }
            }
                      
        }

        public void setOutputVoltageLimit(int volts)
        {
            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        send("SU " + volts.ToString("00"));
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        //send("SU " + volts.ToString("00"));
                        break;
                    }
            }
            
        }

        public void setOutputCurrentLimit(double amps)
        {
            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        send("SI " + amps.ToString("0.00"));
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        send("ISET1:" + amps.ToString("0.00"));
                        break;
                    }
            }
            
        }

        public double getOutputVoltage()
        {
            string temp = "";
            double retval = -1.0;

            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        temp = sendAndReceive("V");
                        if (temp.Length == 8 || temp.Length==7)
                        {
                            if (double.TryParse(temp.Substring(1,5), out retval) == true)
                            {
                                
                            }
                            else
                            {
                                retval = -1.0;
                            }
                        }
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        temp = sendAndReceive("VOUT1?");
                        
                        if (double.TryParse(temp, out retval) == true)
                        {

                        }
                        else
                        {
                            retval = -1.0;
                        }
                        
                        break;
                    }
            }


            return retval;
        }

        public double getOutputCurrent()
        {
            string temp = "";
            double retval = -1.0;

            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        temp = sendAndReceive("A");
                        if (temp.Length == 8 || temp.Length == 6)
                        {
                            if (double.TryParse(temp.Substring(1), out retval) == true)
                            {

                            }
                            else
                            {
                                return -1.0;
                            }
                        }
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        temp = sendAndReceive("IOUT1?");
                        
                        if (double.TryParse(temp, out retval) == true)
                        {

                        }
                        else
                        {
                            return -1.0;
                        }
                        
                        break;
                    }
            }

            
            return retval;
        }

        public double getOutputCurrentLimit()
        {
            string temp = "";
            double retval = -1.0;

            switch (_psu)
            {

                case PsuType.GwInstek:
                    {
                        temp = sendAndReceive("I");
                        if (temp.Length == 7)
                        {
                            if (double.TryParse(temp.Substring(1, 4), out retval) == true)
                            {
                                
                            }
                            else
                            {
                                return -1.0;
                            }
                        }
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        temp = sendAndReceive("I");
                        //if (temp.Length == 7)
                        //{
                            if (double.TryParse(temp, out retval) == true)
                            {

                            }
                            else
                            {
                                return -1.0;
                            }
                        //}
                        break;
                    }
            }


            return retval;
        }
        private void send(string msg)
        {
            string temp = msg;

            switch (_psu)
            {
                case PsuType.GwInstek:
                    {
                        temp += Convert.ToChar(0x0D);
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        break;
                    }
            }


            try
            {
                _sp.Write(temp);
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message );
            }

            System.Windows.Forms.Application.DoEvents();
            System.Diagnostics.Debug.WriteLine(temp);
        }

        private string sendAndReceive(string msg)
        {
            string retVal = "";

            string temp = msg;
            
            switch (_psu)
            {
                case PsuType.GwInstek:
                    {
                        temp+=Convert.ToChar(0x0D);
                        try
                        {
                            _sp.Write(temp);

                            System.Diagnostics.Debug.WriteLine(temp);

                            System.Windows.Forms.Application.DoEvents();

                            DateTime dt = DateTime.Now.AddMilliseconds(500);
                            while (dt.CompareTo(DateTime.Now) > 0)
                            {
                                if (_sp.BytesToRead > 0)
                                {
                                    retVal = _sp.ReadLine();
                                    break;
                                }
                                System.Windows.Forms.Application.DoEvents();
                            }

                            System.Diagnostics.Debug.WriteLine(retVal);

                        }
                        catch (Exception err)
                        {
                            System.Diagnostics.Debug.WriteLine(err.Message);
                        }
                        break;
                    }
                case PsuType.Tenma722545:
                    {
                        
                        try
                        {
                            _sp.Write(temp);

                            System.Diagnostics.Debug.WriteLine(temp);

                            System.Windows.Forms.Application.DoEvents();

                            DateTime dt = DateTime.Now.AddMilliseconds(500);
                            while (dt.CompareTo(DateTime.Now) > 0)
                            {
                                if (_sp.BytesToRead > 0)
                                {
                                    retVal += _sp.ReadExisting();
                                    break;
                                }
                                System.Windows.Forms.Application.DoEvents();
                            }

                            System.Diagnostics.Debug.WriteLine(retVal);

                        }
                        catch (Exception err)
                        {
                            System.Diagnostics.Debug.WriteLine(err.Message);
                        }
                        break;
                    }
            }
            
            
            return retVal;
        }
    }
}

