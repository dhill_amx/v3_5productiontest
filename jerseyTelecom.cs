﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V3ProductionTest.dk.neoconsult.prod;
using System.Net;

namespace V3ProductionTest
{
    class jerseyTelecomInterface
    {
        private static string server = "https://prod.neoconsult.dk";
        private static string apiUser = "JW@JTM2M_GB";
        private static string apiPass = "wewu3seby7";

        public static string getPhoneNumber(string simNumber)
        {
            string phoneNum = "";

            NextM2MServiceService n = new NextM2MServiceService();
            n.Credentials = new NetworkCredential(apiUser, apiPass);
            n.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
            var servicePoint = ServicePointManager.FindServicePoint(new Uri(server));
            servicePoint.Expect100Continue = false;

            int page = 1;
            sim[] simList = null;
            do
            {
                simList = n.getSims(page++, 100);
                if (simList == null) break;
                for (int i = 0; i < simList.GetUpperBound(0); i++)
                {
                    if (simList[i].iccId == simNumber)
                    {
                        phoneNum = simList[i].msisdn;
                        break;
                    }
                }
            } while (simList != null);

            if(phoneNum.StartsWith("44")) phoneNum = "0" + phoneNum.Substring(2);
            return phoneNum;
        }

        public static string getPhoneNumberLookup(string imsi)
        {
            string phoneNum = "";

            NextM2MServiceService n = new NextM2MServiceService();
            n.Credentials = new NetworkCredential(apiUser, apiPass);
            n.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
            var servicePoint = ServicePointManager.FindServicePoint(new Uri(server));
            servicePoint.Expect100Continue = false;


            sim simList = null;

            simList = n.getSim(imsi);

            if (simList != null)
            {
                if (simList.imsi == imsi)
                {
                    phoneNum = simList.msisdn;
                }
            }

            if (phoneNum.StartsWith("44")) phoneNum = "0" + phoneNum.Substring(2);
            return phoneNum;
        }
    }
}
