﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace V3ProductionTest
{
    public class RelayBoard
    {
        private SerialPort _sp;
        private int _comPort = 9;

        public enum relayStates
        {
            Relay1_ON = 0x65,
            Relay1_OFF = 0x6F,
            Relay2_ON = 0x66,
            Relay2_OFF = 0x70,
            Relay3_ON = 0x67,
            Relay3_OFF = 0x71,
            Relay4_ON = 0x68,
            Relay4_OFF = 0x72,
            Relay5_ON = 0x69,
            Relay5_OFF = 0x73,
            Relay6_ON = 0x6A,
            Relay6_OFF = 0x74,
            Relay7_ON = 0x6B,
            Relay7_OFF = 0x75,
            Relay8_ON = 0x6C,
            Relay8_OFF = 0x76,
            RelayAll_OFF = 0x6E
        }

        public RelayBoard()
        {
            _sp = new SerialPort();
            
        }

        ~RelayBoard()
        {
            try
            {
                if (_sp.IsOpen)
                {
                    _sp.Close();
                }
                _sp = null;
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        public bool connect(int comport)
        {
            bool retval = false;
            try
            {
                _comPort = comport;
                if (_sp.IsOpen == false)
                {
                    _sp.BaudRate = 19200;
                    _sp.Parity = Parity.None;
                    _sp.StopBits = StopBits.Two;
                    _sp.DataBits = 8;
                    _sp.ReadTimeout = 50;
                    _sp.WriteTimeout = 50;
                    _sp.PortName = "COM" + comport.ToString();
                    _sp.Open();
                }
                if (_sp.IsOpen == true)
                {
                    setRelayState(relayStates.RelayAll_OFF);
                    retval = true;
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Please Power Down the test equipment and restart the program - Unable to communicate with Relay Card", "", System.Windows.Forms.MessageBoxButtons.OK);
                retval = false;
            }
            return retval;
        }

        public void setRelayState(relayStates rs)
        {
            byte[] data = {(byte)rs};

            try
            {
                _sp.Write(data,0,1);
                _sp.DiscardInBuffer();
                Thread.Sleep(1000);                
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                if (_sp.IsOpen)
                {
                    _sp.Close();
                }
                _sp = null;
                _sp = new SerialPort();
                
                _sp.BaudRate = 19200;
                _sp.Parity = Parity.None;
                _sp.StopBits = StopBits.Two;
                _sp.DataBits = 8;
                _sp.ReadTimeout = 50;
                _sp.WriteTimeout = 100;
                _sp.PortName = "COM" + _comPort.ToString();
                _sp.Open();

                _sp.Write(data, 0, 1);
                _sp.DiscardInBuffer();
                Thread.Sleep(1000);
            }

        }
        
    }
}
