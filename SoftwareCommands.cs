﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CANL2dotNET;
using System.IO;

namespace V3ProductionTest
{   
    public class SoftwareCommands
    {
        public CanInterfaceSofting _can;
        public uint targetIdent = 0x7E0;
        public uint responseIdent = 0x7F1;
        PARAM_CLASS canMsg = new PARAM_CLASS();

        private int _canChannel = 1;

        public byte[] response1;

        public SoftwareCommands(CanInterfaceSofting canInterface, int canChannel)
        {
            _can = canInterface;
            _canChannel = canChannel;
        }

        public int readBatteryVoltageAD(ref int adValue)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0400");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0400");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x04)
                {
                    retVal = 1;
                    adValue = response1[3];
                    adValue *= 0x100;
                    adValue += response1[2];
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readDate(ref DateTime rtc)
        {
            int response, retVal, year;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0600");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0600");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x06)
                {
                    retVal = 1;
                    year = response1[4];
                    year *= 0x100;
                    year += response1[5];
                    rtc = new DateTime(year, response1[3], response1[2]);
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeDate(DateTime rtc)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0602" + int.Parse(rtc.Year.ToString().Substring(0, 2)).ToString("X2") + int.Parse(rtc.Year.ToString().Substring(2)).ToString("X2") + rtc.Month.ToString("X2") + rtc.Day.ToString("X2"));
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0602" + int.Parse(rtc.Year.ToString().Substring(0, 2)).ToString("X2") + int.Parse(rtc.Year.ToString().Substring(2)).ToString("X2") + rtc.Month.ToString("X2") + rtc.Day.ToString("X2"));
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x06)
                {

                    try
                    {
                        if (response1[0] == 0x06 && response1[1] == 0x02 && response1[2] == 0x80)
                        {
                            retVal = 1;
                        }
                        else
                        {
                            retVal = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readTime(ref DateTime rtc)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0601");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0601");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x06 && response1[0] == 0x06)
                {

                    try
                    {
                        rtc = new DateTime(01, 01, 01, int.Parse(response1[2].ToString("X2")), int.Parse(response1[3].ToString("X2")), int.Parse(response1[4].ToString("X2")));

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int writeTime(DateTime rtc)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0603" + rtc.Hour.ToString("X2") + rtc.Minute.ToString("X2") + rtc.Second.ToString("X2"));
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0603" + rtc.Hour.ToString("X2") + rtc.Minute.ToString("X2") + rtc.Second.ToString("X2"));
                response = _can.getResponseChannel2(canMsg, ref response1);
            }


            if (response == 1)
            {
                if (response1.Length == 0x03)
                {

                    try
                    {
                        if (response1[0] == 0x06 && response1[1] == 0x03 && response1[2] == 0x80)
                        {
                            retVal = 1;
                        }
                        else
                        {
                            retVal = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIMEI(ref string imei)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0200");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0200");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x02)
                {

                    try
                    {
                        imei = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            imei += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int writeIMEI(string imei)
        {
            int response, retVal;

            string cmd = "0B00";

            foreach (char c in imei)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readIMEIFromFlash(ref string imei)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C00");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C00");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x0C)
                {

                    try
                    {
                        imei = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            imei += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIMSI(ref string imsi)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0201");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0201");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x02)
                {

                    try
                    {
                        imsi = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            imsi += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIMSIFromFlash(ref string imsi)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C01");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C01");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x0C)
                {

                    try
                    {
                        imsi = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            imsi += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readHardwareVariantFromFlash(ref string hwVariant)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C04");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C04");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x06 && response1[0] == 0x0C)
                {

                    try
                    {
                        hwVariant = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            hwVariant += (response1[i]).ToString("X2");
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readSimNumber(ref string simNumber)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0206");
                response = _can.getResponseChannel1(canMsg, ref response1, 12);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0206");
                response = _can.getResponseChannel2(canMsg, ref response1, 12);
            }

            if (response == 1)
            {
                if (response1[0] == 0x02 && response1[1] == 0x06)
                {

                    try
                    {
                        simNumber = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            simNumber += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readSimFromFlash(ref string sim)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C02");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C02");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x16 && response1[0] == 0x0C)
                {

                    try
                    {
                        sim = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            sim += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readModemFirmwareVersion(ref string version)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0202");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0202");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x23 && response1[0] == 0x02)
                {

                    try
                    {
                        version = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0)
                            { break; }
                            version += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readTelephoneNumber(ref string number)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0203");
                response = _can.getResponseChannel1(canMsg, ref response1, 12);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0203");
                response = _can.getResponseChannel2(canMsg, ref response1, 12);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x02)
                {

                    try
                    {
                        number = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            number += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readTelephoneNumberFromFlash(ref string number)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C03");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C03");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {
                        number = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            number += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readPartNumberFromFlash(ref string number)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C05");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C05");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x1A && response1[0] == 0x0C)
                {

                    try
                    {
                        number = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            number += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }



        public int readIctPCBIDFromFlash(ref string pcbId)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C06");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C06");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x20 && response1[0] == 0x0C && response1[1] == 0x06)
                {

                    try
                    {
                        pcbId = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            pcbId += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIctTestDateFromFlash(ref string date)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C07");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C07");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x0A && response1[0] == 0x0C && response1[1] == 0x07)
                {

                    try
                    {
                        date = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            date += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIctFailCountFromFlash(ref int count)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C08");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C08");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x08)
                {

                    try
                    {
                        count = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIctPassFlagFromFlash(ref int flag)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C09");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C09");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x09)
                {

                    try
                    {
                        flag = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readPCBVariantFromFlash(ref int var)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C1B");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C1B");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x1B)
                {

                    try
                    {
                        var = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readVehicleVariantFromFlash(ref int var)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C1C");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C1C");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x1C)
                {

                    try
                    {
                        var = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIOConfigFromFlash(ref int var)
        {
         /*   int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C1D");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C1D");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x1D)
                {

                    try
                    {
                        var = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal; */
            return 1;
        }

        public int readVehicleFlagsFromFlash(ref int var)
        {
           /* int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C1E");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C1E");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C && response1[1] == 0x1E)
                {

                    try
                    {
                        var = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal; */
            return 1;
        }

        public int readOnNetworkStatus(ref string status)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0204");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0204");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x02)
                {

                    try
                    {
                        status = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            status += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readBatterySwitchStatus(ref int status)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0700");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0700");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x07)
                {
                    try
                    {
                        status = response1[2];

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int sendSms(string timestamp, string targetPhoneNumber)
        {
            int response, retVal, sent;

            if (_canChannel == 1)
            {
                sent = _can.sendCanFrameChannel1(targetIdent, 0, "0205" + timestamp + targetPhoneNumber);                
                if (sent >= 0)
                {
                    response = _can.getResponseChannel1(canMsg, ref response1, 20);
                }
                else
                {
                    response = -4;
                }
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0205" + timestamp + targetPhoneNumber);
                response = _can.getResponseChannel2(canMsg, ref response1, 20);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x02)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int getSendSmsStatus()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "020B");
                response = _can.getResponseChannel1(canMsg, ref response1, 20);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "020B");
                response = _can.getResponseChannel2(canMsg, ref response1, 20);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x02)
                {
                    if (response1[2] == 0x01)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int sendUdpPacket( string timestamp, string ip, string port)
        {
            int response, retVal, sent;

            
            if (_canChannel == 1)
            {
                sent = _can.sendCanFrameChannel1(targetIdent, 0, "0209" + timestamp + ip + port);
                if (sent >= 0)
                {
                    response = _can.getResponseChannel1(canMsg, ref response1, 20);
                }else
                {
                    response = -4;
                }
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0209" + timestamp + ip + port);
                response = _can.getResponseChannel2(canMsg, ref response1, 20);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x02)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int sdCardCheck()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0E00");
                response = _can.getResponseChannel1(canMsg, ref response1, 30);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0E00");
                response = _can.getResponseChannel2(canMsg, ref response1, 30);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0E)
                {
                    //if (response1[2] == 0x7f)
                    //{
                    //    retVal = -1;
                    //}
                    //else
                    //{
                        retVal = response1[2];
                    //}
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int fullEEpromErase()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0F0E");
                response = _can.getResponseChannel1(canMsg, ref response1, 30);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0F0E");
                response = _can.getResponseChannel2(canMsg, ref response1, 30);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0F)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int fullFlashErase()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0103");
                response = _can.getResponseChannel1(canMsg, ref response1, 30);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0103");
                response = _can.getResponseChannel2(canMsg, ref response1, 30);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x01)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int flashTestRoutine()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0106");
                response = _can.getResponseChannel1(canMsg, ref response1, 5);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0106");
                response = _can.getResponseChannel2(canMsg, ref response1, 5);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x01)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int loadDefaultConfiguration()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "5000");
                response = _can.getResponseChannel1(canMsg, ref response1, 3);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "5000");
                response = _can.getResponseChannel2(canMsg, ref response1, 3);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x50)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int storeParameters()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0108");
                response = _can.getResponseChannel1(canMsg, ref response1, 40);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0108");
                response = _can.getResponseChannel2(canMsg, ref response1, 40);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03)
                {
                    if (response1[0] == 0x01 && response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int sendUnitToSleep()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0D02");
                //response = _can.getResponseChannel1(canMsg, ref response1, 3);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0D02");
                //response = _can.getResponseChannel2(canMsg, ref response1, 3);
            }

            return 1;
        }

        public int readModemType(ref string modemType)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "02XX");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "02XX");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }


            if (response == 1)
            {
                if (response1.Length == 0x11 && response1[0] == 0x02)
                {

                    try
                    {
                        modemType = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            modemType += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int gpsStatus(ref string status)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0300");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0300");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            status = "";

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x03)
                {

                    try
                    {
                        if (response1[2] == 0x80)
                        {
                            status = "GPS OK";
                            retVal = 1;
                        }
                        else
                        {
                            status = "GPS NOK";
                            retVal = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int gpsSatsAndSnr(ref int numSats, ref int snr)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0301");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0301");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            numSats = 0;
            snr = 0;

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x03 && response1[1] == 0x01)
                {

                    try
                    {
                        numSats = response1[2];
                        snr = response1[3];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int kLinePullUpOn()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0500");
                response = _can.getResponseChannel1(canMsg, ref response1, 5);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0500");
                response = _can.getResponseChannel2(canMsg, ref response1, 5);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x05)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int kLinePullUpOff()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0501");
                response = _can.getResponseChannel1(canMsg, ref response1, 5);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0501");
                response = _can.getResponseChannel2(canMsg, ref response1, 5);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x05)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int kLineTest()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0503");
                response = _can.getResponseChannel1(canMsg, ref response1, 3);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0503");
                response = _can.getResponseChannel2(canMsg, ref response1, 3);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x05)
                {
                    if (response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readIgnitionStatus(ref int status)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0701");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0701");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x07 && response1[1] == 0x01)
                {
                    try
                    {
                        status = response1[2];

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readAccelerometerData(ref int x, ref int y, ref int z, ref bool hiRes)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0901");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0901");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                try
                {
                    if (response1.Length == 0x05 && response1[0] == 0x09)
                    {
                        x = response1[2];
                        y = response1[3];
                        z = response1[4];

                        if (x > 0x7F) x -= 256;
                        if (y > 0x7F) y -= 256;
                        if (z > 0x7F) z -= 256;

                        retVal = 1;

                        hiRes = false;
                    }
                    else if (response1.Length == 0x08 && response1[0] == 0x09)
                    {
                        x = response1[2];
                        x *= 0x100;
                        x += response1[3];

                        y = response1[4];
                        y *= 0x100;
                        y += response1[5];

                        z = response1[6];
                        z *= 0x100;
                        z += response1[7];

                        if (x > 0x7FFF) x -= 65536;
                        if (y > 0x7FFF) y -= 65536;
                        if (z > 0x7FFF) z -= 65536;

                        retVal = 1;

                        hiRes = true;
                    }
                    else
                    {
                        retVal = -2;
                    }
                }
                catch (Exception e)
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int writeSimNumber(string simNumber)
        {
            int response, retVal;

            string cmd = "0B02";

            foreach (char c in simNumber)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int ioOutON()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0703");
                response = _can.getResponseChannel1(canMsg, ref response1, 5);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0703");
                response = _can.getResponseChannel2(canMsg, ref response1, 5);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03)
                {
                    if (response1[0] == 0x07 && response1[1] == 0x03 && response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int ioOutOFF()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0704");
                response = _can.getResponseChannel1(canMsg, ref response1, 5);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0704");
                response = _can.getResponseChannel2(canMsg, ref response1, 5);
            }
            if (response == 1)
            {
                if (response1.Length == 0x03)
                {
                    if (response1[0] == 0x07 && response1[1] == 0x04 && response1[2] == 0x80)
                    {
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int writeIMSINumber(string IMSINumber)
        {
            int response, retVal;

            string cmd = "0B01";

            foreach (char c in IMSINumber)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readIctPcbId(ref string pcbId, bool readFromEeprom)
        {
            int response, retVal;
            string cmd = "0109";
            int check = 01;
            if (readFromEeprom)
            {
                cmd = "0F09";
                check = 0x0F;
            }

            pcbId = "";

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x20 && response1[0] == check && response1[1] == 0x09)
                {

                    try
                    {
                        for (int i = 2; i < response1.Length; i++)
                        {
                            pcbId += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }
            return retVal;
        }

        public int writeIctPcbId(string pcbId)
        {
            int response, retVal;

            string cmd = "0B06";

            foreach (char c in pcbId)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readPcbVariant(ref int pcbVariant, bool readFromEeprom)
        {
            int response, retVal;
            string cmd = "0C1B";
            int check = 0x0C;
            int check2 = 0x1B;

            if (readFromEeprom == true)
            {
                cmd = "0F0D";
                check = 0x0F;
                check2 = 0x0D;
            }

            pcbVariant = 0;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == check && response1[1] == check2)
                {

                    try
                    {
                        pcbVariant = ((int)response1[2]);

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }
            return retVal;
        }

        public int writePcbVariant(int var)
        {
            int response, retVal;

            string cmd = "0D03";

            cmd += var.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0D && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writePcbVariant_TestCode_3_2(int var)
        {
            int response, retVal;

            string cmd = "0B1B";

            cmd += var.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0B && response1[1] == 0x1B && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeVehicleVariant(int var)
        {
            int response, retVal;

            string cmd = "0B1C";

            cmd += var.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0B && response1[1] == 0x1C && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeIOConfig(int var)
        {
            int response, retVal;

            string cmd = "0B1D";

            cmd += var.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0B && response1[1] == 0x1D && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeVehicleFlags(int var)
        {
            int response, retVal;

            string cmd = "0B1E";

            cmd += var.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0B && response1[1] == 0x1E && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readIctPassFail(ref int passFail, bool readFromEeprom)
        {
            int response, retVal;
            string cmd = "010A";
            int check = 01;
            if (readFromEeprom)
            {
                cmd = "0F0A";
                check = 0x0F;
            }
            passFail = 0;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == check && response1[1] == 0x0A)
                {

                    try
                    {
                        passFail = response1[2];

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }
            return retVal;
        }
        public int writeIctPassFail(int passFailFlag)
        {
            int response, retVal;

            string cmd = "0B09";

            cmd += passFailFlag.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int readIctFailCount(ref int count, bool readFromEeprom)
        {
            int response, retVal;
            string cmd = "010B";
            int check = 01;
            if (readFromEeprom)
            {
                cmd = "0F0B";
                check = 0x0F;
            }
            count = 0;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == check && response1[1] == 0x0B)
                {

                    try
                    {
                        count = response1[2];

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }
            return retVal;
        }
        public int writeIctFailCount(int failCount)
        {
            int response, retVal;

            string cmd = "0B08";

            cmd += failCount.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int readIctTestDate(ref string ictTestDate, bool readFromEeprom)
        {
            int response, retVal;
            string cmd = "010C";
            int check = 01;
            if (readFromEeprom)
            {
                cmd = "0F0C";
                check = 0x0F;
            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x0A && response1[0] == check && response1[1] == 0x0C)
                {

                    try
                    {
                        ictTestDate = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            ictTestDate += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int writeIctTestDate(string testDate)
        {
            int response, retVal;

            string cmd = "0B07";

            foreach (char c in testDate)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeAteTestDate(string testDate)
        {
            int response, retVal;

            string cmd = "0B0A";

            foreach (char c in testDate)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                response = _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                if (response == 0)
                {
                    response = _can.getResponseChannel1(canMsg, ref response1);
                }
            }
            else
            {
                response = _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                if (response == 0)
                {
                    response = _can.getResponseChannel2(canMsg, ref response1);
                }
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int writeAteFailCount(int failCount)
        {
            int response, retVal;

            string cmd = "0B0B";

            cmd += failCount.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int writeAtePassFail(int passFailFlag)
        {
            int response, retVal;

            string cmd = "0B0C";

            cmd += passFailFlag.ToString("X2");

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int writePhoneNumber(string phoneNumber)
        {
            int response, retVal;

            string cmd = "0B03";

            foreach (char c in phoneNumber)
            {
                cmd += ((int)c).ToString("X2");

            }
            //cmd.PadRight(36, '0');
            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeHardwareVariantNumber(string hardwareVariantNumber)
        {
            int response, retVal;

            string cmd = "0B04";

            if (hardwareVariantNumber.Length != 8) return -3;

            for (int c = 0; c < 8; c += 2)
            {
                cmd += hardwareVariantNumber.Substring(c, 2);

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writePartNumber(string partNumber)
        {
            int response, retVal;

            string cmd = "0B05";

            if (partNumber.Length > 23) return -3;

            foreach (char c in partNumber)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeFirmwareID(string id)
        {
            int response, retVal;

            string cmd = "0B0D";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }
        public int writeServerTelNumberSecondary(string id)
        {
            int response, retVal;

            string cmd = "0B1F";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsPrimaryServer(string id)
        {
            int response, retVal;

            string cmd = "0B20";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsSecondaryServer(string id)
        {
            int response, retVal;

            string cmd = "0B21";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsEngineeringServer(string id)
        {
            int response, retVal;

            string cmd = "0B22";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsAUXServer(string id)
        {
            int response, retVal;

            string cmd = "0B23";

            foreach (char c in id)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeAdLow(int adValue)
        {
            int response, retVal;
            string temp = adValue.ToString("X4");
            string cmd = "0B0E";

            if (temp.Length != 4) return -3;

            for (int c = 0; c < 4; c += 2)
            {
                cmd += temp.Substring(c, 2);

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeAdMid(int adValue)
        {
            int response, retVal;
            string temp = adValue.ToString("X4");
            string cmd = "0B0F";

            if (temp.Length != 4) return -3;

            for (int c = 0; c < 4; c += 2)
            {
                cmd += temp.Substring(c, 2);

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }


        public int writeAdHigh(int adValue)
        {
            int response, retVal;
            string temp = adValue.ToString("X4");
            string cmd = "0B10";

            if (temp.Length != 4) return -3;

            for (int c = 0; c < 4; c += 2)
            {
                cmd += temp.Substring(c, 2);

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeSmsOnlyFlag(bool smsOnly)
        {
            int response, retVal;

            string cmd = "0B11";


            if (smsOnly == true)
            {
                cmd += "01";
            }
            else
            {
                cmd += "00";
            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeSmsServerPhoneNumber(string phoneNumber)
        {
            int response, retVal;

            string cmd = "0B12";

            foreach (char c in phoneNumber)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsPortNumber(int number)
        {
            int response, retVal;
            string temp = number.ToString("X4");
            string cmd = "0B13";

            if (temp.Length != 4) return -3;

            for (int c = 0; c < 4; c += 2)
            {
                cmd += temp.Substring(c, 2);

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsIpAddress(string address)
        {
            int response, retVal;
            string cmd = "0B14";

            string[] temp = address.Split('.');

            if (temp.Length != 4) return -3;

            for (int c = 0; c < 4; c++)
            {
                cmd += int.Parse(temp[c]).ToString("X2");
            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsAPN(string apn)
        {
            int response, retVal;

            string cmd = "0B15";

            foreach (char c in apn)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsUserName(string userName)
        {
            int response, retVal;

            string cmd = "0B16";

            foreach (char c in userName)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeGprsPassword(string pwd)
        {
            int response, retVal;

            string cmd = "0B17";

            foreach (char c in pwd)
            {
                cmd += ((int)c).ToString("X2");
            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeBootloaderVersion(string bootloaderVersion)
        {
            int response, retVal;

            string cmd = "0B18";

            foreach (char c in bootloaderVersion)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int writeFirmwareFilename(string fileName)
        {
            int response, retVal;

            string cmd = "0B19";

            foreach (char c in fileName)
            {
                cmd += ((int)c).ToString("X2");

            }

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, cmd);
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, cmd);
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x3 && response1[0] == 0x0b && response1[2] == 0x80)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -2;
            }

            return retVal;
        }

        public int readAteTestDateFromFlash(ref string date)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0A");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0A");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x0A && response1[0] == 0x0C)
                {

                    try
                    {
                        date = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            date += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readAteFailCountFromFlash(ref int count)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0B");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0B");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C)
                {

                    try
                    {
                        count = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }
        public int readAtePassFlagFromFlash(ref int flag)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0C");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0C");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C)
                {

                    try
                    {
                        flag = response1[2];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readFirmwareIdFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0D");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0D");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x20 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readServerTelNumberSecondaryFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C1F");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C1F");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsPrimaryServerFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C20");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C20");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x42 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsSecondaryServerFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C21");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C21");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x42 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsEngineeringServerFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C22");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C22");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x42 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsAUXServerFromFlash(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C23");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C23");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x42 && response1[0] == 0x0C)
                {

                    try
                    {
                        id = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            id += ((Char)response1[i]).ToString();
                        }
                        id = id.Replace("\0", "");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readAdLowFromFlash(ref int value)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0E");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0E");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x0C)
                {

                    try
                    {
                        value = response1[2];
                        value = value << 8;
                        value += response1[3];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readAdMidFromFlash(ref int value)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C0F");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C0F");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x0C)
                {

                    try
                    {
                        value = response1[2];
                        value = value << 8;
                        value += response1[3];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readAdHighFromFlash(ref int value)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C10");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C10");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x0C)
                {

                    try
                    {
                        value = response1[2];
                        value = value << 8;
                        value += response1[3];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }
        public int readSmsOnlyFromFlash(ref bool flag)
        {
            int response, retVal;

            flag = false;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C11");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C11");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0C)
                {

                    try
                    {
                        if (response1[2] == 1)
                        {
                            flag = true;
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readSmsServerNumberFromFlash(ref string number)
        {
            int response, retVal;

            number = "";

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C12");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C12");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {

                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            number += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsPortFromFlash(ref int port)
        {
            int response, retVal;

            port = 0;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C13");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C13");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x0C)
                {

                    try
                    {
                        port = response1[2];
                        port = port << 8;
                        port += response1[3];
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }
        public int readGprsServerIpFromFlash(ref string address)
        {
            int response, retVal;
            address = "";

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C14");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C14");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x06 && response1[0] == 0x0C)
                {

                    try
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            if (address == "")
                            { }
                            else
                            {
                                address += ".";
                            }
                            address += response1[i + 2].ToString();
                        }

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsApnFromFlash(ref string apn)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C15");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C15");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x22 && response1[0] == 0x0C)
                {

                    try
                    {
                        apn = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            apn += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsUserNameFromFlash(ref string name)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C16");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C16");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {
                        name = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            name += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGprsPasswordFromFlash(ref string password)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C17");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C17");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {
                        password = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            password += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readBootLoaderVersionFromFlash(ref string bootloaderVersion)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C18");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C18");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x12 && response1[0] == 0x0C)
                {

                    try
                    {
                        bootloaderVersion = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            bootloaderVersion += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readFirmwareFilenameFromFlash(ref string filename)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0C19");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0C19");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x10 && response1[0] == 0x0C)
                {

                    try
                    {
                        filename = "";
                        for (int i = 2; i < response1.Length; i++)
                        {
                            if (response1[i] == 0) break;
                            filename += ((Char)response1[i]).ToString();
                        }
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readProductionTestFirmwareVersion(ref string version)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0D00");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0D00");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x04 && response1[0] == 0x0D && response1[1] == 0x00)
                {

                    try
                    {
                        version = "";
                        version = response1[2].ToString("X2") + ".";
                        version += response1[3].ToString("X2");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readProcessorVersion(ref string version)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.channel1.CANL2_reset_lost_msg_counter();
                _can.channel1.CANL2_reset_rcv_fifo();
                _can.channel1.CANL2_reset_xmt_fifo();
                System.Threading.Thread.Sleep(25);
                _can.sendCanFrameChannel1(targetIdent, 0, "0D01");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.channel2.CANL2_reset_lost_msg_counter();
                _can.channel2.CANL2_reset_rcv_fifo();
                _can.channel2.CANL2_reset_xmt_fifo();
                System.Threading.Thread.Sleep(25);
                _can.sendCanFrameChannel2(targetIdent, 0, "0D01");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                int respLen = 6;
                
                if (response1.Length == respLen && response1[0] == 0x0D && response1[1] == 0x01)
                {

                    try
                    {
                        version = "";

                        version = response1[2].ToString("X2");
                        version += response1[3].ToString("X2");
                        version += response1[4].ToString("X2");
                        version += response1[5].ToString("X2");
                        
                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGyroID(ref string id)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.channel1.CANL2_reset_lost_msg_counter();
                _can.channel1.CANL2_reset_rcv_fifo();
                _can.channel1.CANL2_reset_xmt_fifo();
                System.Threading.Thread.Sleep(25);
                _can.sendCanFrameChannel1(targetIdent, 0, "0902");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.channel2.CANL2_reset_lost_msg_counter();
                _can.channel2.CANL2_reset_rcv_fifo();
                _can.channel2.CANL2_reset_xmt_fifo();
                System.Threading.Thread.Sleep(25);
                _can.sendCanFrameChannel2(targetIdent, 0, "0902");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x09 && response1[1] == 0x02)
                {

                    try
                    {
                        id = "";
                        id = response1[2].ToString("X2");

                        retVal = 1;
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readGyroData(ref int x, ref int y, ref int z)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0903");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0903");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                try
                {
                    if (response1.Length == 0x08 && response1[0] == 0x09)
                    {
                        x = response1[2];
                        x *= 0x100;
                        x += response1[3];

                        y = response1[4];
                        y *= 0x100;
                        y += response1[5];

                        z = response1[6];
                        z *= 0x100;
                        z += response1[7];

                        if (x > 0x7FFF) x -= 65536;
                        if (y > 0x7FFF) y -= 65536;
                        if (z > 0x7FFF) z -= 65536;

                        retVal = 1;

                    }
                    else
                    {
                        retVal = -2;
                    }
                }
                catch (Exception e)
                {
                    retVal = -1;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int isOnNetwork()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0204");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0204");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x02)
                {

                    try
                    {
                        if (response1[2] == 0x80)
                        {
                            retVal = 1;
                        }
                        else
                        {

                            retVal = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int getGsmStatus(out string explanation)
        {
            int response, retVal;
            explanation = "";

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "020A");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "020A");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x02)
                {

                    try
                    {
                        retVal=response1[2];
                        switch (retVal)
                        {
                            case 0:
                                explanation = "GSM OK";
                                break;
                            case 0x10:
                                explanation = "Imei ERR";
                                break;
                            case 0x11:
                                explanation = "GSM Comms ERR - Check modem soldering";
                                break;
                            case 0x12:
                                explanation = "Imsi ERR - Check SIM";
                                break;
                            case 0x13:
                                explanation = "Sim Logic ERR - PL9, IC21, TR10, TR11";
                                break;
                            case 0x14:
                                explanation = "GSM Firmware ERR";
                                break;
                            case 0xFF:
                                explanation = "GSM Not Ready";
                                break;
                            default:
                                explanation = "GSM Unknown ERR";
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int clearCrankDetectFlag()
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0D03");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0D03");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0D)
                {

                    try
                    {
                        if (response1[2] == 0x80)
                        {
                            retVal = 1;
                        }
                        else
                        {

                            retVal = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int readCrankDetectFlag(ref int flag)
        {
            int response, retVal;

            if (_canChannel == 1)
            {
                _can.sendCanFrameChannel1(targetIdent, 0, "0D04");
                response = _can.getResponseChannel1(canMsg, ref response1);
            }
            else
            {
                _can.sendCanFrameChannel2(targetIdent, 0, "0D04");
                response = _can.getResponseChannel2(canMsg, ref response1);
            }

            if (response == 1)
            {
                if (response1.Length == 0x03 && response1[0] == 0x0D)
                {

                    try
                    {
                        flag = response1[2];
                        retVal = 1;
                        
                    }
                    catch (Exception e)
                    {
                        retVal = -1;
                    }
                }
                else
                {
                    retVal = -2;
                }
            }
            else
            {
                retVal = -3;
            }

            return retVal;
        }

        public int getRtuCan2Response(uint ident)
        {
            int response, retVal;
            response = 0;
            CanInterfaceSofting.canFrames2.Clear();

            for(int i = 0;i<3;i++)
            {
                _can.sendCanFrameChannel2(ident, 0, "0101");
                for (int c = 0; c < 5; c++)
                {
                    System.Threading.Thread.Sleep(100);
                    response = _can.getCanFrameChannel2(canMsg);
                    if (response == 1) break;
                }
                System.Windows.Forms.Application.DoEvents();
                if (response == 1) break;
            }

            if (response == 1)
            {
                if (canMsg.Ident == 0x7F2 && canMsg.RCV_data[0] ==8)
                {

                    retVal = 1;
                }
                else
                {
                    retVal = 0;
                }
            }
            else
            {
                retVal = -1;
            }

            return retVal;
        }
        private byte[] temp;
        private byte[] binData;
        private byte[] binDataWhiteList;
        private int datapointer, binState;
        private long memStart = 0x186000 + 4;
        private uint whiteListTx = 0x7E0;
        private uint whiteListRx = 0x7F1;
        private byte subDp, chk;
        private byte _msbLen, _lsbLen, _msbCrc, _lsbCrc = 0;

        public bool readWhiteListFromFile(string fileName)
        {
            string temp = "";
            bool retVal = false;
            try
            {
                binData = File.ReadAllBytes(fileName);

                _msbLen = binData[14];
                _lsbLen = binData[13];
                _msbCrc = binData[16];
                _lsbCrc = binData[15];

                int labelLen = binData[21] + (binData[22] << 8);



                int dataLen = binData[13] + (binData[14] << 8);

                binDataWhiteList = new byte[dataLen];

                for (int i = 0; i < dataLen; i++)
                {
                    binDataWhiteList[i] = binData[i + 23 + labelLen];
                }

                datapointer = 0;
                retVal = true;
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
            }
            return retVal;
        }
        private void sendProgramRequestWhiteList()
        {
            temp = new byte[128];
            System.Diagnostics.Debug.WriteLine("DP = " + datapointer.ToString("X2"));
            System.Diagnostics.Debug.WriteLine("memStart = " + memStart.ToString("X2"));

            for (int i = 0; i < 128; i++)
            {
                if ((datapointer) >= binDataWhiteList.GetLength(0))
                {
                    break;
                }
                temp[i] = binDataWhiteList[datapointer++];
            }
            //datapointer += 128;

            
            byte[] data = new byte[8];

            data[0] = 6;
            data[1] = 0x31;
            data[2] = (byte)((memStart & 0xFF000000) >> 24);
            data[3] = (byte)((memStart & 0xFF0000) >> 16);
            data[4] = (byte)((memStart & 0xFF00) >> 8);
            data[5] = (byte)((memStart & 0xFF));
            data[6] = 128;
            data[7] = 0;


            memStart += 128;
            subDp = 0;
            chk = temp[0];

            int res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);

            binState = 1;
        }

        private void sendFirstBinFrameWhiteList()
        {

            int res = 0;

            byte[] data = new byte[8];

            chk = 0;

            data[0] = 0x10;
            data[1] = 0x80;
            for (int i = 2; i < 8; i++)
            {
                data[i] = temp[i - 2];
                chk = (byte)(chk ^ (byte)(temp[i - 2]));
            }

            subDp = 6;

            res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);
           
            binState = 2;
        }
        private void sendCfBinFramesWhiteList(Action<int , int , int > reportProgress)
        {
            

            int res = 0;
            byte id = 0x21;

            for (int i = 0; i < 18; i++)
            {
                byte[] data = new byte[8];

                data[0] = id++;
                if (id == 0x30) id = 0x21;
                for (byte p = 1; p < 8; p++)
                {
                    if ((p - 1 + subDp) == 128)
                    {
                        data[p] = chk;
                        break;
                    }
                    else
                    {
                        data[p] = temp[p - 1 + subDp];
                        chk = (byte)(chk ^ temp[p - 1 + subDp]);
                    }

                }
                subDp += 7;
                res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);


                System.Threading.Thread.Sleep(10);
                
            }

            try
            {
                reportProgress(0,  binDataWhiteList.GetLength(0), datapointer);
            }
            catch (Exception err)
            {
                
            }

            binState = 3;
        }
        private bool enterUploadWhiteList()
        {

            System.Diagnostics.Debug.WriteLine("Access Upload Mode...");


            
            byte[] data = new byte[8];

            data[0] = 2;
            data[1] = 0x0d;
            data[2] = 0x05;
            data[3] = 0;
            data[4] = 0;
            data[5] = 0;
            data[6] = 0;
            data[7] = 0;

            int res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);

            int response;
            DateTime timeout = DateTime.Now.AddMilliseconds(1000);
            PARAM_CLASS canMsg = new PARAM_CLASS();
            do
            {
                response = _can.getCanFrameChannel1(canMsg);
               
                switch (response)
                {
                    case 0:
                        {
                            //No Can Frame

                            break;
                        }
                    case 1:
                    case 9:
                        {
                            //Std Frame Received
                            
                            if (canMsg.Ident == whiteListRx)
                            {
                                if (canMsg.RCV_data[3] == 0x80)
                                {
                                    System.Diagnostics.Debug.WriteLine("Upload Mode Entered");
                                    return true;
                                }
                            }
                            break;
                        }
                }
            } while (DateTime.Now < timeout);
            System.Diagnostics.Debug.WriteLine("Access Upload Mode Failed");
            return false;
        }

        private void exitUploadWhiteList()
        {

            System.Diagnostics.Debug.WriteLine("Exit Upload Mode...");


            
            byte[] data = new byte[8];

            data[0] = 1;
            data[1] = 0x61;
            data[2] = 0x00;
            data[3] = 0;
            data[4] = 0;
            data[5] = 0;
            data[6] = 0;
            data[7] = 0;

            int res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);
                        
            int response;
            DateTime timeout = DateTime.Now.AddMilliseconds(1000);
            PARAM_CLASS canMsg = new PARAM_CLASS();
            do
            {
                response = _can.getCanFrameChannel1( canMsg);
                
                switch (response)
                {
                    case 0:
                        {
                            //No Can Frame

                            break;
                        }
                    case 1:
                    case 9:
                        {
                            //Std Frame Received
                            
                            if (canMsg.Ident == whiteListTx)
                            {
                                if (canMsg.RCV_data[3] == 0x80)
                                {
                                    System.Diagnostics.Debug.WriteLine("Upload Mode Exited");
                                    return;
                                }
                            }
                            break;
                        }
                }
            } while (DateTime.Now < timeout);
            System.Diagnostics.Debug.WriteLine("Exit Upload Mode Failed");
        }

        public bool verifyUploadWhiteList()
        {
            return verifyUploadWhiteList(_msbLen, _lsbLen, _msbCrc, _lsbCrc);
        }
        private bool verifyUploadWhiteList(byte msbLen, byte lsbLen, byte msbCrc, byte lsbCrc)
        {
            bool retVal = false;

            System.Diagnostics.Debug.WriteLine("Verfiy Upload ...");


           
            byte[] data = new byte[8];

            data[0] = 6;
            data[1] = 0x0d;
            data[2] = 0x06;
            data[3] = msbLen;
            data[4] = lsbLen;
            data[5] = msbCrc;
            data[6] = lsbCrc;
            data[7] = 0;

            int res = _can.sendCanFrameChannel1(whiteListTx, 0, 8, data);

           
            int response;
            DateTime timeout = DateTime.Now.AddMilliseconds(1000);
            PARAM_CLASS canMsg = new PARAM_CLASS();
            do
            {
                response = _can.getCanFrameChannel1( canMsg);
                
                switch (response)
                {
                    case 0:
                        {
                            //No Can Frame

                            break;
                        }
                    case 1:
                    case 9:
                        {
                            //Std Frame Received
                           
                            if (canMsg.Ident == whiteListRx)
                            {
                                if (canMsg.RCV_data[3] == 0x80)
                                {
                                    System.Diagnostics.Debug.WriteLine("Upload Verifed");
                                    return true;
                                }
                            }
                            break;
                        }
                }
            } while (DateTime.Now < timeout);
            System.Diagnostics.Debug.WriteLine("Upload Failed Len/CRC");
            return false;
        }
        public bool loadWhiteList(Action<int, int, int> reportProgress)
        {
            bool finished = false;
            bool exitLoop = false;
            int response;
            PARAM_CLASS canMsg = new PARAM_CLASS();
            string textToDisplay = "";
            
            datapointer = 0;
            memStart = 0x186004;
            binState = 0;
            DateTime timeout = DateTime.Now.AddSeconds(60);

            

            if (enterUploadWhiteList() == false)
            {
                if (enterUploadWhiteList() == false)
                {
                    if (enterUploadWhiteList() == false)
                    {
                        return false;
                    }
                }
            }

            sendProgramRequestWhiteList();
            bool rollback = false;
            do
            {
                response = _can.getCanFrameChannel1(canMsg);
                
                switch (response)
                {
                    case 0:
                        {
                            //No Can Frame

                            break;
                        }
                    case 1:
                    case 9:
                        {
                            //Std Frame Received
                            
                            if (canMsg.Ident == whiteListRx)
                            {
                                switch (binState)
                                {
                                    case 1:
                                        if (canMsg.RCV_data[3] == 0x7F)
                                        {
                                            if (rollback == false)
                                            {
                                                rollback = true;
                                                datapointer -= 128;
                                                memStart -= 128;
                                            }
                                            sendProgramRequestWhiteList();
                                        }
                                        else if (canMsg.RCV_data[3] == 0x80)
                                        {
                                            rollback = false;
                                            sendFirstBinFrameWhiteList();
                                        }
                                        else if (canMsg.RCV_data[3] == 0x55)
                                        {
                                            enterUploadWhiteList();
                                            if (rollback == false)
                                            {
                                                rollback = true;
                                                datapointer -= 128;
                                                memStart -= 128;
                                            }
                                            sendProgramRequestWhiteList();
                                        }

                                        break;
                                    case 2:
                                        if (canMsg.RCV_data[3] == 0x7F)
                                        {
                                            if (rollback == false)
                                            {
                                                rollback = true;
                                                datapointer -= 128;
                                                memStart -= 128;
                                            }
                                            sendProgramRequestWhiteList();
                                        }
                                        else if (canMsg.RCV_data[0] == 0x30)
                                        {
                                            sendCfBinFramesWhiteList(reportProgress);
                                        }
                                        else
                                        {

                                        }
                                        break;
                                    case 3:
                                        if (canMsg.RCV_data[3] == 0x7F)
                                        {
                                            if (rollback == false)
                                            {
                                                rollback = true;
                                                datapointer -= 128;
                                                memStart -= 128;
                                            }
                                            sendProgramRequestWhiteList();
                                        }
                                        else if (canMsg.RCV_data[3] == 0x80)
                                        {
                                            if (datapointer >= binDataWhiteList.GetLength(0))
                                            {
                                                exitLoop = true;
                                                finished = true;
                                            }
                                            else
                                            {
                                                sendProgramRequestWhiteList();
                                            }
                                        }
                                        break;
                                }

                            }
                            break;
                        }
                }
                System.Windows.Forms.Application.DoEvents();

                if(timeout<DateTime.Now)
                {
                   // exitLoop = true;
                }

            } while (exitLoop == false);
                        
            exitUploadWhiteList();

            return finished;
        }
    }
}
