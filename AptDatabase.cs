﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace V3ProductionTest
{
    public class Id
    {
        public string password;        

        public Id()
        {
            password = "";            
        }

        public Id(string password)
        {            
            this.password = password;            
        }
    }
    public class Customer
    {
        public int id;
        public string name;
        public string labelFile;

        public Customer()
        {
            id = 0;
            name = "";
            labelFile = "";
        }

        public Customer(int id, string custName, string labelTemplate)
        {
            this.id = id;
            name = custName;
            labelFile = labelTemplate;
        }
    }

    public class configV2
    {
        public int id;
        public string testSequence;
        public string displayDescription;
        public bool production;
        public string voidSoftware;
        public bool allowVariantSelection;
        public int fwVarMappingID;
        public bool allowVehicleFlags;
        public bool allowLite;
        public bool largeMicro;
        public bool v3_5Production;

        public configV2()
        {
            this.id = 0;
            this.testSequence = "";
            this.displayDescription = "";
            this.production = false;
            this.voidSoftware = "";
            this.allowVariantSelection = false;
            this.fwVarMappingID = 0;
            this.allowVehicleFlags = false;
            this.allowLite = false ;
            this.largeMicro = false;
            this.v3_5Production = false;
        }

        public configV2(int id, string testSequence, string displayDescription, bool production, string voidSoftware, bool allowVariantSelection, int fwVarMappingID, bool allowVehicleFlags, bool allowLite, bool largeMicro, bool v3_5Production)
        {
            this.id = id;
            this.testSequence = testSequence;
            this.displayDescription = displayDescription;
            this.production = production;
            this.voidSoftware = voidSoftware;
            this.allowVariantSelection = allowVariantSelection;
            this.fwVarMappingID = fwVarMappingID;
            this.allowVehicleFlags = allowVehicleFlags;
            this.allowLite = allowLite;
            this.largeMicro = largeMicro;
            this.v3_5Production = v3_5Production;
        }
    }

    public class customerToConfigV2
    {
        public int id;
        public int fkCustomerID;
        public int fkConfigV2;

        public customerToConfigV2()
        {
            this.id = 0;
            this.fkCustomerID = 0;
            this.fkConfigV2 = 0;
        }
        public customerToConfigV2(int id, int fkCustomerID, int fkConfigV2)
        {
            this.id = id;
            this.fkCustomerID = fkCustomerID;
            this.fkConfigV2 = fkConfigV2;
        }
    }

    public class firmwareVariantMapping
    {
        public int id;
        public string description;
        public string varIDList;

        public firmwareVariantMapping()
        {
            this.id = 0;
            this.description = "";
            this.varIDList = "";
        }
        public firmwareVariantMapping(int id, string description, string varIDList)
        {
            this.id = id;
            this.description = description;
            this.varIDList = varIDList;
        }
    }

    public class simDetails
    {
        public int id;
        public string description;
        public int typeCode;
        public bool enable;
        public string apn;
        public string user;
        public string pass;
        public int phoneNumLen;
        public int simNumLen;
        public bool manualPhoneNum;
        public bool sendTestSms;

        public simDetails()
        {
            this.id = 0;
            this.description = "";
            this.typeCode = 0;
            this.enable = false;
            this.apn = "";
            this.user = "";
            this.pass = "";
            this.phoneNumLen = 0;
            this.simNumLen = 0;
            this.manualPhoneNum = false;
            this.sendTestSms = false;
        }

        public simDetails(int id, string description, int typeCode, bool enable, string apn, string user, string pass, int phoneNumLen, int simNumLen, bool manualPhoneNum, bool sendTestSms)
        {
            this.id = id;
            this.description = description;
            this.typeCode = typeCode;
            this.enable = enable;
            this.apn = apn;
            this.user = user;
            this.pass = pass;
            this.phoneNumLen = phoneNumLen;
            this.simNumLen = simNumLen;
            this.manualPhoneNum = manualPhoneNum;
            this.sendTestSms = sendTestSms;            
        }
    }
    public class variantID
    {
        public int id;
        public string description;
        public int varID;
        public bool enabled;

        public variantID()
        {
            this.id = 0;
            this.description = "";
            this.varID = 0;
            this.enabled = false;
        }

        public variantID(int id, string description, int varID, bool enabled)
        {
            this.id = id;
            this.description = description;
            this.varID = varID;
            this.enabled = enabled;
        }
    }
}
