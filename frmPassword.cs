﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace V3ProductionTest
{
    public enum loginRequest
	{
        admin,
        labelReprint,
        ict,
        advanced
	         
	}
    public partial class frmPassword : Form
    {
        private int _timePeriod = 0;

        public int TimePeriod
        {
            get { return _timePeriod; }
            set { _timePeriod = value; }
        }
        private bool _passwordValidated = false;
        
        loginRequest _lr;

        public bool PasswordValidated
        {
            get { return _passwordValidated; }            
        }

        public frmPassword(loginRequest lr)
        {
            
            _lr = lr;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _passwordValidated = false;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string temp = "";
                _passwordValidated = false;
                
                List<Id> dt;
                dt = AmlConnector.getIds();

               

                if (_lr == loginRequest.admin)
                {
                    temp = dt[0].password;
                }
                else if (_lr == loginRequest.labelReprint)
                {
                    temp = dt[1].password;
                }
                else if (_lr == loginRequest.ict)
                {
                    temp = dt[2].password;
                }
                else if (_lr == loginRequest.advanced)
                {
                    temp = dt[3].password;
                }

                if (temp == this.textBox1.Text)
                {
                    _passwordValidated = true;
                }

                try
                {
                    //_timePeriod = int.Parse(dt.Rows[0].ItemArray[1].ToString());
                    _timePeriod = 30;
                }
                catch (Exception localErr)
                { }
            }
            catch (Exception err)
            { }
            this.Close();
        }

        private void frmPassword_Load(object sender, EventArgs e)
        {

        }

        private void frmPassword_Activated(object sender, EventArgs e)
        {
            this.textBox1.Focus();
        }

       
    }
}
