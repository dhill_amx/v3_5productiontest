﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using U8 = System.Byte;   // unsigned char
using U16 = System.UInt16; // unsigned short
using U32 = System.UInt32; // unsigned int
using U64 = System.UInt64; // unsigned long long

using I8 = System.SByte; // signed char
using I16 = System.Int16; // signed short
using I32 = System.Int32; // signed int
using I64 = System.Int64; // signed long long

namespace V3ProductionTest
{
    public class jLinkProgressEventArgs: EventArgs
    {
        private string _msg;
        private int _percentage;

        public int Percentage
        {
          get { return _percentage; }
          set { _percentage = value; }
        }

        public string Msg
        {
          get { return _msg; }
          set { _msg = value; }
        }
    }

    public delegate void reportProgress(object sender, jLinkProgressEventArgs e);

    public class jLinkAPI
    {
        #region Functions
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void JLINKARM_LOG(sbyte[] s);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void JLINK_FLASH_PROGRESS_CB_FUNC(byte[] sAction, byte[] sProg, int percentage);

        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Open", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Open();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_SelectIP", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_SelectIP(string sHost, int Port);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_EMU_SelectByUSBSN", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_EMU_SelectByUSBSN(int SerilaNo);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_OpenEx", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_OpenEx(JLINKARM_LOG pfLog, JLINKARM_LOG pfErrorOut);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_ExecCommand", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static Int32 JLINKARM_ExecCommand(sbyte[] pIn, out sbyte[] pOut, int BufferSize);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_TIF_Select", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_TIF_Select(int Interface);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_SetSpeed", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_SetSpeed(int Speed);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Connect", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Connect();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Close", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static extern int JLINKARM_Close();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_ReadMemU32", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_ReadMemU32(U32 Addr, U32 NumItems, U32[] pData, U8[] pStatus);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_WriteU32", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_WriteU32(U32 Addr, U32 Data);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Halt", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static char JLINKARM_Halt();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Reset", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_Reset();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_BeginDownload", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_BeginDownload(U32 Flags);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINK_DownloadFile", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINK_DownloadFile(sbyte[] sFileName, U32 Addr);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_EndDownload", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_EndDownload();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINKARM_Go", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINKARM_Go();
        [DllImport("JLinkARM.dll", EntryPoint = "JLINK_SetFlashProgProgressCallback", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINK_SetFlashProgProgressCallback(JLINK_FLASH_PROGRESS_CB_FUNC pfOnFlashProgess);
        [DllImport("JLinkARM.dll", EntryPoint = "JLINK_EraseChip", CallingConvention = CallingConvention.Cdecl)]
        unsafe public extern static int JLINK_EraseChip();
        #endregion

        #region Defines
        U32 FLASH_OPTKEYR       =          (0x40023C00 + 0x08);
        U32 FLASH_SR            =          (0x40023C00 + 0x0C);
        U32 FLASH_OPTCR         =          (0x40023C00 + 0x14);

        U32 STM32F2XXX_OPT_RESET_VALUE = (0x0FFFAAEC);

        #endregion

        int statusBufferSize = 100;
        U8[] _status;
        JLINK_FLASH_PROGRESS_CB_FUNC _progress;

        public event reportProgress progressStatus;

        public jLinkAPI()
        {
            _status = new U8[statusBufferSize];
            _progress = new JLINK_FLASH_PROGRESS_CB_FUNC(processFlashProgress);
        }
        ~jLinkAPI()
        {
            try
            {
                JLINKARM_Close();
            }
            catch (Exception e)
            {               
                
            }
        }

        private void processFlashProgress(byte[] sAction, byte[] sProg, int percentage)
        {
            string sAct = "None";
            string sP = "None";
            string msg = "None";
            if (sAction != null)
            {
                sAct = ASCIIEncoding.ASCII.GetString(sAction);
            }
            if (sProg != null)
            {
                sP = ASCIIEncoding.ASCII.GetString(sProg);
            }
            msg = "sAction = " + sAct + " , sProg = " + sP ;
            //System.Diagnostics.Debug.WriteLine(msg + " , % = " + percentage.ToString());
            jLinkProgressEventArgs e = new jLinkProgressEventArgs();
            e.Msg = msg;
            e.Percentage = percentage;
            updateStatus(e);
        }

        protected virtual void updateStatus(jLinkProgressEventArgs e)
        {
            if (progressStatus != null)
            {
                progressStatus(this, e);
            }
        }
        public int _SecureChip() 
        {
            U32[] optcr = new U32[1];
            U32[] Status = new U32[1];

            JLINKARM_ReadMemU32(FLASH_OPTCR, 1, optcr, _status);
            //
            // If device is already read-protected, we are done here
            //
            if (((optcr[0] >> 8) & 0xFF) != 0xAA) {
                return 0;
            }
            //
            // If flash option control register is locked, unlock it
            //
            if ((optcr[0] & 1) == 1) {
                JLINKARM_WriteU32(FLASH_OPTKEYR, 0x08192A3B);
                JLINKARM_WriteU32(FLASH_OPTKEYR, 0x4C5D6E7F);
            }
            optcr[0] &= 0xFFFF00FC;
            //
            // Set protection to protection level 1
            //
            optcr[0] |= 0x000000FF << 8;
            JLINKARM_WriteU32(FLASH_OPTCR, optcr[0]);
            //
            // Start option byte programming
            //
            JLINKARM_WriteU32(FLASH_OPTCR, (optcr[0] | (1 << 1)));
            //
            // Wait for operation to complete
            //
            do 
            {
                JLINKARM_ReadMemU32(FLASH_SR, 1,Status, _status);
                //System.Diagnostics.Debug.WriteLine(Status[0].ToString());
            } while ((Status[0] & (1uL << 16))!=0);
            JLINKARM_WriteU32(FLASH_OPTCR, optcr[0] | 1);  // Lock access to flash option control register
            return 0;
        }

        /*********************************************************************
        *
        *       _UnsecureChip
        */
        public int _UnsecureChip()
        {
            U32[] optcr = new U32[1];
            U32[] Status = new U32[1];

            Status[0] = (1 << 11) | (1 << 12);            // Debug independent watchdog and debug window watchdog are stopped when the CPU is halted
            JLINKARM_WriteU32(0xE0042008, Status[0]);     // DBGMCU_APB1_FZ
            JLINKARM_Halt();
            JLINKARM_ReadMemU32(FLASH_OPTCR, 1, optcr, _status);
            //
            // Option bytes have reset values, do not perform any unsecure
            //
            if ((optcr[0] & 0x0FFFFFEC) == STM32F2XXX_OPT_RESET_VALUE) {
                return 0;
            }
            //
            // If flash option control register is locked, unlock it
            //
            if ((optcr[0] & 1) == 1) {
                JLINKARM_WriteU32(FLASH_OPTKEYR, 0x08192A3B);
                JLINKARM_WriteU32(FLASH_OPTKEYR, 0x4C5D6E7F);
            }
            //
            // Set option bytes to reset values
            //
            JLINKARM_WriteU32(FLASH_OPTCR, STM32F2XXX_OPT_RESET_VALUE);
            //
            // Start option byte programming
            //
            JLINKARM_WriteU32(FLASH_OPTCR, STM32F2XXX_OPT_RESET_VALUE | (1 << 1));
            //
            // Wait for operation to complete
            //
            do 
            {
                JLINKARM_ReadMemU32(FLASH_SR, 1, Status, _status);                
            } while ((Status[0] & (1uL << 16)) !=0);
            JLINKARM_WriteU32(FLASH_OPTCR, STM32F2XXX_OPT_RESET_VALUE | 1);  // Lock access to flash option control register
            
            return 0;
            
        }

        private static void JLINKARM_LOG_callback_pfErrorOut(sbyte[] s)
        {
            try
            {
                foreach(sbyte b in s)
                {
                    System.Diagnostics.Debug.Write(b.ToString("X"));
                }
                System.Diagnostics.Debug.WriteLine("");
            }
            catch (Exception e)
            {                
               
            }
            
        }
        private static JLINKARM_LOG pJLINKARM_LOG_callback_pfErrorOut = JLINKARM_LOG_callback_pfErrorOut;

        public bool connect(string device)
        {
            bool retVal = false;
            sbyte[] acIn = new sbyte[256];
            sbyte[] acOut = new sbyte[256];

            int res = JLINKARM_OpenEx(null, pJLINKARM_LOG_callback_pfErrorOut);

            if (res < 0)
            {
                retVal = false;
            }
            else
            {
                //Disable Power
                acIn = Array.ConvertAll(Encoding.ASCII.GetBytes("SupplyPower = 0"), q => Convert.ToSByte(q));
                res = JLINKARM_ExecCommand(acIn, out acOut, 256);

                //Disable Power
                acIn = Array.ConvertAll(Encoding.ASCII.GetBytes("SupplyPowerDefault = 0"), q => Convert.ToSByte(q));
                res = JLINKARM_ExecCommand(acIn, out acOut, 256);

                //Set Device
                acIn = Array.ConvertAll(Encoding.ASCII.GetBytes("Device = " + device), q => Convert.ToSByte(q));
                res = JLINKARM_ExecCommand(acIn, out acOut, 256);

                //Select JTAG
                JLINKARM_TIF_Select(0);           

                // Select target interface speed
                // Auto = 0
                // Adaptive = 0xFFFF
                JLINKARM_SetSpeed(0);

                res = JLINKARM_Connect();
                if(res<0)
                {       
                    retVal = false;
                }
                else
	            {
                    retVal = true;
	            }       
            }

            return retVal;
        }

        public void close()
        {
            JLINKARM_Close();
        }

        public int program(string filePath, bool forceErase)
        {
            int retval = 0;
            int res = 0;
            sbyte[] acIn = new sbyte[256];
            sbyte[] acOut = new sbyte[256];

            //acIn = Array.ConvertAll(Encoding.ASCII.GetBytes("DisableInfoWinFlashDL" ), q => Convert.ToSByte(q));
            //res = JLINKARM_ExecCommand(acIn, out acOut, 256);

            JLINK_SetFlashProgProgressCallback(_progress);

            
            _UnsecureChip();

            if (forceErase)
            {
                JLINK_EraseChip();
            }

            JLINKARM_Reset();

            JLINKARM_BeginDownload(0);

            res = JLINK_DownloadFile(Array.ConvertAll(Encoding.ASCII.GetBytes(filePath), q => Convert.ToSByte(q)), 0);

            JLINKARM_EndDownload();

            if (res < 0)
            {                
                retval = res;
            }
            else
            {
                JLINKARM_Reset();
                JLINKARM_Go();
                retval = res;
            }
            
            return retval;
        }
    }
}
