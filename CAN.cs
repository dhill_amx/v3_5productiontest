﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using CANL2dotNET;

namespace V3ProductionTest
{
    public class canEvent
    {
        public int type;
        public PARAM_CLASS frame;

        public canEvent(PARAM_CLASS x, int frameType)
        {
            type = frameType;
            frame = new PARAM_CLASS();
            frame.Ident = x.Ident;
            frame.Time = x.Time;
            x.RCV_data.CopyTo(frame.RCV_data, 0);
        }
    }

    public class CanInterfaceSofting
    {
        private int _presc_chan1, _sjw_chan1, _tseg1_chan1, _tseg2_chan1, _sam_chan1;
        private int _presc_chan2, _sjw_chan2, _tseg1_chan2, _tseg2_chan2, _sam_chan2;

        private uint _stdAccCode_chan1, _stdAccMask_chan1, _extdAccCode_chan1, _extdAccMask_chan1;
        private uint _stdAccCode_chan2, _stdAccMask_chan2, _extdAccCode_chan2, _extdAccMask_chan2;

        public CANL2dotNET.CANL2Channel channel1;
        public CANL2dotNET.CANL2Channel channel2;
        
        private bool _canBoardInit;

        private int numChannelsFound = 0;

        public canBaudRates buadRate_chan1;
        public canBaudRates buadRate_chan2;
        
        private uint flowControlTarget_chan1;
        private uint flowControlTarget_chan2;

        public uint FlowControlTarget_chan2
        {
            get { return flowControlTarget_chan2; }
            set { flowControlTarget_chan2 = value; }
        }

        public uint FlowControlTarget_chan1
        {
            get { return flowControlTarget_chan1; }
            set { flowControlTarget_chan1 = value; }
        }

        public enum canBaudRates
        {
            [XmlEnum("1000")]
            _1mBaud = 0,
            [XmlEnum("800")]
            _800kBaud,
            [XmlEnum("500")]
            _500kBaud,
            [XmlEnum("250")]
            _250kBaud,
            [XmlEnum("125")]
            _125kBaud,
            [XmlEnum("100")]
            _100kBaud
        };

        enum canUsbFifoSizes
        {
            _255 = 0,
            _511,
            _1023,
            _2047,
            _4095,
            _8191,
            _16383,
            _32767,
            _65535
        };

        public static Queue<canEvent> canFrames1 = new Queue<canEvent>(50);
        public static Queue<canEvent> canFrames2 = new Queue<canEvent>(50);

        public CanInterfaceSofting()
        {
            //
            // TODO: Add constructor logic here
            //

            _canBoardInit = false;

            //Default Baud Rate is 500Kb
            //setBaudRate(2,1,8,7,0);//250
            setBaudRate(1,canBaudRates._500kBaud);
            setBaudRate(2, canBaudRates._500kBaud);

            //Default Accept Anything
            setAcceptance(1,0, 0, 0, 0);
            setAcceptance(2,0, 0, 0, 0);

            //CANUsb fifo size
            //_fifoSize = canUsbFifoSizes._4095 ;

            channel1 = new CANL2Channel();
            channel1.CANEvent += new CANL2Channel.CANEventDelegate(channel1_CANEvent);
            channel2 = new CANL2Channel();
            channel2.CANEvent += new CANL2Channel.CANEventDelegate(channel2_CANEvent);

            flowControlTarget_chan1 = 0x7E0;
            flowControlTarget_chan2 = 0x7E0;
        }

        
        ~CanInterfaceSofting()
        {
           CloseInterface();
        }
        
        private void channel1_CANEvent(CANL2dotNET.PARAM_CLASS param, int type)
        {
            canFrames1.Enqueue(new canEvent(param, type));
        }

        private void channel2_CANEvent(PARAM_CLASS param, int type)
        {
            canFrames2.Enqueue(new canEvent(param, type));
        }

        public void setBaudRate(int channel,int presc, int sjw, int tseg1, int tseg2, int sample)
        {
            if (channel == 1)
            {
                _presc_chan1 = presc;
                _sjw_chan1 = sjw;
                _tseg1_chan1 = tseg1;
                _tseg2_chan1 = tseg2;
                _sam_chan1 = sample;
            }
            else
            {
                _presc_chan2 = presc;
                _sjw_chan2 = sjw;
                _tseg1_chan2 = tseg1;
                _tseg2_chan2 = tseg2;
                _sam_chan2 = sample;
            }
        }

        public void setBaudRate(int channel,canBaudRates cbr)
        {
            if (channel == 1)
            {
                switch (cbr)
                {
                    case canBaudRates._1mBaud:
                        {
                            _presc_chan1 = 1;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 4;
                            _tseg2_chan1 = 3;
                            _sam_chan1 = 0;
                            break;
                        }
                    case canBaudRates._800kBaud:
                        {
                            _presc_chan1 = 1;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 6;
                            _tseg2_chan1 = 3;
                            _sam_chan1 = 0;
                            break;
                        }
                    case canBaudRates._500kBaud:
                        {
                            _presc_chan1 = 1;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 8;
                            _tseg2_chan1 = 7;
                            _sam_chan1 = 0;
                            break;
                        }
                    case canBaudRates._250kBaud:
                        {
                            _presc_chan1 = 2;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 8;
                            _tseg2_chan1 = 7;
                            _sam_chan1 = 0;
                            break;
                        }
                    case canBaudRates._125kBaud:
                        {
                            _presc_chan1 = 4;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 8;
                            _tseg2_chan1 = 7;
                            _sam_chan1 = 0;
                            break;
                        }
                    case canBaudRates._100kBaud:
                        {
                            _presc_chan1 = 4;
                            _sjw_chan1 = 4;
                            _tseg1_chan1 = 11;
                            _tseg2_chan1 = 8;
                            _sam_chan1 = 0;
                            break;
                        }
                    default:
                        {
                            //500 kBaud
                            _presc_chan1 = 1;
                            _sjw_chan1 = 1;
                            _tseg1_chan1 = 8;
                            _tseg2_chan1 = 7;
                            _sam_chan1 = 0;
                            break;
                        }
                }

                buadRate_chan1 = cbr;
            }
            else
            {
                switch (cbr)
                {
                    case canBaudRates._1mBaud:
                        {
                            _presc_chan2 = 1;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 4;
                            _tseg2_chan2 = 3;
                            _sam_chan2 = 0;
                            break;
                        }
                    case canBaudRates._800kBaud:
                        {
                            _presc_chan2 = 1;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 6;
                            _tseg2_chan2 = 3;
                            _sam_chan2 = 0;
                            break;
                        }
                    case canBaudRates._500kBaud:
                        {
                            _presc_chan2 = 1;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 8;
                            _tseg2_chan2 = 7;
                            _sam_chan2 = 0;
                            break;
                        }
                    case canBaudRates._250kBaud:
                        {
                            _presc_chan2 = 2;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 8;
                            _tseg2_chan2 = 7;
                            _sam_chan2 = 0;
                            break;
                        }
                    case canBaudRates._125kBaud:
                        {
                            _presc_chan2 = 4;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 8;
                            _tseg2_chan2 = 7;
                            _sam_chan2 = 0;
                            break;
                        }
                    case canBaudRates._100kBaud:
                        {
                            _presc_chan2 = 4;
                            _sjw_chan2 = 4;
                            _tseg1_chan2 = 11;
                            _tseg2_chan2 = 8;
                            _sam_chan2 = 0;
                            break;
                        }
                    default:
                        {
                            //500 kBaud
                            _presc_chan2 = 1;
                            _sjw_chan2 = 1;
                            _tseg1_chan2 = 8;
                            _tseg2_chan2 = 7;
                            _sam_chan2 = 0;
                            break;
                        }
                }

                buadRate_chan2 = cbr;
            }
        }

        public void setAcceptance(int channel, uint standardAcceptanceCode, uint standardAcceptanceMask, uint extendedAcceptanceCode, uint extendedAcceptanceMask)
        {
            if (channel == 1)
            {
                _stdAccCode_chan1 = standardAcceptanceCode;
                _stdAccMask_chan1 = standardAcceptanceMask;
                _extdAccCode_chan1 = extendedAcceptanceCode;
                _extdAccMask_chan1 = extendedAcceptanceMask;
            }
            else
            {
                _stdAccCode_chan2 = standardAcceptanceCode;
                _stdAccMask_chan2= standardAcceptanceMask;
                _extdAccCode_chan2 = extendedAcceptanceCode;
                _extdAccMask_chan2 = extendedAcceptanceMask;
            }
        }

        public void CloseInterface()
        {
            try
            {
                if (numChannelsFound == 1)
                {
                    channel1.INIL2_close_channel();
                }
                else
                {
                    channel1.INIL2_close_channel();
                    channel2.INIL2_close_channel();
                }                
            }
            catch (Exception e)
            {
            }
        }

        public int Init()
        {
            bool initComplete;
            int result;

            CHDSNAPSHOT[] snp;
            UInt32 numOfCh = 1, num;
            snp = new CHDSNAPSHOT[1];

            initComplete = false;
            
            _canBoardInit = false;

            try
            {
                if (_canBoardInit == false)
                {
                    do
                    {
                        num = numOfCh;

                        snp = new CHDSNAPSHOT[numOfCh];
                        result = channel1.CANL2_get_all_CAN_channels(ref numOfCh, ref snp);
                    } while ((num < numOfCh) && (result == 0)); // till snp is big enough to hold all channels

//                    frm.textBox1.Text = result.ToString() + " " + numOfCh.ToString() + " " + snp[0].ChannelName + Environment.NewLine;

                    numChannelsFound = (int)numOfCh;

                    try
                    {
                        if (numChannelsFound == 2)
                        {
                            result = channel1.INIL2_close_channel();
                            result = channel2.INIL2_close_channel();
                        }
                        else if (numChannelsFound == 1)
                        {
                            result = channel1.INIL2_close_channel();
                            
                        }
                    }
                    catch (Exception errIgnore)
                    { }

                    result = channel1.INIL2_intitialize_channel(snp[0].ChannelName);
                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                    if (result == 0)
                    {
                        result = channel1.CANL2_reset_chip();
                        //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                        if (result == 0)
                        {
                            if (numOfCh > 1)
                            {
                                result = channel2.INIL2_intitialize_channel(snp[1].ChannelName);
                                //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                if (result == 0)
                                {
                                    result = channel2.CANL2_reset_chip();
                                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                    if (result == 0)
                                    {
                                        _canBoardInit = true;
                                    }
                                    else
                                    {
                                        _canBoardInit = false;
                                    }
                                }
                                else
                                {
                                    _canBoardInit = false;
                                }
                            }
                            else
                            {
                                _canBoardInit = true;
                            }
                            

                        }
                        else
                        {
                            _canBoardInit = false;
                        }
                    }
                    else
                    {
                        _canBoardInit = false;
                        switch (result)
                        {
                            case 0xFE00:
                                {
                                    //Plug Device In
                                    break;
                                }

                            case 0xFE0F:
                                {
                                    //Unplug and Replug Device
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                    }
                }

                if (_canBoardInit == true)
                {
                    result = channel1.CANL2_reset_chip();
                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                    if (result == 0)
                    {
                        result = channel1.CANL2_initialize_chip(_presc_chan1, _sjw_chan1, _tseg1_chan1, _tseg2_chan1, _sam_chan1);
                        //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                        if (result == 0)
                        {
                            result = channel1.CANL2_set_acceptance(_stdAccCode_chan1, _stdAccMask_chan1, _extdAccCode_chan1, _extdAccMask_chan1);
                            //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                            if (result == 0)
                            {
                                
                                result = channel1.CANL2_set_output_control(-1);
                                //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                if (result == 0)
                                {
                                    result = channel1.CANL2_initialize_interface(0, 1, 0, 0, 0, 0, 0, 0, 0, 1);
                                    
                                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                    if (result == 0)
                                    {
                                        //result = CANPC_set_rcv_fifo_size((int)_fifoSize);
                                        result = channel1.CANL2_enable_fifo();
                                        if (result == 0)
                                        {
                                            result = channel1.CANL2_enable_error_frame_detection();
                                            //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                            if (result == 0)
                                            {
                                                result = channel1.CANL2_start_chip();
                                                //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                if (result == 0)
                                                {
                                                    if (numOfCh > 1)
                                                    {
                                                        result = channel2.CANL2_reset_chip();
                                                        //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                        if (result == 0)
                                                        {
                                                            result = channel2.CANL2_initialize_chip(_presc_chan2, _sjw_chan2, _tseg1_chan2, _tseg2_chan2, _sam_chan2);
                                                            //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                            if (result == 0)
                                                            {
                                                                result = channel2.CANL2_set_acceptance(_stdAccCode_chan2, _stdAccMask_chan2, _extdAccCode_chan2, _extdAccMask_chan2);
                                                                //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                                if (result == 0)
                                                                {

                                                                    result = channel2.CANL2_set_output_control(-1);
                                                                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                                    if (result == 0)
                                                                    {
                                                                        result = channel2.CANL2_initialize_interface(0, 1, 0, 0, 0, 0, 0, 0, 0, 1);

                                                                        //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                                        if (result == 0)
                                                                        {
                                                                            //result = CANPC_set_rcv_fifo_size((int)_fifoSize);
                                                                            result = channel2.CANL2_enable_fifo();
                                                                            if (result == 0)
                                                                            {
                                                                                result = channel2.CANL2_enable_error_frame_detection();
                                                                                //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                                                if (result == 0)
                                                                                {
                                                                                    result = channel2.CANL2_start_chip();
                                                                                    //frm.textBox1.Text = frm.textBox1.Text + result.ToString() + Environment.NewLine;
                                                                                    if (result == 0)
                                                                                    {
                                                                                        initComplete = true;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        initComplete = false;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    initComplete = false;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                initComplete = false;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            initComplete = false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        initComplete = false;
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    initComplete = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                initComplete = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            initComplete = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        initComplete = true;
                                                    }                                                           
                                                    
                                                }
                                                else
                                                {
                                                    initComplete = false;
                                                }
                                            }
                                            else
                                            {
                                                initComplete = false;
                                            }
                                        }
                                        else
                                        {
                                            initComplete = false;
                                        }
                                    }
                                    else
                                    {
                                        initComplete = false;
                                    }
                                }
                                else
                                {
                                    initComplete = false;
                                }

                            }
                            else
                            {
                                initComplete = false;
                            }
                        }
                        else
                        {
                            initComplete = false;
                        }
                    }
                    else
                    {
                        initComplete = false;
                    }
                }
                else
                {
                    initComplete = false;
                }

            }
            catch (Exception e)
            {
                initComplete = false;
            }

            finally
            {
                if (initComplete == false)
                {
                    result = channel1.INIL2_close_channel();
                    result = channel2.INIL2_close_channel();
                }
            }

            if (initComplete == true)
            {
                System.Threading.Thread.Sleep(500);
                return 0;

            }
            else
            {
                return -1;
            }
        }

        #region Old Can Stuff
        //public int getResponse(int channel, PARAM_CLASS msg, ref byte[] validResponse)
        //{
        //    return getResponse(channel, msg, ref validResponse, 3);
        //}

        //public int getResponse(int channel,PARAM_CLASS msg,ref byte[] validResponse, int timeoutSeconds)
        //{
        //    DateTime timeout = DateTime.Now.AddSeconds(timeoutSeconds);
        //    int retVal = 0;
        //    int numBytes = 0;
        //    int i, datapointer;

        //    PARAM_CLASS fullMsg = new PARAM_CLASS();
            

        //    while (DateTime.Now.CompareTo(timeout) < 0) 
        //    {
        //        retVal = getCanFrame(channel,msg);

        //        if (retVal == 1)
        //        {
        //            if ((msg.RCV_data[0] & 0xF0) == 0x10)
        //            {
        //                if (channel == 1)
        //                {
        //                    retVal = channel1.CANL2_send_data(flowControlTarget_chan1, 0, 8, new byte[8] { 0x30, 0x00, 0x10, 0, 0, 0, 0, 0 });
        //                }
        //                else
        //                {
        //                    retVal = channel2.CANL2_send_data(flowControlTarget_chan2, 0, 8, new byte[8] { 0x30, 0x00, 0x10, 0, 0, 0, 0, 0 });
        //                }
        //                numBytes = ((msg.RCV_data[0] & 0x0f) << 8) + msg.RCV_data[1];
        //                validResponse = new byte[numBytes];

        //                for (int c = 2; c < 8; c++)
        //                {
        //                    validResponse[c - 2] = msg.RCV_data[c];
        //                }
        //                datapointer = 6;

        //                for (int cnt = 0; cnt < Math.Ceiling((decimal)(numBytes - 6) / 7); cnt++)
        //                {
        //                    DateTime dt = DateTime.Now.AddMilliseconds(1000);

        //                    do
        //                    {
        //                        retVal = getCanFrame(channel, msg);
        //                        if (retVal == 1 || retVal == 9) break;
        //                        System.Windows.Forms.Application.DoEvents();
        //                    } while (DateTime.Now.CompareTo(dt) <= 0);

        //                    if (retVal == 1 || retVal == 9)
        //                    {
        //                        for (int c = 1; c < 8; c++)
        //                        {
        //                            if (datapointer < numBytes)
        //                            {
        //                                validResponse[datapointer++] = msg.RCV_data[c];
        //                            }
        //                            else
        //                            {
        //                                break;
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        break;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                validResponse = new byte[msg.RCV_data[0]];
        //                for (int c = 1; c <= msg.RCV_data[0]; c++)
        //                {
        //                    validResponse[c - 1] = msg.RCV_data[c];
        //                }
        //                //msg.RCV_data.CopyTo(wholeMsg,  1);
        //            }
        //            break;
        //        }
        //        else
        //        {
        //            if (retVal != 0)
        //            {
        //                System.Diagnostics.Debug.WriteLine(retVal.ToString());
        //            }
        //        }

        //    }
        //    return retVal;
        //}
        
        //public int getCanFrame(int channel, PARAM_CLASS msg)
        //{
        //    int retVal;
        //    retVal = 0;
        //    canEvent x;
        //    try
        //    {
        //        if (channel == 1)
        //        {
        //            if (canFrames1.Count > 0)
        //            {
        //                x = canFrames1.Dequeue();
        //                retVal = x.type;
        //                msg.Ident = x.frame.Ident;
        //                x.frame.RCV_data.CopyTo(msg.RCV_data, 0);
        //                msg.Time = x.frame.Time;
        //                System.Diagnostics.Debug.WriteLine("Rcv\t" + msg.Ident.ToString("X") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2"));
        //            }
        //        }
        //        else
        //        {
        //            if (canFrames2.Count > 0)
        //            {
        //                x = canFrames2.Dequeue();
        //                retVal = x.type;
        //                msg.Ident = x.frame.Ident;
        //                x.frame.RCV_data.CopyTo(msg.RCV_data, 0);
        //                msg.Time = x.frame.Time;
        //                System.Diagnostics.Debug.WriteLine("Rcv\t" + msg.Ident.ToString("X") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2"));
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        retVal = -1;
        //    }

        //    return retVal;

        //}

        //public int sendCanFrame(int channel,uint ident, int Xtd, int DataLength, byte[] data)
        //{
        //    int retVal = 0;
            
        //    try
        //    {
        //        System.Threading.Thread.Sleep(10);

        //        System.Windows.Forms.Application.DoEvents();

        //        System.Diagnostics.Debug.WriteLine("Sent\t" + ident.ToString("X") + " , " + data[0].ToString("X2") + " , " + data[1].ToString("X2") + " , " + data[2].ToString("X2") + " , " + data[3].ToString("X2") + " , " + data[4].ToString("X2") + " , " + data[5].ToString("X2") + " , " + data[6].ToString("X2") + " , " + data[7].ToString("X2") + " " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString());

        //        if (channel == 1)
        //        {
        //            retVal = channel1.CANL2_send_data(ident, Xtd, DataLength, data);
        //        }
        //        else
        //        {
        //            retVal = channel2.CANL2_send_data(ident, Xtd, DataLength, data);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        retVal = -1;
        //    }

        //    return retVal;

        //}

        //public int sendCanFrame(int channel,uint ident, int Xtd, string data)
        //{
        //    int retVal = 0;
        //    byte[] d = new byte[8];
        //    int dataPointer = 0;
        //    int i, numCF, DataLength;            
        //    byte c;
        //    PARAM_CLASS canMsg = new PARAM_CLASS();

        //    DataLength = data.Length / 2;

        //    if (DataLength <= 7)
        //    {
        //        d[0] = (byte)DataLength;
        //        for (i = 1; i <= DataLength; i++)
        //        {
        //            d[i] = Byte.Parse(data.Substring((i-1) * 2, 2), System.Globalization.NumberStyles.HexNumber);
        //        }

        //        retVal = sendCanFrame(channel,ident, Xtd, 8, d);
        //    }
        //    else
        //    {
        //        d[0] = 0x10;
        //        d[1] = (byte)DataLength;
        //        for (i = 2; i < 8; i++)
        //        {
        //            d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
        //            dataPointer++;
        //        }
        //        retVal = sendCanFrame(channel,ident, Xtd, 8, d);
        //        if (retVal == 0)
        //        {
        //            DateTime dt = DateTime.Now.AddMilliseconds(1000);

        //            do
        //            {
        //                retVal = getCanFrame(channel,canMsg);
        //                if (retVal == 1 || retVal == 9) break;
        //                System.Windows.Forms.Application.DoEvents();
        //            } while (DateTime.Now.CompareTo(dt) <= 0);
                                     
        //            if (retVal == 1 || retVal == 9)
        //            {
        //                if (canMsg.RCV_data[0] == 0x30)
        //                {
        //                    numCF = (DataLength - 7) / 7;
        //                    if ((numCF * 7) + 6 < DataLength) numCF++;
        //                    for (c = 0; c < numCF; c++)
        //                    {
        //                        d[0] = (byte)(0x21 + (byte)c);

        //                        for (i = 1; i < 8; i++)
        //                        {
        //                            if (dataPointer < DataLength)
        //                            {
        //                                d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
        //                                dataPointer++;
        //                            }
        //                            else
        //                            {
        //                                break;
        //                            }
        //                        }
        //                        retVal = sendCanFrame(channel,ident, Xtd, DataLength, d);
        //                        if (retVal != 0) break;
        //                        System.Threading.Thread.Sleep(canMsg.RCV_data[2]);
        //                    }
        //                }
        //                else
        //                {
        //                    retVal = -2;
        //                }
        //            }
        //            else
        //            {
        //                retVal = -3;
        //            }
        //        }
        //    }


        //    return retVal;

        //}

        #endregion Old Can Stuff

        public int getResponseChannel1(PARAM_CLASS msg, ref byte[] validResponse)
        {
            return getResponseChannel1(msg, ref validResponse, 3);
        }

        public int getResponseChannel1( PARAM_CLASS msg, ref byte[] validResponse, int timeoutSeconds)
        {
            DateTime timeout = DateTime.Now.AddSeconds(timeoutSeconds);
            int retVal = 0;
            int numBytes = 0;
            int i, datapointer;

            PARAM_CLASS fullMsg = new PARAM_CLASS();


            while (DateTime.Now.CompareTo(timeout) < 0)
            {
                retVal = getCanFrameChannel1( msg);

                if (retVal == 1)
                {
                    if ((msg.RCV_data[0] & 0xF0) == 0x10)
                    {
                        retVal = channel1.CANL2_send_data(flowControlTarget_chan1, 0, 8, new byte[8] { 0x30, 0x00, 0x10, 0, 0, 0, 0, 0 });
                        
                        numBytes = ((msg.RCV_data[0] & 0x0f) << 8) + msg.RCV_data[1];
                        validResponse = new byte[numBytes];

                        for (int c = 2; c < 8; c++)
                        {
                            validResponse[c - 2] = msg.RCV_data[c];
                        }
                        datapointer = 6;

                        for (int cnt = 0; cnt < Math.Ceiling((decimal)(numBytes - 6) / 7); cnt++)
                        {
                            DateTime dt = DateTime.Now.AddMilliseconds(1000);

                            do
                            {
                                retVal = getCanFrameChannel1(msg);
                                if (retVal == 1 || retVal == 9) break;
                                System.Windows.Forms.Application.DoEvents();
                            } while (DateTime.Now.CompareTo(dt) <= 0);

                            if (retVal == 1 || retVal == 9)
                            {
                                for (int c = 1; c < 8; c++)
                                {
                                    if (datapointer < numBytes)
                                    {
                                        validResponse[datapointer++] = msg.RCV_data[c];
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            validResponse = new byte[msg.RCV_data[0]];
                            for (int c = 1; c <= msg.RCV_data[0]; c++)
                            {
                                validResponse[c - 1] = msg.RCV_data[c];
                            }
                        }
                        catch (Exception err)
                        { }
                        //msg.RCV_data.CopyTo(wholeMsg,  1);
                    }
                    break;
                }
                else
                {
                    if (retVal != 0)
                    {
                        System.Diagnostics.Debug.WriteLine(retVal.ToString());
                        this.CloseInterface();
                        this.Init();
                    }
                }

            }
            return retVal;
        }

        public int getCanFrameChannel1(PARAM_CLASS msg)
        {
            int retVal;
            retVal = 0;
            canEvent x;
            try
            {
                
                if (canFrames1.Count > 0)
                {
                    x = canFrames1.Dequeue();
                    retVal = x.type;
                    msg.Ident = x.frame.Ident;
                    x.frame.RCV_data.CopyTo(msg.RCV_data, 0);
                    msg.Time = x.frame.Time;
                    System.Diagnostics.Debug.WriteLine("Rcv\t" + msg.Ident.ToString("X") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2") + " " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString());
                }
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retVal = -1;
            }

            return retVal;

        }

        public int sendCanFrameChannel1(uint ident, int Xtd, int DataLength, byte[] data)
        {
            int retVal = 0;

            try
            {
                System.Threading.Thread.Sleep(10);

                System.Windows.Forms.Application.DoEvents();

                System.Diagnostics.Debug.WriteLine("Sent\t" + ident.ToString("X") + " , " + data[0].ToString("X2") + " , " + data[1].ToString("X2") + " , " + data[2].ToString("X2") + " , " + data[3].ToString("X2") + " , " + data[4].ToString("X2") + " , " + data[5].ToString("X2") + " , " + data[6].ToString("X2") + " , " + data[7].ToString("X2") + " " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString());

                canFrames1.Clear();
                retVal = channel1.CANL2_send_data(ident, Xtd, DataLength, data);                

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retVal = -1;
            }

            return retVal;

        }

        public int sendCanFrameChannel1(uint ident, int Xtd, string data)
        {
            int retVal = 0;
            byte[] d = new byte[8];
            int dataPointer = 0;
            int i, numCF, DataLength;
            byte c;
            PARAM_CLASS canMsg = new PARAM_CLASS();
            int wait = 0;
            DataLength = data.Length / 2;

            //retVal = this.channel1.CANL2_reset_rcv_fifo();
            //canFrames1.Clear();

            if (DataLength <= 7)
            {
                for (i = 0; i < 8; i++)
                {
                    d[i] = 0;
                }

                d[0] = (byte)DataLength;
                for (i = 1; i <= DataLength; i++)
                {
                    d[i] = Byte.Parse(data.Substring((i - 1) * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }

                retVal = sendCanFrameChannel1(ident, Xtd, 8, d);
            }
            else
            {
                d[0] = 0x10;
                d[1] = (byte)DataLength;
                for (i = 2; i < 8; i++)
                {
                    d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    dataPointer++;
                }
                retVal = sendCanFrameChannel1(ident, Xtd, 8, d);
                if (retVal == 0)
                {
                    DateTime dt = DateTime.Now.AddMilliseconds(1000);

                    do
                    {
                        retVal = getCanFrameChannel1(canMsg);
                        if (retVal == 1 || retVal == 9) break;
                        System.Windows.Forms.Application.DoEvents();
                    } while (DateTime.Now.CompareTo(dt) <= 0);

                    if (retVal == 1 || retVal == 9)
                    {
                        if (canMsg.RCV_data[0] == 0x30)
                        {
                            numCF = (DataLength - 7) / 7;
                            wait = canMsg.RCV_data[2];
                            wait = 50;
                            if ((numCF * 7) + 6 < DataLength) numCF++;
                            for (c = 0; c < numCF; c++)
                            {
                                for (i = 0; i < 8; i++)
                                {
                                    d[i] = 0;
                                }

                                d[0] = (byte)(0x21 + (byte)c);

                                for (i = 1; i < 8; i++)
                                {
                                    if (dataPointer < DataLength)
                                    {
                                        d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
                                        dataPointer++;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                System.Threading.Thread.Sleep(wait);

                                retVal = sendCanFrameChannel1(ident, Xtd, 8, d);
                                if (retVal != 0) break;
                                
                                //System.Threading.Thread.Sleep(100);
                            }
                        }
                        else
                        {
                            retVal = -2;
                        }
                    }
                    else
                    {
                        retVal = -3;
                    }
                }
            }


            return retVal;

        }


        public int getResponseChannel2(PARAM_CLASS msg, ref byte[] validResponse)
        {
            return getResponseChannel2(msg, ref validResponse, 3);
        }

        public int getResponseChannel2(PARAM_CLASS msg, ref byte[] validResponse, int timeoutSeconds)
        {
            DateTime timeout = DateTime.Now.AddSeconds(timeoutSeconds);
            int retVal = 0;
            int numBytes = 0;
            int i, datapointer;

            PARAM_CLASS fullMsg = new PARAM_CLASS();


            while (DateTime.Now.CompareTo(timeout) < 0)
            {
                retVal = getCanFrameChannel2(msg);

                if (retVal == 1)
                {
                    if ((msg.RCV_data[0] & 0xF0) == 0x10)
                    {
                        retVal = channel2.CANL2_send_data(flowControlTarget_chan2, 0, 8, new byte[8] { 0x30, 0x00, 0x10, 0, 0, 0, 0, 0 });
                        
                        numBytes = ((msg.RCV_data[0] & 0x0f) << 8) + msg.RCV_data[1];
                        validResponse = new byte[numBytes];

                        for (int c = 2; c < 8; c++)
                        {
                            validResponse[c - 2] = msg.RCV_data[c];
                        }
                        datapointer = 6;

                        for (int cnt = 0; cnt < Math.Ceiling((decimal)(numBytes - 6) / 7); cnt++)
                        {
                            DateTime dt = DateTime.Now.AddMilliseconds(1000);

                            do
                            {
                                retVal = getCanFrameChannel2( msg);
                                if (retVal == 1 || retVal == 9) break;
                                System.Windows.Forms.Application.DoEvents();
                            } while (DateTime.Now.CompareTo(dt) <= 0);

                            if (retVal == 1 || retVal == 9)
                            {
                                for (int c = 1; c < 8; c++)
                                {
                                    if (datapointer < numBytes)
                                    {
                                        validResponse[datapointer++] = msg.RCV_data[c];
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            validResponse = new byte[msg.RCV_data[0]];
                            for (int c = 1; c <= msg.RCV_data[0]; c++)
                            {
                                validResponse[c - 1] = msg.RCV_data[c];
                            }
                        }
                        catch (Exception err)
                        { }                        
                        //msg.RCV_data.CopyTo(wholeMsg,  1);
                    }
                    break;
                }
                else
                {
                    if (retVal != 0)
                    {
                        System.Diagnostics.Debug.WriteLine(retVal.ToString());
                    }
                }

            }
            return retVal;
        }

        public int getCanFrameChannel2(PARAM_CLASS msg)
        {
            int retVal;
            retVal = 0;
            canEvent x;
            try
            {
                if (canFrames2.Count > 0)
                    {
                        x = canFrames2.Dequeue();
                        retVal = x.type;
                        msg.Ident = x.frame.Ident;
                        x.frame.RCV_data.CopyTo(msg.RCV_data, 0);
                        msg.Time = x.frame.Time;
                        System.Diagnostics.Debug.WriteLine("Rcv\t" + msg.Ident.ToString("X") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2") + " " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString());
                    }
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retVal = -1;
            }

            return retVal;

        }

        public int sendCanFrameChannel2(uint ident, int Xtd, int DataLength, byte[] data)
        {
            int retVal = 0;

            try
            {
                System.Threading.Thread.Sleep(10);

                System.Windows.Forms.Application.DoEvents();

                System.Diagnostics.Debug.WriteLine("Sent\t" + ident.ToString("X") + " , " + data[0].ToString("X2") + " , " + data[1].ToString("X2") + " , " + data[2].ToString("X2") + " , " + data[3].ToString("X2") + " , " + data[4].ToString("X2") + " , " + data[5].ToString("X2") + " , " + data[6].ToString("X2") + " , " + data[7].ToString("X2") + " " + DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString());

                canFrames2.Clear();
                retVal = channel2.CANL2_send_data(ident, Xtd, DataLength, data);
                

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retVal = -1;
            }

            return retVal;

        }

        public int sendCanFrameChannel2(uint ident, int Xtd, string data)
        {
            int retVal = 0;
            byte[] d = new byte[8];
            int dataPointer = 0;
            int i, numCF, DataLength;
            byte c;
            PARAM_CLASS canMsg = new PARAM_CLASS();
            int wait = 0;
            DataLength = data.Length / 2;

            //retVal = this.channel2.CANL2_reset_rcv_fifo();
            //canFrames2.Clear();

            if (DataLength <= 7)
            {
                for (i = 0; i < 8; i++)
                {
                    d[i] = 0;
                }

                d[0] = (byte)DataLength;
                for (i = 1; i <= DataLength; i++)
                {
                    d[i] = Byte.Parse(data.Substring((i - 1) * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }

                retVal = sendCanFrameChannel2( ident, Xtd, 8, d);
            }
            else
            {
                d[0] = 0x10;
                d[1] = (byte)DataLength;
                for (i = 2; i < 8; i++)
                {
                    d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
                    dataPointer++;
                }
                retVal = sendCanFrameChannel2( ident, Xtd, 8, d);
                if (retVal == 0)
                {
                    DateTime dt = DateTime.Now.AddMilliseconds(1000);

                    do
                    {
                        retVal = getCanFrameChannel2(canMsg);
                        if (retVal == 1 || retVal == 9) break;
                        System.Windows.Forms.Application.DoEvents();
                    } while (DateTime.Now.CompareTo(dt) <= 0);

                    if (retVal == 1 || retVal == 9)
                    {
                        if (canMsg.RCV_data[0] == 0x30)
                        {
                            numCF = (DataLength - 7) / 7;
                            wait = canMsg.RCV_data[2];
                            wait = 50;
                            if ((numCF * 7) + 6 < DataLength) numCF++;
                            for (c = 0; c < numCF; c++)
                            {
                                for (i = 0; i < 8; i++)
                                {
                                    d[i] = 0;
                                }

                                d[0] = (byte)(0x21 + (byte)c);

                                for (i = 1; i < 8; i++)
                                {
                                    if (dataPointer < DataLength)
                                    {
                                        d[i] = Byte.Parse(data.Substring(dataPointer * 2, 2), System.Globalization.NumberStyles.HexNumber);
                                        dataPointer++;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                System.Threading.Thread.Sleep(wait);
                                retVal = sendCanFrameChannel2( ident, Xtd, 8, d);
                                if (retVal != 0) break;
                                
                            }
                        }
                        else
                        {
                            retVal = -2;
                        }
                    }
                    else
                    {
                        retVal = -3;
                    }
                }
            }


            return retVal;

        }
    }
}
