﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Globalization;
using System.ComponentModel;
using System.IO;

namespace V3ProductionTest
{
    public class Tests
    {
        public JigControl _jig;
        public SoftwareCommands _sc;
        //private int _canChannel;
        private int _jigIdent;

        public int JigIdent
        {
            get { return _jigIdent; }            
        }

        private double _currentTestA = -1.0;
        
        private double _voltSwitchUp = 0.0;
        private double _voltSwitchDown = 0.0;
        
        private int _adLow = 0;
        private int _adMid = 0;
        private int _adHigh = 0;
        
        private string _imei = "";

        private string _imsi = "";

        private string _modemType = "";

        private string _modemFirmwareVersion = "";

        private string _phoneNumber = "";

        private string _gpsStatus = "";

        private double _gpsVoltageReading = 0.0;

        private string _ictPcbId = "";

        private string _ictTestDate = "";

        private int _ictPassFailFlag = 0;

        private int _ictFailCount = 0;

        public enum TestID
        {
            [XmlEnum("CurrentDrawA")]
            CurrentDrawA = 0,
            [XmlEnum("TestRouteValidation")]
            TestRouteValidation,
            [XmlEnum("BatterySwitchingPoint")]
            BatterySwitchingPoint,
            [XmlEnum("LatchRemoved")]
            LatchRemoved,
            [XmlEnum("BatteryADReadings")]
            BatteryADReadings,
            [XmlEnum("SetRealTimeClock")]
            SetRealTimeClock,
            [XmlEnum("FullFlashErase")]
            FullFlashErase,
            [XmlEnum("FlashTestRoutine")]
            FlashTestRoutine,
            [XmlEnum("LoadDefaultConfiguration")]
            LoadDefaultConfiguration,
            [XmlEnum("ReadIMEI")]
            readIMEI,            
            [XmlEnum("ReadIMSI")]
            ReadIMSI,
            [XmlEnum("ReadModemType")]
            ReadModemType,
            [XmlEnum("CheckModemFirmwareVersion")]
            CheckModemFirmwareVersion,
            [XmlEnum("ReadPhoneNumber")]
            ReadPhoneNumber,
            [XmlEnum("GPSStatus")]
            GPSStatus,
            [XmlEnum("GPSVoltage")]
            GPSVoltage,
            [XmlEnum("KLineVoltageNoPullup")]
            KLineVoltageNoPullup,
            [XmlEnum("KLineVoltageWithPullup")]
            KLineVoltageWithPullup,
            [XmlEnum("KLineCommsTest")]
            KLineCommsTest,
            [XmlEnum("IODriverTest")]
            IODriverTest,
            [XmlEnum("IgnitionLine")]
            IgnitionLine,
            [XmlEnum("AuxPowerSupply")]
            AuxPowerSupply,
            [XmlEnum("Accelerometer")]
            Accelerometer,
            [XmlEnum("ScanSimNumber")]
            ScanSimNumber,
            [XmlEnum("PrintLabel")]
            PrintLabel,
            [XmlEnum("ScanLabel")]
            ScanLabel,
            [XmlEnum("StoreParameters")]
            StoreParameters,
            [XmlEnum("LoadTestFirmware")]
            LoadTestFirmware,
            [XmlEnum("GPSNumSats")]
            GPSNumSats,
            [XmlEnum("LoadAppFirmware")]
            LoadAppFirmware,
            [XmlEnum("readIctPcbID")]
            ReadIctPcbID,
            [XmlEnum("readIctPassFail")]
            ReadIctPassFail,
            [XmlEnum("readIctFailCount")]
            ReadIctFailCount,
            [XmlEnum("readIctTestDate")]
            ReadIctTestDate,
            
            [XmlEnum("verifyIctPcbID")]
            VerifyIctPcbID,
            [XmlEnum("verifyIctPassFail")]
            VerifyIctPassFail,
            [XmlEnum("verifyIctFailCount")]
            VerifyIctFailCount,
            [XmlEnum("verifyIctTestDate")]
            VerifyIctTestDate,

            [XmlEnum("readAteFailCountFromFlash")]
            ReadAteFailCountFromFlash,

            [XmlEnum("verifyAtePassFail")]
            VerifyAtePassFail,
            [XmlEnum("verifyAteFailCount")]
            VerifyAteFailCount,
            [XmlEnum("verifyAteTestDate")]
            VerifyAteTestDate,

            [XmlEnum("writeIctPcbID")]
            WriteIctPcbID,
            [XmlEnum("writeIctPassFail")]
            WriteIctPassFail,
            [XmlEnum("writeIctFailCount")]
            WriteIctFailCount,
            [XmlEnum("writeIctTestDate")]
            WriteIctTestDate,
            [XmlEnum("writeAtePassFail")]
            WriteAtePassFail,
            [XmlEnum("writeAteFailCount")]
            WriteAteFailCount,
            [XmlEnum("writeAteTestDate")]
            WriteAteTestDate,
            [XmlEnum("verifyIMEI")]
            VerifyIMEI,
            [XmlEnum("verifyIMSI")]
            VerifyIMSI,
            [XmlEnum("verifyPhonenumber")]
            VerifyPhonenumber,
            [XmlEnum("verifyHWVariant")]
            VerifyHWVariant,
            [XmlEnum("verifySIMnumber")]
            VerifySimNumber,
            [XmlEnum("verifySmsServerPhoneNumber")]
            VerifySmsServerPhoneNumber,
            [XmlEnum("verifyGprsPort")]
            VerifyGprsPort,
            [XmlEnum("verifyGprsServerIP")]
            VerifyGprsIP,
            [XmlEnum("verifyGprsAPN")]
            VerifyGprsAPN,
            [XmlEnum("verifyGprsUserName")]
            VerifyGprsUserName,
            [XmlEnum("verifyGprsPassword")]
            VerifyGprsPassword,
            [XmlEnum("verifyPartNumber")]
            VerifyPartNumber,
            [XmlEnum("verifyADLow")]
            VerifyAdLow,
            [XmlEnum("verifyADMid")]
            VerifyAdMid,
            [XmlEnum("verifyADHigh")]
            VerifyAdHigh,
            [XmlEnum("verifyFirmwareID")]
            VerifyFirmwareID,
            [XmlEnum("verifySmsOnly")]
            VerifySmsOnly,
            [XmlEnum("verifyBootLoaderVersion")]
            VerifyBootLoaderVersion,
            [XmlEnum("verifyFirmwareFileName")]
            VerifyFirmwareFileName,
            [XmlEnum("writeIMEI")]
            WriteIMEI,
            [XmlEnum("writeIMSI")]
            WriteIMSI,
            [XmlEnum("writeSIMnumber")]
            WriteSimNumber,
            [XmlEnum("writePhoneNumber")]
            WritePhoneNumber,
            [XmlEnum("writeHwNumber")]
            WriteHwNumber,
            [XmlEnum("writeSmsServerPhoneNumber")]
            WriteSmsServerPhoneNumber,
            [XmlEnum("writeGprsPort")]
            WriteGprsPort,
            [XmlEnum("writeGprsServerIP")]
            WriteGprsIP,
            [XmlEnum("writeGprsAPN")]
            WriteGprsAPN,
            [XmlEnum("writeGprsUserName")]
            WriteGprsUserName,
            [XmlEnum("writeGprsPassword")]
            WriteGprsPassword,
            [XmlEnum("writePartNumber")]
            WritePartNumber,
            [XmlEnum("writeADLow")]
            WriteAdLow,
            [XmlEnum("writeADMid")]
            WriteAdMid,
            [XmlEnum("writeADHigh")]
            WriteAdHigh,
            [XmlEnum("writeSmsOnly")]
            WriteSmsOnly,
            [XmlEnum("writeFirmwareID")]
            WriteFirmwareID,
            [XmlEnum("writeBootLoaderVersion")]
            WriteBootLoaderVersion,
            [XmlEnum("writeFirmwareFileName")]
            WriteFirmwareFileName,
            [XmlEnum("readProdFirmwareVersion")]
            ReadProdFirmwareVersion,
            [XmlEnum("readProcessorVersion")]
            ReadProcessorVersion,
            [XmlEnum("currentDrawSleep")]
            SleepCurrent,
            [XmlEnum("readPcbVariant")]
            ReadPcbVariant,
            [XmlEnum("writePcbVariant")]
            WritePcbVariant,
            [XmlEnum("verifyPcbVariant")]
            VerifyPcbVariant,
            [XmlEnum("writeVehicleVariant")]
            WriteVehicleVariant,
            [XmlEnum("verifyVehicleVariant")]
            VerifyVehicleVariant,
            [XmlEnum("writeIOConfig")]
            WriteIOConfig,

            [XmlEnum("writeServerTelNumberSecondary")]
            WriteServerTelNumberSecondary,
            [XmlEnum("writeGprsPrimaryServer")]
            WriteGprsPrimaryServer,
            [XmlEnum("writeGprsSecondaryServer")]            
            WriteGprsSecondaryServer,
            [XmlEnum("writeGprsEngineeringServer")]            
            WriteGprsEngineeringServer,
            [XmlEnum("writeGprsAUXServer")]            
            WriteGprsAUXServer,
            [XmlEnum("verifyServerTelNumberSecondary")]            
            VerifyServerTelNumberSecondary,
            [XmlEnum("verifyGprsPrimaryServer")]            
            VerifyGprsPrimaryServer,
            [XmlEnum("verifyGprsSecondaryServer")]            
            VerifyGprsSecondaryServer,
            [XmlEnum("verifyGprsEngineeringServer")]            
            VerifyGprsEngineeringServer,
            [XmlEnum("verifyGprsAUXServer")]            
            VerifyGprsAUXServer,

            [XmlEnum("verifyIOConfig")]
            VerifyIOConfig,
            [XmlEnum("readGyroID")]
            readGyroID,
            [XmlEnum("readGyroData")]
            readGyroData,
            [XmlEnum("writeVehicleFlags")]
            WriteVehicleFlags,
            [XmlEnum("verifyVehicleFlags")]
            VerifyVehicleFlags,
            [XmlEnum("LoadTestFirmwareJlink")]
            LoadTestFirmwareJlink,
            [XmlEnum("LoadAppFirmwareJlink")]
            LoadAppFirmwareJlink,
            [XmlEnum("CrankTest")]
            CrankTest,
            [XmlEnum("Can2RtuTest")]
            Can2RtuTest,
            [XmlEnum("Can2PassThruRtuTest")]
            Can2PassThruRtuTest,
            [XmlEnum("SendUdp")]
            SendUdp,
            [XmlEnum("EraseEEprom")]
            EraseEEprom,
            [XmlEnum("SDcardCheck")]
            SDcardCheck,
            [XmlEnum("LoadWhiteList")]
            LoadWhiteList,
            [XmlEnum("VerifyWhiteList")]
            VerifyWhiteList
        };

        public Tests(JigControl jig, int jigIdent, SoftwareCommands sc)
        {
            _jig = jig;
            _sc = sc;
            //_canChannel = canChannel;
            _jigIdent = jigIdent;
        }

        public bool currentDraw_A(double powerupVoltage, double currentLimit, double LL, double UL, out double result)
        {
            bool retVal = false;
            bool temp = false;
            double curr = 0.0;

            _jig.setPowerSupply(powerupVoltage,currentLimit);
            Thread.Sleep(1000);
            _jig.setPowerSupplyState(true);
            Thread.Sleep(1000); 
            
            //_jig.set10R( true);
            //Thread.Sleep(100);
            //_jig.set10RDMM( true);

            _jig.setVbatt(true);

            Thread.Sleep(1000);
            for (int count = 0; count < 3; count++)
            {
                temp = _jig.measureCurrent(ref curr);
                if (temp == true)
                {
                    break;
                }
                Thread.Sleep(100);
            }

            if (temp == true)
            {
                if (curr >= LL && curr <= UL)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                curr= -1.0;
            }
            //_jig.set10R( false);
            //_jig.set10RDMM( false);
            _jig.setVbatt(false);

            result = curr;

            return retVal;
        }

        public bool currentDraw_Sleep(double LL, double UL, out double result)
        {
            bool retVal = false;
            bool temp = false;
            double curr = 0.0;

            _sc.sendUnitToSleep();
            
            Thread.Sleep(1000);
            for (int count = 0; count < 3; count++)
            {
                temp = _jig.measureCurrent(ref curr);
                if (temp == true)
                {
                    break;
                }
                Thread.Sleep(100);
            }

            if (temp == true)
            {
                if (curr >= LL && curr <= UL)
                {
                   retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                curr = -1.0;
            }



            _jig.setVbatt(false);
            Thread.Sleep(100);
            _jig.setVbatt( true);
            Thread.Sleep(250);

            result = curr;

            return retVal;
        }

        public bool batterySwitchingPoint(double powerupVoltage, double currentLimit, double rampToVoltage, double rampIncrement, double upLL, double upUL, double downLL, double downUL, out double upValue, out double downValue)
        {
            bool retVal = false;
            int temp = 0;

            int status = 0;

            _jig.set10R( false);
            _jig.set10RDMM( false);
            _jig.setPowerSupply(powerupVoltage, currentLimit);
            _jig.setPowerSupplyState(true);
            _jig.setVbatt( true);
            
            Thread.Sleep(100);

            for (int count = 0; count < 3; count++)
            {
                temp=_sc.readBatterySwitchStatus(ref status);
                if (temp > 0)
                {
                    break;
                }
                Thread.Sleep(100);
            }

            if (temp > 0)
            {
                if (status == 1)
                {
                    int count = 0;
                    int result = 0;
                    double x = 0.0;
                    for ( x = powerupVoltage; x <= rampToVoltage; x += rampIncrement)
                    {
                        _jig.setPowerSupplyVoltage(x);
                        Application.DoEvents();
                        Thread.Sleep(100);

                        for (count = 0; count < 3; count++)
                        {
                            result = _sc.readBatterySwitchStatus( ref status);
                            if (result>0)break;
                        }

                        if (result > 0)
                        {
                            if (status == 0)
                            {
                                break;
                            }
                            else
                            {
                                //continue with increment
                            }
                        }
                        else
                        {
                            break;                            
                        }
                    }

                    _voltSwitchUp = x;

                    if (result > 0 && status == 0 && x >= upLL && x <= upUL)
                    {
                        for (x = rampToVoltage; x >= powerupVoltage; x -= rampIncrement)
                        {
                            _jig.setPowerSupplyVoltage(x);
                            Application.DoEvents();
                            Thread.Sleep(100);

                            for (count = 0; count < 3; count++)
                            {
                                result = _sc.readBatterySwitchStatus( ref status);
                                if (result > 0) break;
                            }

                            if (result > 0)
                            {
                                if (status == 1)
                                {
                                    break;
                                }
                                else
                                {
                                    //continue with increment
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        _voltSwitchDown = x;

                        if (result > 0 && status == 1 && x >= downLL && x <= downUL)
                        {
                            _jig.setPowerSupplyVoltage(rampToVoltage);
                            Thread.Sleep(100);
                            retVal = true;
                        }
                        else
                        {
                            retVal = false;
                        }
                    }
                    else
                    {
                        retVal = false;
                    }
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }

            upValue = _voltSwitchUp;
            downValue = _voltSwitchDown;

            return retVal;
        }

        public bool latchRemoved(double powerupVoltage, double currentLimit, double rampToVoltage, double rampIncrement,  out double upValue)
        {
            bool retVal = false;
            int temp = 0;

            int status = 0;

            _jig.set10R( false);
            _jig.set10RDMM( false);
            _jig.setPowerSupply(powerupVoltage, currentLimit);
            _jig.setPowerSupplyState(true);
            _jig.setVbatt( true);

            Thread.Sleep(100);

            for (int count = 0; count < 3; count++)
            {
                temp = _sc.readBatterySwitchStatus(ref status);
                if (temp > 0)
                {
                    break;
                }
                Thread.Sleep(100);
            }

            if (temp > 0)
            {
                if (status == 1)
                {
                    int count = 0;
                    int result = 0;
                    double x = 0.0;
                    for (x = powerupVoltage; x <= rampToVoltage; x += rampIncrement)
                    {
                        _jig.setPowerSupplyVoltage(x);
                        Application.DoEvents();
                        Thread.Sleep(100);

                        for (count = 0; count < 3; count++)
                        {
                            result = _sc.readBatterySwitchStatus(ref status);
                            if (result > 0) break;
                        }

                        if (result > 0)
                        {
                            if (status == 0)
                            {
                                break;
                            }
                            else
                            {
                                //continue with increment
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    _voltSwitchUp = x;

                    if (result > 0 && status == 1 && x >= rampToVoltage)
                    {                        
                        _jig.setPowerSupplyVoltage(rampToVoltage);
                        Thread.Sleep(100);
                        retVal = true;
                        
                    }
                    else
                    {
                        retVal = false;
                    }
                }
                else
                {
                    retVal = false;
                }

                upValue = _voltSwitchUp;
            }
            else
            {
                upValue = temp;
                retVal = false;
            }

            
            

            return retVal;
        }

        public bool batteryADReadings(double currentLimit, double testVoltage1, double LL1, double UL1, double testVoltage2, double LL2, double UL2, double testVoltage3, double LL3, double UL3, out int lowAD, out int midAD, out int highAD, failureLabel flab)
        {
            bool retVal = false;            
            int count = 0;
            int result = 0;
            int value = 0;

            _jig.set10R( false);
            _jig.set10RDMM( false);

            _jig.setPowerSupply(10.0, currentLimit);

            for (int i = 0; i < 10; i++)
            {
                Application.DoEvents();
                Thread.Sleep(100);
            }

            _jig.setPowerSupply(testVoltage1, currentLimit);
            _jig.setPowerSupplyState(true);
            _jig.setVbatt( true);

            for (int i = 0; i < 10; i++)
            {
                Application.DoEvents();
                Thread.Sleep(100);
            }

            int tempAverage = 0;
            tempAverage = 0;
            for (count = 0; count < 1; count++)
            {
                result = _sc.readBatteryVoltageAD( ref value);
                tempAverage += value;
                //if (result > 0) break;
            }
            value = (int)(tempAverage / 1);

            _adLow = value;
            if (result > 0 && value>= LL1 && value<= UL1)
            {
                _jig.setPowerSupplyVoltage(testVoltage2);
                Application.DoEvents();
                for (int i = 0; i < 10; i++)
                {
                    Application.DoEvents();
                    Thread.Sleep(100);
                }
                tempAverage = 0;
                for (count = 0; count < 1; count++)
                {
                    result = _sc.readBatteryVoltageAD( ref value);
                    tempAverage += value;
                    //if (result > 0) break;
                }
                value = (int)(tempAverage / 1);
                
                _adMid = value;

                if (result > 0 && value >= LL2 && value <= UL2)
                {
                    _jig.setPowerSupplyVoltage(testVoltage3);
                    Application.DoEvents();
                    for (int i = 0; i < 10; i++)
                    {
                        Application.DoEvents();
                        Thread.Sleep(100);
                    }
                    tempAverage = 0;
                    for (count = 0; count < 1; count++)
                    {
                        result = _sc.readBatteryVoltageAD( ref value);
                        tempAverage += value;
                        //if (result > 0) break;
                    }
                    value = (int)(tempAverage / 1);

                    _adHigh = value;

                    if (result > 0 && value >= LL3 && value <= UL3)
                    {
                        retVal = true;
                    }
                    else
                    {
                        retVal = false;
                        flab.TestID += " - adHigh";
                        flab.TestLL = LL3.ToString();
                        flab.TestUL = UL3.ToString(); ;
                        flab.TestResult = _adHigh.ToString(); 
                    }
                }
                else
                {
                    retVal = false;
                    flab.TestID += " - adMid";
                    flab.TestLL = LL2.ToString();
                    flab.TestUL = UL2.ToString(); ;
                    flab.TestResult = _adMid.ToString(); 
                }
            }
            else
            {
                retVal = false;
                flab.TestID += " - adLow";
                flab.TestLL = LL1.ToString();
                flab.TestUL = UL1.ToString(); ;
                flab.TestResult = _adLow.ToString(); 
            }

            lowAD = _adLow;
            midAD = _adMid;
            highAD = _adHigh;

            //lowAD = 410;
            //midAD = 416;
            //highAD = 422;

            return retVal;
        }

        public bool setRealTimeClock(int chargingDelay)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            DateTime dt = DateTime.UtcNow;             
            DateTime dtFromUnit = new DateTime();

            for (count = 0; count < 3; count++)
            {                
                result = _sc.writeDate( dt);

                if (result > 0) break;
                Thread.Sleep(100);
            }
            
            if (result > 0)
            {
                //for (int t = 0; t < chargingDelay; t++)
                //{
                //    Thread.Sleep(500);
                //    Application.DoEvents();
                //    Thread.Sleep(500);
                //    Application.DoEvents();
                //}
                for (count = 0; count < 3; count++)
                {
                    dt = DateTime.UtcNow;
                    result = _sc.writeTime( dt);
                    
                    if (result > 0) break;
                    Thread.Sleep(100);
                }

                if (result > 0)
                {
                    for (count = 0; count < 20; count++)
                    {
                        Thread.Sleep(100);
                        Application.DoEvents();
                    }
                    for (count = 0; count < 3; count++)
                    {
                       
                        result = _sc.readTime( ref dtFromUnit);

                        if (result > 0) break;
                    }

                    if (result > 0 && (dt.Second != dtFromUnit.Second))
                    {
                        int temp = dtFromUnit.Second;
                                                
                        //_jig.setVbatt( false);
                        for (count = 0; count < 20; count++)
                        {
                            Thread.Sleep(100);
                            Application.DoEvents();
                        }
                        //_jig.setVbatt( true);
                        Thread.Sleep(200);
                        for (count = 0; count < 3; count++)
                        {
                            dt = DateTime.UtcNow;
                            result = _sc.readTime( ref dtFromUnit);

                            if (result > 0) break;
                        }

                        if (result > 0 && temp != dtFromUnit.Second)
                        {
                            retVal = true;
                        }
                        else
                        {
                            retVal = false;
                        }
                    }
                    else
                    {
                        retVal = false;
                    }
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }
        public bool sendSms(string phoneNum, string internationFormatNumber, int timeout, ref bool cancel,failureLabel fl , string phoneNumberToSendTo, Action<int , int , int > reportProgress)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            try
            {
                ////return true;

                ////org.dyndns.amxtech.DatabaseWebService ws = new RTU_Functional_Test.org.dyndns.amxtech.DatabaseWebService();
                DateTime dtnow = DateTime.UtcNow;
                ////string foo = ws.TestMachineGetCurrentTime();
                //string foo = globals.rs.getServerTime();

                //int fudge = 0;
                //try
                //{

                //    DateTime dt = DateTime.SpecifyKind(DateTime.Parse(foo, new CultureInfo("en-GB")), DateTimeKind.Utc);
                //    if (dt < dtnow)
                //    {
                //        int tempFudge = (int)dt.Subtract(dtnow).TotalSeconds;

                //        fudge += Math.Abs(tempFudge) + 2;
                //    }
                //}
                //catch (Exception myerr)
                //{ }

                try
                {
                    string timestamp = DateTime.Now.ToString("yyMMddhhmmss");
                    byte[] x = Encoding.ASCII.GetBytes(timestamp);
                    string temp = "";
                    foreach( byte b in x)
                    {
                        temp += b.ToString("X2");
                    }
                    temp += "00";

                    for (count = 0; count < 3; count++)
                    {
                        System.Threading.Thread.Sleep(250);
                        dtnow = DateTime.UtcNow;
                        result = _sc.sendSms(temp, phoneNumberToSendTo);                        

                        if (result > 0) break;
                        System.Threading.Thread.Sleep(500);
                    }

                    if (result > 0)
                    {
                        for (count = 0; count < 60; count++)
                        {
                            result = _sc.getSendSmsStatus();
                            if (result > 0) break;
                            System.Threading.Thread.Sleep(1000);
                        }
                    }

                    if (result > 0)
                    {
                        

                        retVal = false;


                        //rikMachine.DatabaseWebService ws = new RTU_Functional_Test.rikMachine.DatabaseWebService();
                        string num = "";
                        reportProgress(0, timeout+1, 0);
                        for (int i = 0; i < timeout; i+=2)
                        {
                            reportProgress(0, timeout+1, i);
                            //num = ws.TestMachineConfirmPhoneNum(phoneNum);
                            //num = ws.TestMachineConfirmPhoneNumAndPayload(phoneNum, phoneNum);
                            
                            //num = globals.rs.confirmPhoneNumber(phoneNum);
                            //num = globals.rs.confirmPhoneNumberAndPayload(phoneNum, phoneNum + " " + timestamp);
                            //num = globals.rs.confirmPhoneNumberAndContainsPayload(phoneNum, phoneNum + " " + timestamp);
                            
                            //if (num == "NO RESULT")
                            if (AmlConnector.smsReceived(internationFormatNumber, phoneNum + " " + timestamp) == false)
                            {
                                //break;
                            }
                            else
                            {
                                retVal = true;
                                break;
                            }
                            Thread.Sleep(1000);

                            if (cancel == true)
                            {
                                retVal = false;
                                break;
                            }
                        }
                        reportProgress(0, timeout + 1, 0);
                        if (retVal == false)
                        {
                            if (num == "NO RESULT")
                            {
                                fl.TestLL = "SMS not received by server";
                            }
                            else
                            {
                                fl.TestLL = "Network delay too large";
                            }
                        }



                    }
                    else
                    {
                        fl.TestLL = "Error sending SMS";
                        retVal = false;
                    }
                }
                catch (Exception err)
                {
                    fl.TestLL = "Error Web Service";
                    retVal = false;
                }
            }
            catch (Exception err1)
            {
                retVal = false;
                fl.TestLL = "No connection to server";
            }

            return retVal;
        }

        public bool sendUdp(string imei, int timeout, ref bool cancel, failureLabel fl, string ip, string port, Action<int, int, int> reportProgress)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            try
            {
                
                DateTime dtnow = DateTime.UtcNow;
               
                try
                {
                    string timestamp = DateTime.Now.ToString("MMddhhmmss");
                    byte[] x = Encoding.ASCII.GetBytes(timestamp);
                    string temp = "";
                    foreach (byte b in x)
                    {
                        if (b > 0)
                        {
                            temp += b.ToString("X2");
                        }
                        else
                        {
                            //Mode to stop firmware truncating on null terminated string
                            temp += "01";
                        }

                    }
                    temp += "00";

                    for (count = 0; count < 3; count++)
                    {
                        System.Threading.Thread.Sleep(250);
                        dtnow = DateTime.UtcNow;
                        result = _sc.sendUdpPacket(timestamp, ip, port);

                        if (result > 0) break;
                        System.Threading.Thread.Sleep(500);
                        this._sc._can.Init();
                    }

                    

                    if (result > 0)
                    {
                        retVal = false;

                        string num = "";
                        reportProgress(0, timeout + 1, 0);
                        for (int i = 0; i < timeout; i += 2)
                        {
                            reportProgress(0, timeout + 1, i);                            

                            switch(i)
                            {
                                case 10:
                                case 20:
                                    {
                                        result = _sc.sendUdpPacket(timestamp, ip, port);
                                        break;
                                    }                                
                            }

                            if (AmlConnector.udpReceived(imei, timestamp) == false)
                            {
                                //break;
                            }
                            else
                            {
                                retVal = true;
                                break;
                            }
                            Thread.Sleep(1000);

                            if (cancel == true)
                            {
                                retVal = false;
                                break;
                            }
                        }
                        reportProgress(0, timeout + 1, 0);
                        //if (retVal == false)
                        {                            
                            fl.TestLL = "UDP not received by server";                            
                        }

                    }
                    else
                    {
                        fl.TestLL = "Error sending UDP";
                        retVal = false;
                    }
                }
                catch (Exception err)
                {
                    fl.TestLL = "Error Web Service";
                    retVal = false;
                }
            }
            catch (Exception err1)
            {
                retVal = false;
                fl.TestLL = "No connection to server";
            }

            return retVal;
        }

        public int sdCardCheck()
        {
            int retVal = -1;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.sdCardCheck();

                if (result > 0) break;
            }

            //if (result > 0 && result < 0x7F)
            //{
            //    retVal = result;
            //}
            //else
            //{
            //    retVal = result;
            //}
            return result;
        }

        public bool fullEEpromErase()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.fullEEpromErase();

                if (result > 0) break;
                if (count == 1)
                {
                    _jig.setVbatt(false);
                    Thread.Sleep(100);
                    _jig.setVbatt(true);
                    Thread.Sleep(100);
                }
            }

            if (result > 0)
            {
                if (count >= 1)
                {
                    Thread.Sleep(15000);
                }
                    retVal = true;
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }

        public bool fullFlashErase()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            
            for (count = 0; count < 3; count++)
            {
                result = _sc.fullFlashErase();

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;                
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }

        public bool flashTestRoutine()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.flashTestRoutine();

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }
        public bool storeParameters()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.storeParameters();

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }
        public bool loadDefaultConfiguration()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.loadDefaultConfiguration();

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }

        public bool readIMEI(out string imei)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _imei="";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readIMEI( ref _imei);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            imei=_imei;

            return retVal;
        }

        public bool readIMSI(out string imsi)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _imsi = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIMSI( ref _imsi);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            imsi = _imsi;
            return retVal;
        }

        public bool readModemType(out string modemType)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _modemType = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readModemType( ref this._modemType );

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            modemType = _modemType;
            return retVal;
        }

        public bool checkModemFirmwareVersion(List<string> validFirmwareVersion,out string readFirmwareVersion)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _modemFirmwareVersion = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readModemFirmwareVersion( ref this._modemFirmwareVersion );

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = false;
                foreach (string s in validFirmwareVersion)
                {
                    if (this._modemFirmwareVersion.StartsWith(s) == true)
                    {
                        retVal = true;
                        break;
                    }
                }
            }
            else
            {
                retVal = false;
            }
            readFirmwareVersion = _modemFirmwareVersion;
            return retVal;
        }

        public bool readPhoneNumber(out string phoneNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _phoneNumber = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readTelephoneNumber( ref _phoneNumber);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            phoneNumber = _phoneNumber;
            return retVal;
        }
        public bool readGPSStatus(out string status)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _gpsStatus = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.gpsStatus( ref _gpsStatus);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            status = _gpsStatus;
            return retVal;
        }

        public bool readGpsSatsAndSnr(int ll, int ul, out int numSats, out int snr, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            numSats = 0;
            snr = 0;
            
            for (count = 0; count < 3; count++)
            {
                result = _sc.gpsSatsAndSnr( ref numSats ,ref snr);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (snr >= ll && snr <= ul)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                    fl.TestLL = ll.ToString();
                    fl.TestUL = ul.ToString();
                    fl.TestResult = "SNR = " + snr.ToString(); 
                }
            }
            else
            {
                retVal = false;

            }
            
            return retVal;
        }
        
        public bool gpsVoltage(double LL, double UL, out double gpsVoltage)
        {
            bool retVal = false;
            _jig.setKLine(false);
            _jig.setGPS( true);
            Thread.Sleep(1000);
            _gpsVoltageReading = 0.0;
            if (_jig.measureVoltage(ref _gpsVoltageReading) == true)
            {
                if (_gpsVoltageReading >= LL && _gpsVoltageReading <= UL)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            gpsVoltage = _gpsVoltageReading;
            _jig.setGPS( false);
            return retVal;
        }

        public bool KLineVoltageNoPullup( double LL, double UL, out double result)
        {
            bool retVal = false;
            bool temp = false;
            double volt = 0.0;

            _jig.setPowerSupply(13.2, globals.defaultCurrent);
            Thread.Sleep(100);
            _jig.setKLine( false);
            _jig.setKLineDMM( true);

            for (int count = 0; count < 3; count++)
            {
                result = _sc.kLinePullUpOff();

                if (result > 0) break;
            }

            for (int count = 0; count < 3; count++)
            {
                temp = _jig.measureVoltage(ref volt);
                if (temp == true)
                {
                    break;
                }
            }

            _jig.setKLine( false);
            _jig.setKLineDMM( false);

            if (temp == true)
            {
                if (volt >= LL && volt <= UL)
                {
                    retVal = true;
                }
            }
            else
            {
                volt = -1.0;
            }
            result = volt;

           

            return retVal;
        }

        public bool KLineVoltageWithPullup(double LL, double UL, out double result)
        {
            bool retVal = false;
            bool temp = false;
            double volt = 0.0;

            _jig.setPowerSupply(13.2, globals.defaultCurrent);
            Thread.Sleep(100);
            _jig.setKLine( false);
            _jig.setKLineDMM( true);

            for (int count = 0; count < 3; count++)
            {
                result = _sc.kLinePullUpOn();

                if (result > 0) break;
            }
            System.Threading.Thread.Sleep(50);
            for (int count = 0; count < 3; count++)
            {
                temp = _jig.measureVoltage(ref volt);
                if (temp == true)
                {
                    break;
                }
            }

            _jig.setKLine( false);
            _jig.setKLineDMM( false);

            if (temp == true)
            {
                if (volt >= LL && volt <= UL)
                {
                    retVal = true;
                }
            }
            else
            {
                volt = -1.0;
            }
            result = volt;



            return retVal;
        }
        public bool KLineCommsTest()
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            _jig.setKLine(true);
            _jig.setKLineReflectorPower(true);

            Thread.Sleep(250);

            for (count = 0; count < 3; count++)
            {
                result = _sc.kLineTest();

                if (result > 0) break;
            }

            _jig.setKLine(false);
            _jig.setKLineReflectorPower(false);

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            return retVal;
        }
        

        public bool ignitionLine(out int offStatus, out int onStatus, failureLabel fl)
        {
            bool retVal = false;
            int count = 0;
            int result = 0;
            int value = 0;

            _jig.setVIgn( false);

            for (count = 0; count < 3; count++)
            {
                result = _sc.readIgnitionStatus( ref value);
                if (result > 0) break;
            }           
            
            if (result > 0 && value == 1)
            {
                offStatus = value;

                _jig.setVIgn( true);
                Application.DoEvents();
                Thread.Sleep(100);

                for (count = 0; count < 3; count++)
                {
                    result = _sc.readIgnitionStatus( ref value);
                    if (result > 0) break;
                }

                onStatus = value;

                if (result > 0 && value == 0)
                {
                    retVal = true;
                }
                else
                {
                    fl.TestLL = "0";
                    fl.TestUL = "0";
                    fl.TestResult = value.ToString(); 
                    onStatus = -1;
                    retVal = false;
                }                
            }
            else
            {
                fl.TestLL = "1";
                fl.TestUL = "1";
                fl.TestResult = value.ToString(); 

                onStatus = -1;
                offStatus = -1;
                retVal = false;
            }


            _jig.setVIgn( false);
            return retVal;
        }

        public bool ioLineDriver(double offLL, double offUL, double onLL, double onUL, out double offVoltage, out double onVoltage, failureLabel fl)
        {
            bool retVal = false;
            int count = 0;
            int result = 0;
            bool temp = false;
            double volt = 0.0;

            offVoltage = -1.0;
            onVoltage = -1.0;

            _jig.setPowerSupply(13.2, globals.defaultCurrent);
            _jig.setIO( true);
            _jig.setIODMM( true);

            for (count = 0; count < 3; count++)
            {
                result = _sc.ioOutOFF();
                if (result > 0) break;
            }

            if (result > 0)
            {
                for (count = 0; count < 3; count++)
                {
                    temp = _jig.measureVoltage(ref volt);
                    if (temp == true)
                    {
                        break;
                    }
                }

                if (temp == true)
                {
                    offVoltage = volt;
                    if (volt >= offLL && volt <= offUL)
                    {
                        for (count = 0; count < 3; count++)
                        {
                            result = _sc.ioOutON();
                            if (result > 0) break;
                        }

                        if (result > 0)
                        {
                            for (count = 0; count < 3; count++)
                            {
                                temp = _jig.measureVoltage(ref volt);
                                if (temp == true)
                                {
                                    break;
                                }
                            }

                            if (temp == true)
                            {
                                onVoltage = volt;
                                if (volt >= onLL && volt <= onUL)
                                {
                                    retVal = true;
                                }
                                else
                                {
                                    fl.TestLL = onLL.ToString();
                                    fl.TestUL = onUL.ToString();
                                    fl.TestResult = volt.ToString(); 
                                }
                            }
                        }
                    }
                    else
                    {
                        fl.TestLL = offLL.ToString();
                        fl.TestUL = offUL.ToString();
                        fl.TestResult = volt.ToString();
                    }
                }
            }
                    


            _jig.setIO( false);
            _jig.setIODMM( false);
             return retVal;
        }

        public bool AuxPowerSupply(double LL, double UL, out double result)
        {
            bool retVal = false;
            bool temp = false;

            result = 0.0;
            _jig.setBatSup( true);
            _jig.setBatSupDMM( true);
            
            for (int count = 0; count < 3; count++)
            {
                temp = _jig.measureVoltage(ref result);
                if (temp == true)
                {
                    break;
                }
            }

            if (temp == true)
            {
                if (result >= LL && result <= UL)
                {
                    retVal = true;
                }
            }
            else
            {
                retVal = false;
            }

            _jig.setBatSup( false);
            _jig.setBatSupDMM( false);

            return retVal;
        }

        public bool accelerometer(int xll, int xul, int yll, int yul, int zll, int zul, int HRxll, int HRxul, int HRyll, int HRyul, int HRzll, int HRzul, out int x, out int y, out int z, failureLabel fl)
        {
            bool retVal = false;
            bool highRes = false;
            int value=0, value1=0, value2=0, result=0;

            for (int count = 0; count < 3; count++)
            {
                result = _sc.readAccelerometerData( ref value,ref value1,ref value2, ref highRes);
                if (result > 0) break;
            }
            if (result > 0 )
            {
                if (highRes == true)
                {
                    xll = HRxll;
                    xul = HRxul;
                    yll = HRyll;
                    yul = HRyul;
                    zll = HRzll;
                    zul = HRzul;
                    //MessageBox.Show("high res");
                }
                else
                {
                    //MessageBox.Show("low res");
                }

               

                x = value;
                y = value1;
                z = value2;
                
                if ((x >= xll && x <= xul) && (y >= yll && y <= yul) && (z >= zll && z < zul))
                {
                    retVal = true;
                }
                else
                {
                    if (!(x >= xll && x <= xul))
                    {
                        fl.TestLL = xll.ToString();
                        fl.TestUL = xul.ToString();
                        fl.TestResult = x.ToString();
                    }
                    else
                    {
                        if (!(y >= yll && y <= yul))
                        {
                            fl.TestLL = yll.ToString();
                            fl.TestUL = yul.ToString();
                            fl.TestResult = y.ToString();
                        }
                        else
                        {
                            fl.TestLL = zll.ToString();
                            fl.TestUL = zul.ToString();
                            fl.TestResult = z.ToString();
                        }
                    }

                    retVal=false;
                }
            }
            else
            {
                x = 0;
                y = 0;
                z = 0;
                retVal = false;
                fl.TestLL = "";
                fl.TestUL = "";
                fl.TestResult = "Failed to Read Data"; 
            }      
            return retVal;
        }

        public bool scanSimNumber(string simNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeSimNumber( simNumber);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            
            return retVal;
        }

        public bool writeIMSINumber(string imsiNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            if (imsiNumber == "")
            {
                retVal = false;
            }
            else
            {
                for (count = 0; count < 3; count++)
                {
                    result = _sc.writeIMSINumber(imsiNumber);

                    if (result > 0) break;
                }

                if (result > 0)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            return retVal;
        }

        public bool verifyIMSINumber(string imsiNumber, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string flashImsi = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readIMSIFromFlash( ref flashImsi);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (imsiNumber == flashImsi)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = imsiNumber ;
            fl.TestResult = flashImsi ; 
            return retVal;
        }

        public bool writeIMEINumber(string imeiNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            if (imeiNumber == "")
            {
                retVal = false;
            }
            else
            {
                for (count = 0; count < 3; count++)
                {
                    result = _sc.writeIMEI(imeiNumber);

                    if (result > 0) break;
                }

                if (result > 0)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            return retVal;
        }

        public bool verifyIMEINumber(string imeiNumber,failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string flashImei = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readIMEIFromFlash( ref flashImei );

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (imeiNumber == flashImei)
                {
                    retVal = true;
                }
                else 
                { 
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = imeiNumber ;
            fl.TestResult = flashImei ; 
            return retVal;
        }

        public bool writeHwVariant(string hwVariant)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeHardwareVariantNumber(hwVariant);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyHwVariant(string hwVariant, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string flashHwVariant = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readHardwareVariantFromFlash(ref flashHwVariant);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (hwVariant == flashHwVariant)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = hwVariant ;
            fl.TestResult = flashHwVariant ; 
            return retVal;
        }

        public bool loadTestFirmware(string keilPath , string testFirmwarePath, string testSoftVer, bool force)
        {
            bool retVal = false;
            int result = -1;

            string logFile = Application.StartupPath + @"\prog.txt";

            if (System.IO.File.Exists(logFile))
            {                
                try
                {
                    System.IO.File.Delete(logFile);
                }
                catch (System.IO.IOException e)
                {
                    return false;
                }
            }

            _jig.setPowerSupply(13.2, globals.defaultCurrent);
            _jig.setPowerSupplyState(true);
            _jig.setVbatt( true);
            _jig.enableUlink();

            string testValue = "";
            for (int c = 0; c < 30; c++)
            {
                Thread.Sleep(100);
                Application.DoEvents();
            }                                
            bool tVal = this.readProdFirmwareVersion(out testValue);

            if (tVal == true && force == false)
            {
                if (Double.Parse(testValue) == Double.Parse(testSoftVer)) return true;
            }

            _jig.setVbatt( false);
            
            for (int c = 0; c < 30; c++)
            {
                Thread.Sleep(100);
                Application.DoEvents();
            }
            _jig.setVbatt( true);
                             

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = keilPath;
            startInfo.Arguments = "-f \"" + testFirmwarePath +"\" -j0 -o " + "\"" + logFile + "\"";
            startInfo.RedirectStandardOutput = false;
            startInfo.RedirectStandardError  = false;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            startInfo.UseShellExecute = true;
            
            using (Process process = new Process())
            {
                process.StartInfo = startInfo;
                process.Start();
                // only if you need the output for debugging 
                //string output = process.StandardOutput.ReadToEnd();
                //string output = process.StandardError.ReadToEnd(); 

                do
                {
                    Application.DoEvents();
                    for (int c = 0; c < 10; c++)
                    {
                        Thread.Sleep(100);
                    }
                } while (process.HasExited == false);

                result = process.ExitCode;
            } 


            if (result == 0)
            {
                if (System.IO.File.Exists(logFile))
                {
                    TextReader tr = new StreamReader(logFile);
                    string output = tr.ReadToEnd();
                    tr.Close();

                    if (output.Contains("Application running") == true)
                    {
                        retVal = true;
                    }
                    else
                    {
                        retVal = false;
                    }
                }
                else
                {
                    retVal = false;
                }

            }
            else
            {
                retVal = false;
            }

            _jig.setVbatt( false);
            Thread.Sleep(250);
            _jig.setVbatt( true);

            return retVal;
        }

        public bool loadAppFirmware(string keilPath, string appFirmwarePath)
        {
            bool retVal = false;
            int result = -1;

            string logFile = Application.StartupPath + @"\prog.txt";

            if (System.IO.File.Exists(logFile))
            {
                try
                {
                    System.IO.File.Delete(logFile);
                }
                catch (System.IO.IOException e)
                {
                    return false;
                }
            }


            _jig.enableUlink();
            _jig.setVbatt( false);
            for (int d = 0; d < 30; d++)
            {
                Thread.Sleep(100);
                Application.DoEvents();
            }
            _jig.setVbatt( true);
            //for (int c = 0; c < 30; c++)
            //{
            //    Thread.Sleep(100);
            //    Application.DoEvents();
            //}

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = keilPath;
            startInfo.Arguments = "-f \"" + appFirmwarePath + "\" -j0 -o " + "\"" + logFile + "\"";
            startInfo.RedirectStandardOutput = false;
            startInfo.RedirectStandardError = false;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            startInfo.UseShellExecute = true;

            using (Process process = new Process())
            {
                process.StartInfo = startInfo;
                process.Start();
               
                // only if you need the output for debugging 
                //string output = process.StandardOutput.ReadToEnd();
                //string output = process.StandardError.ReadToEnd(); 
                do
                {
                    Application.DoEvents();
                    for (int c = 0; c < 10; c++)
                    {
                        Thread.Sleep(100);
                    }
                } while (process.HasExited == false);

                result = process.ExitCode;
            }

            _jig.setVbatt( false);

            if (result == 0)
            {
                if (System.IO.File.Exists(logFile))
                {
                    TextReader tr = new StreamReader(logFile);
                    string output = tr.ReadToEnd();
                    tr.Close();

                    if (output.Contains("Programming Done") == true)
                    {
                        retVal = true;
                    }
                    else
                    {
                        retVal = false;
                    }
                }
                else
                {
                    retVal = false;
                }

            }
            else
            {
                retVal = false;
            }

            
            return retVal;
        }

        public bool readIctPcbID(out string ictPcbID)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictPcbID = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPcbId( ref ictPcbID, true );

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            
            _ictPcbId = ictPcbID;

            return retVal;
        }
        public bool readIctPcbIDFromFlash(out string ictPcbID)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictPcbID = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPCBIDFromFlash(ref ictPcbID);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictPcbId = ictPcbID;

            return retVal;
        }

        public bool readIctPassFail(int ll, int ul, out int ictPassFail)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictPassFail = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPassFail(ref ictPassFail, true);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if ((ictPassFail >= ll) && (ictPassFail <= ul))
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            
            _ictPassFailFlag = ictPassFail;

            return retVal;
        }
        public bool readIctPassFailFromFlash(int ll, int ul, out int ictPassFail)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictPassFail = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPassFlagFromFlash (ref ictPassFail);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if ((ictPassFail >= ll) && (ictPassFail <= ul))
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }

            _ictPassFailFlag = ictPassFail;

            return retVal;
        }

        public bool readIctFailCount(out int ictFailCount)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictFailCount = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctFailCount(ref ictFailCount, true);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictFailCount = ictFailCount;

            return retVal;
        }
        public bool readIctFailCountFromFlash(out int ictFailCount)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictFailCount = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctFailCountFromFlash(ref ictFailCount);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictFailCount = ictFailCount;

            return retVal;
        }

        public bool readAteFailCountFromFlash(out int ateFailCount)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ateFailCount = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readAteFailCountFromFlash(ref ateFailCount);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ateFailCount == 255)
                {
                    ateFailCount = 0;
                }

                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool readIctTestDate(out string ictTestDate)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictTestDate = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctTestDate(ref ictTestDate, true);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictTestDate = ictTestDate;

            return retVal;
        }

        public bool readIctTestDateFromFlash(out string ictTestDate)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            ictTestDate = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctTestDateFromFlash(ref ictTestDate);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictTestDate = ictTestDate;

            return retVal;
        }

        public bool writeIctPcbID(string ictPcbID)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
                        
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeIctPcbId( ictPcbID);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeIctPassFail( int ictPassFail)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeIctPassFail( ictPassFail);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeIctFailCount(int ictFailCount)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeIctFailCount( ictFailCount);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            _ictFailCount = ictFailCount;

            return retVal;
        }

        public bool writeIctTestDate(string ictTestDate)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeIctTestDate(  ictTestDate);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeSIMNumber(string simNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            if (simNumber == "")
            {
                retVal = false;
            }
            else
            {
                for (count = 0; count < 3; count++)
                {
                    result = _sc.writeSimNumber(simNumber);

                    if (result > 0) break;
                }

                if (result > 0)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            return retVal;
        }

        public bool verifySIMNumber(string simNumber, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string flashSim = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readSimFromFlash(ref flashSim);

                if (result > 0) break;
            }

            flashSim = flashSim.Replace("\0", "");

            if (result > 0)
            {                
                if (simNumber == flashSim)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = simNumber;
            fl.TestResult = flashSim; 
            return retVal;
        }

        public bool writeTelephoneNumber(string phoneNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            if (phoneNumber == "")
            {
                retVal = false;
            }
            else
            {
                for (count = 0; count < 3; count++)
                {
                    result = _sc.writePhoneNumber(phoneNumber);

                    if (result > 0) break;
                }

                if (result > 0)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            return retVal;
        }

        public bool verifyTelephoneNumber(string phoneNumber , failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string flashPhone = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readTelephoneNumberFromFlash(ref flashPhone);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (phoneNumber == flashPhone)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = phoneNumber;
            fl.TestResult = flashPhone; 
            return retVal;
        }

        public bool writePartNumber(string partNumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writePartNumber(partNumber);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyPartNumber(string partNumber,failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readPartNumberFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (partNumber == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = partNumber ;
            fl.TestResult = readValue; 
            return retVal;
        }

        public bool verifyIctPcbID( string ictPcbID, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPCBIDFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ictPcbID == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ictPcbID;
            fl.TestResult = readValue; 
            return retVal;
        }

        public bool verifyIctPassFail(int ictPassFail, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctPassFlagFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ictPassFail == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ictPassFail.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool verifyIctFailCount( int ictFailCount, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctFailCountFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ictFailCount == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }

            fl.TestLL = "";
            fl.TestUL = ictFailCount.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool verifyIctTestDate( string ictTestDate, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            string readValue = "";


            for (count = 0; count < 3; count++)
            {
                result = _sc.readIctTestDateFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ictTestDate == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ictTestDate;
            fl.TestResult = readValue; 

            return retVal;
        }

        public bool verifyAtePassFail(int atePassFail, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAtePassFlagFromFlash (ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (atePassFail == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = atePassFail.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool verifyAteFailCount(int ateFailCount, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAteFailCountFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ateFailCount == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ateFailCount.ToString();
            fl.TestResult = readValue.ToString() ; 
            return retVal;
        }

        public bool verifyAteTestDate(string ateTestDate, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            string readValue = "";


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAteTestDateFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ateTestDate == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }

            fl.TestLL = "";
            fl.TestUL = ateTestDate;
            fl.TestResult = readValue; 

            return retVal;
        }

        public bool writeAtePassFail(int atePassFail)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAtePassFail(atePassFail);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeAteFailCount(int ateFailCount)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAteFailCount(ateFailCount);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeAteTestDate(string ateTestDate)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            
            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAteTestDate(ateTestDate);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeFirmwareID(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeFirmwareID(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeServerTelNumberSecondary(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeServerTelNumberSecondary(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeGprsPrimaryServer(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsPrimaryServer(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeGprsSecondaryServer(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsSecondaryServer(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeGprsEngineeringServer(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsEngineeringServer(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeGprsAUXServer(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsAUXServer(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyFirmwareID(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readFirmwareIdFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue; 
            return retVal;
        }

        public bool verifyServerTelNumberSecondary(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readServerTelNumberSecondaryFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue;
            return retVal;
        }

        public bool verifyGprsPrimaryServer(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsPrimaryServerFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue;
            return retVal;
        }

        public bool verifyGprsSecondaryServer(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsSecondaryServerFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue;
            return retVal;
        }

        public bool verifyGprsEngineeringServer(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsEngineeringServerFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue;
            return retVal;
        }

        public bool verifyGprsAUXServer(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsAUXServerFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue;
            return retVal;
        }

        public bool writeSmsServerPhoneNumber(string smsServerPhonenumber)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeSmsServerPhoneNumber(smsServerPhonenumber);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifySmsServerPhoneNumber(string smsServerPhonenumber, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readSmsServerNumberFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (smsServerPhonenumber == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = smsServerPhonenumber ;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }
        

        public bool writeAdLow(int adLow)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAdLow(adLow);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeAdMid(int adMid)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAdMid(adMid);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool writeAdHigh(int adHigh)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeAdHigh(adHigh);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyADLow(int adLow, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAdLowFromFlash (ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (adLow  == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = adLow.ToString() ;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }
        public bool verifyADMid(int adMid, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAdMidFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (adMid == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = adMid.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }
        public bool verifyADHigh(int adHigh, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readAdHighFromFlash(ref readValue);

                if (result > 0) break;
            }
            
            if (result > 0)
            {
                if (adHigh == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = adHigh.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeSmsOnly(bool smsOnly)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeSmsOnlyFlag(smsOnly);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifySmsOnly(bool smsOnly, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            bool readValue = false;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readSmsOnlyFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (smsOnly == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = smsOnly.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeGprsPortNumber(int portNum)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsPortNumber(portNum);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyGprsPortNumber(int portNum, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsPortFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (portNum == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = portNum.ToString();
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeGprsServerIP(string ipAddress)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsIpAddress(ipAddress);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyGprsServerIP(string ipAddress, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsServerIpFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ipAddress  == readValue )
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ipAddress ;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeGprsAPN(string apn)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsAPN(apn);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyGprsAPN(string apn, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsApnFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (apn == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = apn;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeGprsUserID(string id)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsUserName(id);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyGprsUserID(string id, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsUserNameFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (id == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = id;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeGprsPassword(string password)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeGprsPassword(password);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyGprsPassword(string password, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readGprsPasswordFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (password == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestUL = password;
            fl.TestResult = readValue;

            return retVal;
        }

        public bool writeBootloaderVersion(string version)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeBootloaderVersion(version);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyBootloaderVersion(string version, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readBootLoaderVersionFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (version  == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = version;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeFirmwareFileName(string name)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            for (count = 0; count < 3; count++)
            {
                result = _sc.writeFirmwareFilename(name);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            return retVal;
        }

        public bool verifyFirmwareFileName(string name, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;
            string readValue = "";

            for (count = 0; count < 3; count++)
            {
                result = _sc.readFirmwareFilenameFromFlash (ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (name == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = name;
            fl.TestResult = readValue.ToString(); 
            return retVal;
        }

        public bool writeFailureData(int failCount, string ictPcbID, int ictFailCount, int ictPassFail, string ictTestDate, int pcbVariant, string testSoftVer)
        {
            bool retVal = false;
            bool temp = false;
            int count = 0;
            int status = 0;
            int retryLoop = 0;
            int stage = 0;

            _jig.set10R( false);
            _jig.set10RDMM( false);
            _jig.setPowerSupply(13.2, globals.defaultCurrent);
            _jig.setPowerSupplyState(true);
            _jig.setVbatt( false);
            Thread.Sleep(100);
            _jig.setVbatt( true);

            Thread.Sleep(100);

            for (retryLoop = 0; retryLoop < 3; retryLoop++)
            {

                stage = 1;
                for (count = 0; count < 3; count++)
                {
                    temp = writeAteFailCount(failCount);
                    if (temp == true)
                    {
                        break;
                    }
                    Thread.Sleep(100);
                }

                if (temp == true)
                {
                    stage = 2;
                    for (count = 0; count < 3; count++)
                    {
                        temp = writeAtePassFail(0);
                        if (temp == true)
                        {
                            break;
                        }
                        Thread.Sleep(100);
                    }

                    if (temp == true)
                    {
                        stage = 3;
                        for (count = 0; count < 3; count++)
                        {
                            temp = writeAteTestDate(DateTime.UtcNow.Date.Day.ToString("d2") + DateTime.UtcNow.Date.Month.ToString("d2") + DateTime.UtcNow.Date.Year.ToString("d4"));
                            if (temp == true)
                            {
                                break;
                            }
                            Thread.Sleep(100);
                        }

                        if (temp == true)
                        {
                            stage = 4;
                            if (ictFailCount != -1)
                            {
                                for (count = 0; count < 3; count++)
                                {
                                    temp = writeIctFailCount(ictFailCount);
                                    if (temp == true)
                                    {
                                        break;
                                    }
                                    Thread.Sleep(100);
                                }
                            }

                            if (temp == true)
                            {
                                stage = 5;
                                if (ictPcbID != "")
                                {
                                    for (count = 0; count < 3; count++)
                                    {
                                        temp = writeIctPcbID(ictPcbID);
                                        if (temp == true)
                                        {
                                            break;
                                        }
                                        Thread.Sleep(100);
                                    }
                                }

                                if (temp == true)
                                {
                                    stage = 6;
                                    if (ictPassFail != -1)
                                    {
                                        for (count = 0; count < 3; count++)
                                        {
                                            temp = writeIctPassFail(ictPassFail);
                                            if (temp == true)
                                            {
                                                break;
                                            }
                                            Thread.Sleep(100);
                                        }
                                    }

                                    if (temp == true)
                                    {
                                        stage = 7;
                                        if (ictTestDate != "")
                                        {
                                            for (count = 0; count < 3; count++)
                                            {
                                                temp = writeIctTestDate(ictTestDate);
                                                if (temp == true)
                                                {
                                                    break;
                                                }
                                                Thread.Sleep(100);
                                            }
                                        }

                                        if (temp == true)
                                        {
                                            stage = 8;
                                            if (pcbVariant != -1)
                                            {
                                                for (count = 0; count < 3; count++)
                                                {
                                                    
                                                    temp = writePcbVariant(pcbVariant, true);
                                                    
                                                    if (temp == true)
                                                    {
                                                        break;
                                                    }
                                                    Thread.Sleep(100);
                                                }
                                            }

                                            if (temp == true)
                                            {
                                                stage = 9;
                                                for (count = 0; count < 3; count++)
                                                {
                                                    temp = storeParameters();
                                                    if (temp == true)
                                                    {
                                                        break;
                                                    }
                                                    Thread.Sleep(100);
                                                }

                                                if (temp == true)
                                                {
                                                    stage = 10;
                                                    if (pcbVariant != -1)
                                                    {
                                                        for (count = 0; count < 3; count++)
                                                        {
                                                            temp = writePcbVariant(pcbVariant, true);
                                                            
                                                            if (temp == true)
                                                            {
                                                                break;
                                                            }
                                                            Thread.Sleep(100);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                

                if (temp == true)
                {
                    break;
                }
                else
                {
                    _jig.setVbatt( false);
                    //if (MessageBox.Show("Writing Failure Data Failed. Please Retry (E-Code " + stage.ToString() +")", "", MessageBoxButtons.YesNo) == DialogResult.No )
                    //{
                    //    break;
                    //}
                }
                _jig.setVbatt( true);
            }

            _jig.setVbatt( false);
            return true;
        }
        public bool readProdFirmwareVersion(out string version)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            string _ver = "";

            if (globals.flyingLead)
            {
                _jig.setPowerSupply(13.2, globals.defaultCurrent);
                _jig.setPowerSupplyState(true);
                _jig.setVbatt( true);
            }
        
            Thread.Sleep(2000);
            Application.DoEvents();

            for (count = 0; count < 3; count++)
            {
                result = _sc.readProductionTestFirmwareVersion(ref _ver);

                if (result > 0) break;
                _sc._can.Init();
                CanInterfaceSofting.canFrames1.Clear();
                CanInterfaceSofting.canFrames2.Clear();
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            version = _ver;
            return retVal;
        }
        public bool readProcessorVersion(out string version)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            Thread.Sleep(2000);
            Application.DoEvents();

            string _ver = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readProcessorVersion(ref _ver);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
            version = _ver;
            return retVal;
        }

        public bool readPcbVariant(out int pcbVariant)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            pcbVariant = 0;
            for (count = 0; count < 3; count++)
            {
                result = _sc.readPcbVariant(ref pcbVariant, true);
                //pcbVariant = 4;
                if (result > 0) break;
                _sc._can.Init();
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }            

            return retVal;
        }

        public bool writePcbVariant(int var, bool greaterThan8_2)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                if (greaterThan8_2)
                {
                    result = _sc.writePcbVariant_TestCode_3_2(var);
                }
                else
                {
                    result = _sc.writePcbVariant(var);
                }
                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }

            

            return retVal;
        }

        public bool verifyPcbVariant(int pcbVar, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readPCBVariantFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (pcbVar == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = pcbVar.ToString();
            fl.TestResult = readValue.ToString();
            return retVal;
        }

        public bool writeVehicleVariant(int var)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeVehicleVariant(var);
                
                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }



            return retVal;
        }

        public bool verifyVehicleVariant(int vehicleVar, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readVehicleVariantFromFlash (ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (vehicleVar == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = vehicleVar.ToString();
            fl.TestResult = readValue.ToString();
            return retVal;
        }

        public bool writeIOConfig(int var)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeIOConfig(var);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }



            return retVal;
        }

        public bool verifyIOConfig(int ioConfigVar, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readIOConfigFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (ioConfigVar == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = ioConfigVar.ToString();
            fl.TestResult = readValue.ToString();
            return retVal;
        }

        public bool writeVehicleFlags(int var)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.writeVehicleFlags(var);

                if (result > 0) break;
            }

            if (result > 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }



            return retVal;
        }

        public bool verifyVehicleFlags(int vehicleFlags, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            int readValue = 0;


            for (count = 0; count < 3; count++)
            {
                result = _sc.readVehicleFlagsFromFlash(ref readValue);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (vehicleFlags == readValue)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            fl.TestLL = "";
            fl.TestUL = vehicleFlags.ToString();
            fl.TestResult = readValue.ToString();
            return retVal;
        }

        public bool readGyroID(string validID, out string gyroID, failureLabel fl)
        {
            bool retVal = false;
            int result = 0;
            int count = 0;

            string _ver = "";
            for (count = 0; count < 3; count++)
            {
                result = _sc.readGyroID(ref _ver);

                if (result > 0) break;
            }

            if (result > 0)
            {
                if (validID == _ver)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                retVal = false;
            }
            gyroID = _ver;

            fl.TestLL = validID;
            fl.TestUL = "";
            fl.TestResult = _ver;

            return retVal;
        }

        public bool gyroData(int xll, int xul, int yll, int yul, int zll, int zul, out int x, out int y, out int z, failureLabel fl)
        {
            bool retVal = false;
            int value = 0, value1 = 0, value2 = 0, result = 0;

            for (int count = 0; count < 3; count++)
            {
                result = _sc.readGyroData(ref value, ref value1, ref value2);
                if (result > 0) break;
            }
            if (result > 0)
            {   
                x = value;
                y = value1;
                z = value2;

                if ((x >= xll && x <= xul) && (y >= yll && y <= yul) && (z >= zll && z < zul))
                {
                    retVal = true;
                }
                else
                {
                    if (!(x >= xll && x <= xul))
                    {
                        fl.TestLL = xll.ToString();
                        fl.TestUL = xul.ToString();
                        fl.TestResult = x.ToString();
                    }
                    else
                    {
                        if (!(y >= yll && y <= yul))
                        {
                            fl.TestLL = yll.ToString();
                            fl.TestUL = yul.ToString();
                            fl.TestResult = y.ToString();
                        }
                        else
                        {
                            fl.TestLL = zll.ToString();
                            fl.TestUL = zul.ToString();
                            fl.TestResult = z.ToString();
                        }
                    }

                    retVal = false;
                }
            }
            else
            {
                x = 0;
                y = 0;
                z = 0;
                retVal = false;
                fl.TestLL = "";
                fl.TestUL = "";
                fl.TestResult = "Failed to Read Data";
            }
            return retVal;
        }


        public bool crankDetectTest(failureLabel fl)
        {
            bool retVal = false;
            
            int value = 0;

            if (_sc.clearCrankDetectFlag() == 1)
            {
                if (_sc.readCrankDetectFlag(ref value) > 0)
                {
                    if (value == 0)
                    {
                        _jig.setPowerSupplyVoltage(10.8);
                        Thread.Sleep(1000);
                        _jig.setPowerSupplyVoltage(13.2);
                        Thread.Sleep(1000);

                        if (_sc.readCrankDetectFlag(ref value) > 0)
                        {
                            if (value == 1)
                            {
                                retVal = true;
                                fl.TestResult = "";
                            }
                            else
                            {
                                fl.TestResult = "Failed to set flag";
                            }
                        }
                        else
                        {
                            fl.TestResult = "Comms fail to read flag";
                        }
                    }
                    else
                    {
                        fl.TestResult = "Failed to clear flag";
                    }
                }
                else
                {
                    fl.TestResult = "Failed to read flag";
                }
            }
            else
            {
                fl.TestResult = "Comms failure";
            }

            return retVal;
        }

        public bool can2RtuTest(failureLabel fl)
        {
            bool retval = false;

            _jig.setCAN(false);
            int temp = _sc._can.Init();

            System.Threading.Thread.Sleep(500);
            if (_sc.getRtuCan2Response(0x7E1) > 0)
            {
                retval = true;
            }
            else
            {
                retval = false;
            }
            _jig.setCAN(true);
            _sc._can.Init();
            return retval;
        }

        public bool Can2PassThruRtuTest()
        {
  /*          bool retval = false;
            

            _jig.setCAN(true);
            int temp = _sc._can.Init();

            CanInterfaceSofting.canFrames1.Clear();
            CanInterfaceSofting.canFrames2.Clear();

            for (int i = 0; i < 3; i++)
            {
                if (_sc.clearCrankDetectFlag() == 1)
                {
                    if (CanInterfaceSofting.canFrames2.Count >= 2)
                    {
                        retval = true;
                        break;
                    }
                    else
                    {
                        retval = false;
                    }
                }
                else
                {
                    retval = false;
                }
                System.Threading.Thread.Sleep(250);
            }
*/
            return true;
        }

        public bool loadWhiteList(string filename, Action<int,int,int> reportProgress)
        {
            if(_sc.readWhiteListFromFile(filename) == true)
            {
                return _sc.loadWhiteList(reportProgress);
            }
            else
            {
                return false;
            }
        }

        public bool verifyWhiteList()
        {
            return _sc.verifyUploadWhiteList();
        }
    }
}

