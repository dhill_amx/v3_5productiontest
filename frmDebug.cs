﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//2b34343738323532373838373000 rik ota system
//2b34343738363030333538353500 itagg virtual num

namespace V3ProductionTest
{
    public partial class frmDebug : Form
    {
        RelayBoard _rb = new RelayBoard();

        PSUControl _pc = new PSUControl((PsuType)Properties.Settings.Default.PsuType);
        
        LabJack _lj = new LabJack();
        CanInterfaceSofting canCard = new CanInterfaceSofting();
        jLinkAPI _jl = new jLinkAPI();

        private byte[] relayStates = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] response;

        public frmDebug()
        {
            InitializeComponent();
        }

        ~frmDebug()
        {
            try
            {
                canCard.CloseInterface();
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        private void frmDebug_Load(object sender, EventArgs e)
        {
            this.txtPsuComPort.Text = Properties.Settings.Default.PsuComPort.ToString();
            this.txtRelayComPort.Text = Properties.Settings.Default.RelayComPort.ToString();
            this.txtJigIdent.Text = Properties.Settings.Default.JigIdent.ToString();
            this.txtServerIp.Text = Properties.Settings.Default.ServerIP;
            this.txtServerPort.Text =Properties.Settings.Default.ServerPort;

            this.txtUdpIp.Text = Properties.Settings.Default.UdpIP;
            this.txtUdpPort.Text = Properties.Settings.Default.UdpPort;

            this.txtTestNumber.Text =Properties.Settings.Default.PhoneNumber ;

            this.cbPsuType.Items.Clear();
            this.cbPsuType.Items.Add(PsuType.GwInstek);
            this.cbPsuType.Items.Add(PsuType.Tenma722545);
            
            this.cbPsuType.SelectedItem = (PsuType)Properties.Settings.Default.PsuType;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (relayStates[0] == 0)
            {
                relayStates[0] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay1_ON);
                button1.BackColor = Color.Red;
            }
            else
            {
                relayStates[0] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay1_OFF);
                button1.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (relayStates[1] == 0)
            {
                relayStates[1] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay2_ON);
                button2.BackColor = Color.Red;
            }
            else
            {
                relayStates[1] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay2_OFF);
                button2.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (relayStates[2] == 0)
            {
                relayStates[2] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay3_ON);
                button3.BackColor = Color.Red;
            }
            else
            {
                relayStates[2] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay3_OFF);
                button3.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (relayStates[3] == 0)
            {
                relayStates[3] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay4_ON);
                button4.BackColor = Color.Red;
            }
            else
            {
                relayStates[3] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay4_OFF);
                button4.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (relayStates[4] == 0)
            {
                relayStates[4] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay5_ON);
                button5.BackColor = Color.Red;
            }
            else
            {
                relayStates[4] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay5_OFF);
                button5.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (relayStates[7] == 0)
            {
                if (relayStates[5] == 0)
                {
                    relayStates[5] = 1;
                    _rb.setRelayState(RelayBoard.relayStates.Relay6_ON);
                    button6.BackColor = Color.Red;
                    button13_Click(this, new EventArgs());
                }
                else
                {
                    relayStates[5] = 0;
                    _rb.setRelayState(RelayBoard.relayStates.Relay6_OFF);
                    button6.BackColor = System.Windows.Forms.Control.DefaultBackColor;

                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (relayStates[6] == 0)
            {
                relayStates[6] = 1;
                _rb.setRelayState(RelayBoard.relayStates.Relay7_ON);
                button7.BackColor = Color.Red;
            }
            else
            {
                relayStates[6] = 0;
                _rb.setRelayState(RelayBoard.relayStates.Relay7_OFF);
                button7.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (relayStates[5] == 0)
            {
                if (relayStates[7] == 0)
                {
                    relayStates[7] = 1;
                    _rb.setRelayState(RelayBoard.relayStates.Relay8_ON);
                    button8.BackColor = Color.Red;
                    button13_Click(this, new EventArgs());
                }
                else
                {
                    relayStates[7] = 0;
                    _rb.setRelayState(RelayBoard.relayStates.Relay8_OFF);
                    button8.BackColor = System.Windows.Forms.Control.DefaultBackColor;
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            int temp = 1;
            if (int.TryParse(this.txtPsuComPort.Text, out temp) == true)
            {
                Properties.Settings.Default.PsuComPort = temp;
            }
            if (int.TryParse(this.txtRelayComPort.Text, out temp) == true)
            {
                Properties.Settings.Default.RelayComPort = temp;
            }
            if (int.TryParse(this.txtJigIdent.Text, out temp) == true)
            {
                Properties.Settings.Default.JigIdent = temp;
            }

            Properties.Settings.Default.ServerIP = this.txtServerIp.Text;
            Properties.Settings.Default.ServerPort = this.txtServerPort.Text;

            Properties.Settings.Default.PhoneNumber = this.txtTestNumber.Text;

            Properties.Settings.Default.Processor = this.txtProcessor.Text;

            Properties.Settings.Default.UdpIP = this.txtUdpIp.Text;
            Properties.Settings.Default.UdpPort = this.txtUdpPort.Text;

            Properties.Settings.Default.PsuType = (int)this.cbPsuType.SelectedItem;

            Properties.Settings.Default.Save();

            _pc = new PSUControl((PsuType)Properties.Settings.Default.PsuType);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            button10.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            _lj.configure();
            _pc.init(Properties.Settings.Default.PsuComPort);
            _rb.connect(Properties.Settings.Default.RelayComPort);
            
            canCard.Init();

            button10.Enabled = true;
            this.Cursor = Cursors.Arrow;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (button11.BackColor == System.Windows.Forms.Control.DefaultBackColor)
            {
                _pc.init(Properties.Settings.Default.PsuComPort);
                _pc.output(true);
                button11.BackColor = Color.Red;
            }
            else
            {
                _pc.output(false);
                button11.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.txtCurrent.Text = _pc.getOutputCurrent().ToString("F3") + "A";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
                button14.BackColor = System.Windows.Forms.Control.DefaultBackColor;
            }
            else
            {
                timer1.Enabled = true;
                button14.BackColor = Color.Red;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            button12_Click(this, new EventArgs());
            button13_Click(this, new EventArgs());
        }

        private void button13_Click(object sender, EventArgs e)
        {
            double temp = _lj.ReadAnalogChannel(3, 10);
            if (temp < 2.0)
            {
                lblJigOpen.Text = "NOT SET";
            }
            else
            {
                lblJigOpen.Text = "SET";
            }
            txtJigOpen.Text = temp.ToString("F3") + "V";

            temp = _lj.ReadAnalogChannel(2, 10);
            if (temp < 2.0)
            {
                lblJigClosed.Text = "NOT SET";
            }
            else
            {
                lblJigClosed.Text = "SET";
            }
            txtJigClosed.Text = temp.ToString("F3") + "V";

            temp = _lj.ReadAnalogChannel(0, 10);
            if (relayStates[5]==1)
            {
                button6.Text = "RELAY 6 KLine " + temp.ToString("F3") + "V";
            }
            if (relayStates[7] == 1)
            {
                button8.Text = "RELAY 8 GPS " + temp.ToString("F3") + "V";
            }
            txtMeasure.Text = temp.ToString("F3") + "V";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            rtb1.Clear();
        }

        private void button16_Click(object sender, EventArgs e)
        {            
            int std = 0;
            byte[] data = new byte[8];
            
            int dataPtr = 0;
            byte temp = 0;
            CANL2dotNET.PARAM_CLASS msg = new CANL2dotNET.PARAM_CLASS();

            if (radioButton3.Checked == true)
            {
                std = 1;
            }

            for (int i = 0; i < 16; i += 2)
            {
                if (byte.TryParse(this.txtMessage.Text.Substring(i, 2),System.Globalization.NumberStyles.AllowHexSpecifier,null, out temp) == true)
                {
                    data[dataPtr] = temp;
                }
                dataPtr++;
            }

            if (radioButton1.Checked == true)
            {
                canCard.sendCanFrameChannel1(uint.Parse(this.txtIdent.Text, System.Globalization.NumberStyles.AllowHexSpecifier, null), std, 8, data);
                this.rtb1.AppendText("Sent Ch1 : " + this.txtIdent.Text + " , " + this.txtMessage.Text + "\n");
                canCard.getResponseChannel1(msg, ref response);
                this.rtb1.AppendText("Rcv : " + msg.Ident.ToString("X2") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2") + "\n");
            }
            else
            {
                canCard.sendCanFrameChannel2(uint.Parse(this.txtIdent.Text, System.Globalization.NumberStyles.AllowHexSpecifier, null), std, 8, data);
                this.rtb1.AppendText("Sent Ch2 : " + this.txtIdent.Text + " , " + this.txtMessage.Text + "\n");
                canCard.getCanFrameChannel2(msg);
                this.rtb1.AppendText("Rcv : " + msg.Ident.ToString("X2") + " , " + msg.RCV_data[0].ToString("X2") + " , " + msg.RCV_data[1].ToString("X2") + " , " + msg.RCV_data[2].ToString("X2") + " , " + msg.RCV_data[3].ToString("X2") + " , " + msg.RCV_data[4].ToString("X2") + " , " + msg.RCV_data[5].ToString("X2") + " , " + msg.RCV_data[6].ToString("X2") + " , " + msg.RCV_data[7].ToString("X2") + "\n");
            }
            
        }

        private void button17_Click(object sender, EventArgs e)
        {
            _jl.connect(this.txtProcessor.Text);
            _jl._UnsecureChip();
            _jl.close();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            _jl.connect(this.txtProcessor.Text);
            _jl._SecureChip();
            _jl.close();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Multiselect = false;
            int res = 0;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _jl.connect(this.txtProcessor.Text);
                res = _jl.program(fd.FileName, false);
                if (res < 0)
                {
                    MessageBox.Show("Failed " + res.ToString());
                }
                _jl.close();
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            double volt = 12.0;
            try
            {
                double.TryParse(this.txtVoltage.Text, out volt);
                _pc.setOutputVoltage(volt);
                
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
            }
        }

        private void frmDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                canCard.CloseInterface();
                canCard = null;
            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.ProgressBar pb = new ProgressBar();
            AmlConnector.processFailedInsertProductionData(Application.StartupPath + @"\failedInsertProductionData", pb);
            AmlConnector.processFailedInsertProductionTestData(Application.StartupPath + @"\failedInsertProductionTestResuls", pb);
        }
    }
}
