﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LabJack.LabJackUD;

namespace V3ProductionTest
{
    public class LabJack
    {
        private U3 _u3;

        ~LabJack()
        {
            try
            {
                LJUD.Close();
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        public bool configure()
        {
            bool retVal = false;
            try
            {
                //Open the first found LabJack.
                _u3 = new U3(LJUD.CONNECTION.USB, "0", true); // Connection through USB		
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PIN_CONFIGURATION_RESET, 0, 0, 0);
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_PORT, 0, Math.Pow(2, 6) - 1, 6);
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, 6, 0, 0);     // Set FIO6 to digital IO
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, 7, 0, 0);     // Set FIO7 to digital IO
                LJUD.ePut(_u3.ljhandle, LJUD.IO.GET_DIGITAL_BIT_DIR, 6, 0, 0);       // Set FIO6 to input
                LJUD.ePut(_u3.ljhandle, LJUD.IO.GET_DIGITAL_BIT_DIR, 7, 0, 0);       // Set FIO7 to input
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.AIN_SETTLING_TIME, 1, 0);
                LJUD.ePut(_u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, 9, 0, 0);     // Set FIO9 to digital IO
                LJUD.ePut(_u3.ljhandle, LJUD.IO.GET_DIGITAL_BIT_DIR, 9, 1, 0);       // Set FIO9 to output

                retVal = true;
            }
            catch (LabJackUDException ej)
            {
                ErrorMess(ej);                
            }
            return retVal;
        }

        public double ReadAnalogChannel(int Channel, int SampleSize)
        {
            double sample = 0, value = 0;
            for (int i = 0; i < SampleSize; i++)
            {
                try
                {
                    switch (Channel)
                    {
                        case 4: LJUD.AddRequest(_u3.ljhandle, LJUD.IO.GET_AIN_DIFF, Channel, 0, 5, 0);       // Differential measurement between AIN4-AIN5
                            LJUD.GoOne(_u3.ljhandle);
                            LJUD.GetResult(_u3.ljhandle, LJUD.IO.GET_AIN_DIFF, Channel, ref value);
                            break;
                        default: LJUD.AddRequest(_u3.ljhandle, LJUD.IO.GET_AIN, Channel, 0, 0, 0);
                            LJUD.GoOne(_u3.ljhandle);
                            LJUD.GetResult(_u3.ljhandle, LJUD.IO.GET_AIN, Channel, ref value);
                            break;
                    }
                    sample += value;
                }
                catch (LabJackUDException e)
                {
                    ErrorMess(e);
                    return (0);
                }               
            }
            sample = sample / SampleSize;
            return (sample);
        }

        private void ErrorMess(LabJackUDException Error)
        {
            System.Diagnostics.Debug.WriteLine( Error.LJUDError.ToString());
            
        }
    }
}
