﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace V3ProductionTest
{
    public partial class frmMain : Form
    {
        delegate void cb_displayMessage(string msg);
        delegate void cb_setPgFeedback(int min, int max, int value);
        delegate void cb_addTestNode(string testName);
        delegate void cb_addResultChild(string value);
        delegate void cb_clearTestResults();
        delegate void cb_setTestProgress(int min, int max ,int val);
        delegate void cb_clearScanBox();
        delegate void cb_ScanBoxVisible(bool set);

        object jcLock = new object();

        public string filePath = Application.StartupPath;

        string voidLabelSoftVerJig1 = "Test";
        string voidLabelCommentJig1 = "001";

        bool scanningLabel = false;
        bool scanningSimNumber = false;
        bool scanningPhoneNumber = false;
        bool scanningBoxID = false;

        string m_LabelSerialNum = "";
        string m_SimNumber = "";
        string m_PhoneNumber = "";
        string m_BoxID = "";

        
        bool ignoreIctData = false;
        bool writeDefaultICTData = false;

        bool jigRunning = false;
        bool allowLite = false;
        bool allowVehicleFlags = false;
        bool largeMicro = false;

        int pass = 0;
        int fail = 0;


        TreeNode tn;

        DateTime startTimeJig = DateTime.Now;

        private TestConfig testPathJig;        
        CanInterfaceSofting can = new CanInterfaceSofting();
        JigControl jc;
        SoftwareCommands scJig;
        TestControlVariables tcvTempJig;

        List<Customer> dtCustomers;
        List<customerToConfigV2> dtValidConfigs;
        List<configV2> dtTesTConfigs;
        List<simDetails> dtSimList;
        List<firmwareVariantMapping> dtVariantConfig;
        List<variantID> dtVariantID;
        
        string testSequenceFileName = "";
        string releaseConfigFileName = "";

        public int varIdMapping = 0;
        public string varIdMap = "";

        int autoTestCnt = 0;

        double defaultCurrent = 5.0;

        public frmMain()
        {
            InitializeComponent();
        }
               

        private void displayMessage(string msg)
        {
            if (this.lblMessage.InvokeRequired == true)
            {
                cb_displayMessage c = new cb_displayMessage(displayMessage);
                this.Invoke(c,new object[]{msg});
            }
            else
            {
                this.lblMessage.Text = msg;
                Application.DoEvents();
            }
        }
        private void setTestProgress(int min, int max, int value)
        {
            if (this.pbTests.InvokeRequired == true)
            {
                cb_setTestProgress c = new cb_setTestProgress(setTestProgress);
                this.Invoke(c, new object[] { min,max,value });
            }
            else
            {
                this.pbTests.Minimum = min;
                this.pbTests.Maximum = max;
                this.pbTests.Value = value;
            }
        }
        private void addTestNode(string testName)
        {
            if (this.tvTests.InvokeRequired == true)
            {
                cb_addTestNode c = new cb_addTestNode(addTestNode);
                this.Invoke(c, new object[] { testName });
            }
            else
            {
                if (tn != null)
                {
                    tn.BackColor = Color.LimeGreen;
                }
                tn = new TreeNode(testName.PadRight(24));
                tn.NodeFont = new System.Drawing.Font("Courier New", 15.0f, FontStyle.Bold);
                tvTests.BeginUpdate();
                tvTests.Nodes.Add(tn);
                tvTests.EndUpdate();
                tn.EnsureVisible();
                
            }
        }
        private void addResultChild(string value)
        {
            if (this.tvTests.InvokeRequired == true)
            {
                cb_addResultChild c = new cb_addResultChild(addResultChild);
                this.Invoke(c, new object[] { value });
            }
            else
            {
                tn.Nodes.Add(new TreeNode(value));
            }
        }
        private void clearTestResults()
        {
            if (this.tvTests.InvokeRequired == true)
            {
                cb_clearTestResults c = new cb_clearTestResults(clearTestResults);
                this.Invoke(c);
            }
            else
            {
                tvTests.Nodes.Clear();
            }
        }
        private void clearScanBox()
        {
            if (this.txtScanData.InvokeRequired == true)
            {
                cb_clearScanBox c = new cb_clearScanBox(clearScanBox);
                this.Invoke(c);
            }
            else
            {
                txtScanData.Visible = true;
                txtScanData.Text = "";
                txtScanData.Focus();
            }
        }

        private void ScanBoxVisible(bool set)
        {
            if (this.txtScanData.InvokeRequired == true)
            {
                cb_ScanBoxVisible c = new cb_ScanBoxVisible(ScanBoxVisible);
                this.Invoke(c, new object[] { set });
            }
            else
            {
                txtScanData.Visible = set;                
            }
        }

        private void setPgFeedback(int min, int max, int value)
        {
            if (this.pgFeedback.InvokeRequired == true)
            {
                cb_setPgFeedback c = new cb_setPgFeedback(setPgFeedback);
                this.Invoke(c, new object[] { min, max, value });
            }
            else
            {
                pgFeedback.Minimum = min;
                pgFeedback.Maximum = max;
                pgFeedback.Value = value;
            }
        }
        private void runTestSequence(object sender, DoWorkEventArgs e)
        {
            long boxid = -1;

            string lastTest = "";
            string paramValue = "";
            bool testResult = false;
            int testCount = 0;
            int pcbVar = 255;

            string testSoftVer = "";

            TestControlVariables tcv = (TestControlVariables)e.Argument;

            tcv.results["ateFailCount"] = "0";
            tcv.results["ictPcbId"] = "";
            tcv.results["ictPcbFailCount"] = "-1";
            tcv.results["ictPcbPassFail"] = "-1";
            tcv.results["ictPcbTestDate"] = "";

            tcv.results["imei"] = "";
            tcv.results["imsi"] = "";
            tcv.results["simNumber"] = "";
            tcv.results["phoneNumber"] = "";

            failureLabel fl = new failureLabel();
            fl.JigIdent = tcv.test.JigIdent.ToString();



            voidLabelZebra vl;

            vl = new voidLabelZebra(voidLabelSoftVerJig1, voidLabelCommentJig1);

            //vl.Print();

            lastTest = "ScanBoxID";
            

            clearTestResults();
            addTestNode("ScanBoxID");

            lock (jcLock)
            {
                


                displayMessage("Please scan BOX ID");
                clearScanBox();

                this.KeyPreview = true;
                m_BoxID = "";

                DateTime timeout = DateTime.Now.AddSeconds(60);
                testResult = false;


                while (true)
                {
                    if (globals.cancelTest == true) break;

                    if (scanningBoxID == false)
                    {
                        if (m_BoxID.Length == 11)
                        {
                            break;
                        }
                        else
                        {
                            m_BoxID = "";
                            scanningBoxID = true;
                        }
                    }
                    Application.DoEvents();
                }

                ////m_BoxID = "20150010535"; //voda
                //m_BoxID = "20150010436";//kpn //does not program
                //m_BoxID = "20150010565";//kpn //does not program
                //m_BoxID = "20150010579";//kpn 

                if (m_BoxID.Length == 11)
                {
                    if (long.TryParse(m_BoxID, out boxid) == true)
                    {
                        testResult = true;
                    }
                    else
                    {
                        boxid = -1;
                        testResult = false;
                    }
                }
                else
                {
                    testResult = false;
                }

                displayMessage("");
                ScanBoxVisible(false);
                

            }
            fl.TestID = "Scan Box ID";
            fl.TestLL = "";
            fl.TestUL = "";
            fl.TestResult = m_BoxID.ToString();

            tcv.results["boxID"] = boxid.ToString();

            if (testResult == true)
            {
                startTimeJig = DateTime.Now;
                testResult = false;
                try
                {
                    foreach (TestItem t in tcv.tc.testSequence)
                    {
                        testCount++;                       

                        setTestProgress(0,tcv.tc.testSequence.Count+1,testCount);

                        addTestNode(t.id.ToString());
                        displayMessage("");


                        lastTest = t.id.ToString();

                        fl.TestID = lastTest;

                        if (t.runTest == true)
                        {

                            tcv.results["testSequence"] += t.id.ToString() + ", ";

                            switch (t.id)
                            {
                                case Tests.TestID.CurrentDrawA:
                                    {
                                        #region
                                        double pV = 12.0;
                                        double cL = 2.0;
                                        double ll = 40.0;
                                        double ul = 200.0;
                                        double testValue;
                                        if (t.getParamValue("powerUpVoltage", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out pV) == false)
                                            {
                                                pV = 12.0;
                                            }
                                        }
                                        if (t.getParamValue("currentLimit", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out cL) == false)
                                            {
                                                pV = 2.0;
                                            }
                                        }
                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 40.0;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 12.0;
                                            }
                                        }
                                        #endregion

                                        addResultChild("LL = " + ll.ToString());
                                        addResultChild("UL = " + ul.ToString());

                                        lock (jcLock)
                                        {
                                            testResult = tcv.test.currentDraw_A(pV, cL, ll, ul, out testValue);
                                            tcv.results["currentDrawA"] = testValue.ToString("F12");
                                        }

                                        fl.TestLL = ll.ToString();
                                        fl.TestUL = ul.ToString(); ;
                                        fl.TestResult = testValue.ToString();
                                        addResultChild("Result = " + testValue.ToString());
                                        break;
                                    }
                                case Tests.TestID.SleepCurrent:
                                    {
                                        #region
                                        double ll = 2.0;
                                        double ul = 15.0;
                                        double ul_Low = 8.0;
                                        double testValue;

                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 2.0;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 15.0;
                                            }
                                        }
                                        if (t.getParamValue("ul_Low", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul_Low) == false)
                                            {
                                                ul_Low = 8.0;
                                            }
                                        }
                                        #endregion

                                        addResultChild("LL = " + ll.ToString());
                                        addResultChild("UL = " + ul_Low.ToString());
                                        lock (jcLock)
                                        {

                                            try
                                            {
                                                pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                                            }
                                            catch (Exception err)
                                            {
                                                pcbVar = 255;
                                            }

                                            testResult = tcv.test.currentDraw_Sleep(ll, ul_Low, out testValue);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.currentDraw_Sleep(ll, ul_Low, out testValue);
                                            }
                                            fl.TestUL = ul_Low.ToString();

                                            tcv.results["sleepCurrent"] = testValue.ToString("F12");
                                        }

                                        fl.TestLL = ll.ToString();

                                        fl.TestResult = testValue.ToString();
                                        addResultChild("Result = " + testValue.ToString());
                                        break;
                                    }
                               
                                case Tests.TestID.LoadTestFirmwareJlink:
                                    {
                                        #region

                                        string firmwarePath = "";

                                        if (t.getParamValue("testFirmwarePath", ref paramValue) == true)
                                        {
                                            firmwarePath = filePath + paramValue;
                                        }
                                        #endregion

                                        addResultChild(firmwarePath);
                                        setPgFeedback(0, 100, 0);
                                        lock (jcLock)
                                        {
                                            int tResult = -1;
                                            tcv.test._jig.jl.progressStatus += jl_progressStatus;
                                            for (int i = 0; i < 2; i++)
                                            {
                                                tcv.test._jig.setPowerSupply(13.2, globals.defaultCurrent);
                                                Thread.Sleep(1000);
                                                tcv.test._jig.setPowerSupplyState(true);
                                                Thread.Sleep(1000);
                                                tcv.test._jig.setVbatt(true);
                                                Thread.Sleep(1500);
                                                //tcv.test._jig.lockJig(true);

                                                tcv.test._jig.jl.connect(Properties.Settings.Default.Processor);
                                                tResult = tcv.test._jig.jl.program(firmwarePath, false);
                                                if (tResult < 0)
                                                {
                                                    testResult = false;
                                                    fl.TestUL = "Err Code " + tResult.ToString();
                                                    addResultChild(fl.TestUL);

                                                    tcv.test._jig.jl.progressStatus -= new reportProgress(jl_progressStatus);
                                                    tcv.test._jig.jl.close();
                                                    tcv.test._jig.jl = new jLinkAPI();
                                                    tcv.test._jig.jl.progressStatus += jl_progressStatus;

                                                }
                                                else
                                                {
                                                    testResult = true;
                                                }
                                                tcv.test._jig.jl.close();
                                                if (testResult == true) break;

                                                Application.DoEvents();
                                                Thread.Sleep(100);
                                                tcv.test._jig.setVbatt(false);
                                            }
                                            tcv.test._jig.jl.progressStatus -= new reportProgress(jl_progressStatus);
                                        }
                                        
                                        setPgFeedback(0, 100, 0);
                                        if (testResult == false)
                                        {
                                            fl.TestLL = "";
                                            
                                            fl.TestResult = "Unable to program";
                                        }
                                        else
                                        {
                                            fl.TestLL = "";
                                            fl.TestUL = "";
                                            fl.TestResult = "";
                                        }
                                        break;
                                    }
                                case Tests.TestID.BatterySwitchingPoint:
                                    {
                                        #region
                                        double pV = 13.0;
                                        double cL = 1.0;
                                        double rtv = 13.8;
                                        double rampInc = 0.05;
                                        double upll = 13.3;
                                        double upul = 13.5;
                                        double downll = 13.1;
                                        double downul = 13.3;
                                        double testValue;
                                        double testValue1;

                                        if (t.getParamValue("powerUpVoltage", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out pV) == false)
                                            {
                                                pV = 13.0;
                                            }
                                        }
                                        if (t.getParamValue("currentLimit", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out cL) == false)
                                            {
                                                pV = 2.0;
                                            }
                                        }
                                        if (t.getParamValue("rampToVoltage", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out rtv) == false)
                                            {
                                                rtv = 13.0;
                                            }
                                        }
                                        if (t.getParamValue("rampIncrement", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out rampInc) == false)
                                            {
                                                rampInc = 0.05;
                                            }
                                        }
                                        if (t.getParamValue("upll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out upll) == false)
                                            {
                                                upll = 13.3;
                                            }
                                        }
                                        if (t.getParamValue("upul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out upul) == false)
                                            {
                                                upul = 13.5;
                                            }
                                        }
                                        if (t.getParamValue("downll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out downll) == false)
                                            {
                                                downll = 13.1;
                                            }
                                        }
                                        if (t.getParamValue("downul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out downul) == false)
                                            {
                                                downul = 13.3;
                                            }
                                        }
                                        #endregion

                                        addResultChild("upll = " + upll.ToString());
                                        addResultChild("upul = " + upul.ToString());
                                        addResultChild("downll = " + downll.ToString());
                                        addResultChild("downul = " + downul.ToString());
                                        
                                        lock (jcLock)
                                        {                                            
                                            testResult = tcv.test.batterySwitchingPoint(pV, cL, rtv, rampInc, upll, upul, downll, downul, out testValue, out testValue1);
                                        }

                                        tcv.results["batterySwitchPointUp"] = testValue.ToString("F12");
                                        tcv.results["batterySwitchPointDown"] = testValue1.ToString("F12");
                                        
                                        addResultChild("Result Up = " + testValue.ToString());
                                        addResultChild("Result Down = " + testValue1.ToString());

                                        fl.TestLL = upll.ToString();
                                        fl.TestUL = upul.ToString(); ;
                                        fl.TestResult = testValue.ToString();

                                        break;
                                    }
                                

                                case Tests.TestID.ReadProdFirmwareVersion:
                                    {
                                        #region
                                        string testValue = "";

                                        #endregion
                                        Thread.Sleep(500);
                                        testResult = tcv.test.readProdFirmwareVersion(out testValue);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.readProdFirmwareVersion(out testValue);
                                        }
                                        tcv.results["prodTestFwVersion"] = testValue;

                                        fl.TestLL = "Check Main RTU Connector";
                                        fl.TestUL = "Check PINs on test jig";
                                        if (testValue.Length > 1)
                                        {
                                            fl.TestResult = testValue.ToString();
                                        }
                                        else
                                        {
                                            fl.TestResult = "Error CAN comms";
                                        }

                                        if (testResult == true )
                                        {
                                            testSoftVer = testValue;
                                        }

                                        addResultChild("Result = " + testValue.ToString());
                                        break;
                                    }
                                case Tests.TestID.ReadProcessorVersion:
                                    {
                                        #region
                                        string testValue = "";
                                        if (t.getParamValue("hardwareVersion", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.hardwareVersion = paramValue;
                                        }
                                        #endregion



                                        testResult = tcv.test.readProcessorVersion(out testValue);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.readProcessorVersion(out testValue);
                                        }
                                        if (testResult == true)
                                        {
                                            
                                            if (testValue == "00000411")
                                            {
                                                
                                                try
                                                {
                                                    pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                                                }
                                                catch (Exception err)
                                                {
                                                    pcbVar = 255;
                                                }
                                                
                                                switch (pcbVar)
                                                {
                                                    case 0x20:
                                                        {
                                                            tcv.tc.rc.hardwareVersion = "91210200";
                                                            break;
                                                        }
                                                    case 0x21:
                                                        {
                                                            tcv.tc.rc.hardwareVersion = "81210200";
                                                            break;
                                                        }
                                                    case 0x22:
                                                        {
                                                            tcv.tc.rc.hardwareVersion = "91410200";
                                                            break;
                                                        }
                                                    case 0x23:
                                                        {
                                                            tcv.tc.rc.hardwareVersion = "99210200";
                                                            break;
                                                        }
                                                    case 0x24:
                                                        {
                                                            tcv.tc.rc.hardwareVersion = "99410200";
                                                            break;
                                                        }
                                                    default:
                                                        {
                                                            testResult = false;
                                                            break;
                                                        }
                                                }
                                            }
                                            else
                                            {
                                                testResult = false;
                                            }
                                        }                                        

                                        tcv.results["processorVersion"] = testValue;
                                        tcv.results["hardwareVariant"] = tcv.tc.rc.hardwareVersion;

                                        addResultChild("Processor = " + testValue.ToString());
                                        addResultChild("HW Variant = " + tcv.tc.rc.hardwareVersion);

                                        fl.TestLL = "Unknown Processor";
                                        fl.TestUL = "If no value check CAN connections";
                                        fl.TestResult = testValue.ToString();

                                        break;
                                    }

                                case Tests.TestID.readGyroID:
                                    {
                                        #region
                                        string testValue = "";
                                        paramValue = "";
                                        if (t.getParamValue("gyroID", ref paramValue) == true)
                                        {

                                        }
                                        #endregion


                                        addResultChild("ll = " + paramValue.ToString());
                                        //testResult = tcv.test.readGyroID(paramValue, out testValue, fl);
                                        testResult = true;
                                        testValue = "0xD4";


                                        if (testResult == true)
                                        {
                                            //ok continue
                                        }
                                        else
                                        {
                                            testResult = tcv.test.readGyroID(paramValue, out testValue, fl);
                                        }

                                        addResultChild("Result = " + testValue.ToString());
                                        tcv.results["gyroID"] = testValue;
                                       
                                        break;
                                    } 
                                case Tests.TestID.readGyroData:
                                    {
                                        #region

                                        int xll = -5, yll = -5, zll = -5;
                                        int xul = 5, yul = 5, zul = 5;
                                        int testValue = 0;
                                        int testValue1 = 0;
                                        int testValue2 = 0;

                                        if (t.getParamValue("xll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out xll) == false)
                                            {
                                                xll = -5;
                                            }
                                        }
                                        if (t.getParamValue("xul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out xul) == false)
                                            {
                                                xul = 5;
                                            }
                                        }
                                        if (t.getParamValue("yll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out yll) == false)
                                            {
                                                yll = -5;
                                            }
                                        }
                                        if (t.getParamValue("yul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out yul) == false)
                                            {
                                                yul = 5;
                                            }
                                        } if (t.getParamValue("zll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out zll) == false)
                                            {
                                                zll = -5;
                                            }
                                        }
                                        if (t.getParamValue("zul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out zul) == false)
                                            {
                                                zul = 5;
                                            }
                                        }

                                        #endregion

                                        addResultChild("xll = " + xll.ToString());
                                        addResultChild("xul = " + xul.ToString());
                                        addResultChild("yll = " + yll.ToString());
                                        addResultChild("yul = " + yul.ToString());
                                        addResultChild("zll = " + zll.ToString());
                                        addResultChild("zul = " + zul.ToString());

                                        
                                        testResult = tcv.test.gyroData(xll, xul, yll, yul, zll, zul, out testValue, out testValue1, out testValue2, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.gyroData(xll, xul, yll, yul, zll, zul, out testValue, out testValue1, out testValue2, fl);
                                        }
                                        
                                        
                                        tcv.results["gyroX"] = testValue.ToString();
                                        tcv.results["gyroY"] = testValue1.ToString();
                                        tcv.results["gyroZ"] = testValue2.ToString();

                                        addResultChild("gyroX = " + testValue.ToString());
                                        addResultChild("gyroY = " + testValue1.ToString());
                                        addResultChild("gyroZ = " + testValue2.ToString());

                                        break;
                                    }
                                case Tests.TestID.BatteryADReadings:
                                    {
                                        #region
                                        double pv1 = 12.8, pv2 = 13.0, pv3 = 13.2;
                                        double cL = 1.0;
                                        double lowll = 400.0, midll = 420.0, highll = 440.0;
                                        double lowul = 410.0, midul = 430.0, highul = 450.0;
                                        int testValue;
                                        int testValue1;
                                        int testValue2;
                                        if (t.getParamValue("voltageLow", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out pv1) == false)
                                            {
                                                pv1 = 12.8;
                                            }
                                        }
                                        if (t.getParamValue("voltageMid", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out pv2) == false)
                                            {
                                                pv2 = 13.0;
                                            }
                                        }
                                        if (t.getParamValue("voltageHigh", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out pv3) == false)
                                            {
                                                pv3 = 13.2;
                                            }
                                        }
                                        if (t.getParamValue("currentLimit", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out cL) == false)
                                            {
                                                cL = 1.0;
                                            }
                                        }
                                        if (t.getParamValue("lowll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out lowll) == false)
                                            {
                                                lowll = 400.0;
                                            }
                                        }
                                        if (t.getParamValue("lowul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out lowul) == false)
                                            {
                                                lowul = 410.0;
                                            }
                                        }
                                        if (t.getParamValue("midll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out midll) == false)
                                            {
                                                midll = 420.0;
                                            }
                                        }
                                        if (t.getParamValue("midul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out midul) == false)
                                            {
                                                midul = 430.0;
                                            }
                                        }
                                        if (t.getParamValue("highll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out highll) == false)
                                            {
                                                highll = 440.0;
                                            }
                                        }
                                        if (t.getParamValue("highul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out highul) == false)
                                            {
                                                highul = 450.0;
                                            }
                                        }
                                        #endregion

                                        addResultChild("lowll = " + lowll.ToString());
                                        addResultChild("lowul = " + lowul.ToString());
                                        addResultChild("midll = " + midll.ToString());
                                        addResultChild("midul = " + midul.ToString());
                                        addResultChild("highll = " + highll.ToString());
                                        addResultChild("highul = " + highul.ToString());

                                        lock (jcLock)
                                        {                                            
                                            testResult = tcv.test.batteryADReadings(cL, pv1, lowll, lowul, pv2, midll, midul, pv3, highll, highul, out testValue, out testValue1, out testValue2, fl);
                                            if (testResult == false)
                                            {
                                                tcv.test._jig.setVbatt(false);
                                                testResult = tcv.test.batteryADReadings(cL, pv1, lowll, lowul, pv2, midll, midul, pv3, highll, highul, out testValue, out testValue1, out testValue2, fl);
                                            }
                                        }
                                        tcv.results["adBatteryLow"] = testValue.ToString();
                                        tcv.results["adBatteryMid"] = testValue1.ToString();
                                        tcv.results["adBatteryHigh"] = testValue2.ToString();

                                        addResultChild("adBatteryLow = " + testValue.ToString());
                                        addResultChild("adBatteryMid = " + testValue1.ToString());
                                        addResultChild("adBatteryHigh = " + testValue2.ToString());

                                        break;
                                    }

                                case Tests.TestID.SetRealTimeClock:
                                    {
                                        #region
                                        DialogResult retryTest;
                                        int chargingDelay = 15;
                                        if (t.getParamValue("chargingDelay", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out chargingDelay) == false)
                                            {
                                                chargingDelay = 15;
                                            }
                                        }
                                        #endregion

                                        
                                        lock (jcLock)
                                        {
                                            
                                            testResult = tcv.test.setRealTimeClock(chargingDelay);

                                            if (testResult == false)
                                            {
                                                for (int retryLoop = 0; retryLoop < 3; retryLoop++)
                                                {
                                                    retryTest = MessageBox.Show("Do you want to retest Real Time Clock", "", MessageBoxButtons.YesNo);
                                                    if (retryTest == DialogResult.Yes)
                                                    {
                                                        testResult = tcv.test.setRealTimeClock(chargingDelay * 2);
                                                        if (testResult == true) break;
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testResult.ToString();
                                        break;
                                    }

                                case Tests.TestID.FullFlashErase:
                                    {
                                        testResult = tcv.test.fullFlashErase();
                                        fl.TestLL = "Check Soldering of Flash";
                                        fl.TestUL = "";
                                        fl.TestResult = testResult.ToString();
                                        break;
                                    }
                                case Tests.TestID.FlashTestRoutine:
                                    {
                                        testResult = tcv.test.flashTestRoutine();
                                        fl.TestLL = "Check IO lines to Flash";
                                        fl.TestUL = "";
                                        fl.TestResult = testResult.ToString();
                                        break;
                                    }
                                case Tests.TestID.LoadDefaultConfiguration:
                                    {
                                        testResult = tcv.test.loadDefaultConfiguration();
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.loadDefaultConfiguration();
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testResult.ToString();
                                        break;
                                    }
                                case Tests.TestID.readIMEI:
                                    {
                                        #region
                                        string testValue = "";
                                        int timeoutSeconds = 60;
                                        DateTime timeout;

                                        if (t.getParamValue("timeout", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out timeoutSeconds) == false)
                                            {
                                                timeoutSeconds = 60;
                                            }
                                        }

                                        timeout = DateTime.Now.AddSeconds(timeoutSeconds);
                                        #endregion

                                        while (DateTime.Now.CompareTo(timeout) < 0)
                                        {
                                            testResult = tcv.test.readIMEI(out testValue);
                                            if (testResult == true)
                                            {
                                                break;
                                            }
                                            Application.DoEvents();
                                            if (globals.cancelTest == true) break;
                                        }
                                        tcv.results["imei"] = testValue;

                                        addResultChild("Imei = " + testValue.ToString());
                                        string gsmStatus = "";
                                        addResultChild("GSM Status = " + tcv.test._sc.getGsmStatus(out gsmStatus).ToString());
                                        addResultChild("GSM Status = " + gsmStatus);
                                        addResultChild("GSM On Network = " + tcv.test._sc.isOnNetwork().ToString());
                                        fl.TestLL = gsmStatus;
                                        fl.TestUL = "";
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.ReadIMSI:
                                    {
                                        #region
                                        string testValue = "";
                                        #endregion
                                        testResult = tcv.test.readIMSI(out testValue);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.readIMSI(out testValue);
                                        }
                                        tcv.results["imsi"] = testValue;
                                        addResultChild("Imsi = " + testValue.ToString());
                                        string gsmStatus = "";
                                        addResultChild("GSM Status = " + tcv.test._sc.getGsmStatus(out gsmStatus).ToString());
                                        addResultChild("GSM Status = " + gsmStatus);
                                        fl.TestLL = gsmStatus;
                                        fl.TestUL = "";
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.ReadModemType:
                                    {
                                        #region
                                        string testValue = "";
                                        #endregion
                                        testResult = tcv.test.readModemType(out testValue);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.readModemType(out testValue);
                                        }
                                        tcv.results["modemType"] = testValue;
                                        addResultChild("modemType = " + testValue.ToString());
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.CheckModemFirmwareVersion:
                                    {
                                        #region
                                        string testValue = "";                                       
                                        List<string> validFirmwareVar20 = new List<string>();
                                        List<string> validFirmwareVar21 = new List<string>();
                                        List<string> validFirmwareVar22 = new List<string>();
                                        List<string> validFirmwareVar23 = new List<string>();
                                        List<string> validFirmwareVar24 = new List<string>();

                                        foreach (TestParameter ltp in t.testParams)
                                        {
                                            if (ltp.description.StartsWith("firmwareVersionVar20_Valid") == true)
                                            {
                                                validFirmwareVar20.Add(ltp.value);
                                                addResultChild("FW_20 = " + ltp.value);
                                            }
                                            else if (ltp.description.StartsWith("firmwareVersionVar21_Valid") == true)
                                            {
                                                validFirmwareVar21.Add(ltp.value);
                                                addResultChild("FW_21 = " + ltp.value);
                                            }
                                            else if (ltp.description.StartsWith("firmwareVersionVar22_Valid") == true)
                                            {
                                                validFirmwareVar22.Add(ltp.value);
                                                addResultChild("FW_22 = " + ltp.value);
                                            }
                                            else if (ltp.description.StartsWith("firmwareVersionVar23_Valid") == true)
                                            {
                                                validFirmwareVar23.Add(ltp.value);
                                                addResultChild("FW_23 = " + ltp.value);
                                            }
                                            else if (ltp.description.StartsWith("firmwareVersionVar24_Valid") == true)
                                            {
                                                validFirmwareVar24.Add(ltp.value);
                                                addResultChild("FW_24 = " + ltp.value);
                                            }
                                            else
                                            {
                                                //Not firmware version paramter therefore skip
                                            }
                                        }
                                        #endregion

                                        try
                                        {
                                            pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                                        }
                                        catch (Exception err)
                                        {
                                            pcbVar = 255;
                                        }

                                        addResultChild("pcbVar = " + pcbVar.ToString());
                                        switch (pcbVar)
                                        {
                                            case 0x20:
                                                {
                                                    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar20, out testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar20, out testValue);
                                                    }
                                                    break;
                                                }
                                            case 0x21:
                                                {
                                                    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar21, out testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar21, out testValue);
                                                    }
                                                    break;
                                                }
                                            case 0x22:
                                                {
                                                    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar22, out testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar22, out testValue);
                                                    }
                                                    break;
                                                }
                                            case 0x23:
                                                {
                                                    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar23, out testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar23, out testValue);
                                                    }
                                                    break;
                                                }
                                            case 0x24:
                                                {
                                                    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar24, out testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar24, out testValue);
                                                    }
                                                    break;
                                                }
                                            default:
                                                {
                                                    testResult = false;
                                                    break;
                                                }
                                        }
                                        //if (pcbVar == 0x10)
                                        //{
                                        //    testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar10, out testValue);
                                        //    if (testResult == false)
                                        //    {
                                        //        testResult = tcv.test.checkModemFirmwareVersion(validFirmwareVar10, out testValue);
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    testResult = false;
                                        //}
                                        tcv.results["modemFirmwareVersion"] = testValue;
                                        addResultChild("Result = " + testValue.ToString());
                                        string gsmStatus = "";
                                        addResultChild("GSM Status = " + tcv.test._sc.getGsmStatus(out gsmStatus).ToString());
                                        addResultChild("GSM Status = " + gsmStatus);
                                        fl.TestLL = "Check file for limits";
                                        fl.TestUL = "";
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.ReadPhoneNumber:
                                    {
                                        #region
                                        string testValue = "";

                                        //2b3331393730303233353232383600 kpn rik new
                                        //2b34343738323532373838373000 voda rik old
                                        //2b34343738363030333538353500 itagg 02
                                        //2b34343737383632303130373600 itagg voda
                                        string testPhoneNumber = Properties.Settings.Default.PhoneNumber;

                                        int numberLength = tcv.tc.rc.phoneNumberLength;
                                        int timeoutSms = 60;

                                        if (t.getParamValue("smsTestTimeoutSeconds", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out timeoutSms) == false)
                                            {
                                                timeoutSms = 60;
                                            }
                                        }

                                        #endregion

                                        addResultChild("expectedPhoneNumLen = " + numberLength.ToString());
                                        addResultChild("timeoutSms = " + timeoutSms.ToString());
                                        addResultChild("testPhoneNum = " + testPhoneNumber.ToString());

                                        fl.TestLL = "";
                                        fl.TestUL = "";

                                        setPgFeedback(0,181,0);
                                        displayMessage("Waiting for GSM connection");
                                        int lCnt = 0;
                                        for (lCnt = 0; lCnt < 180; lCnt++)
                                        {
                                            if (tcv.test._sc.isOnNetwork() == 1)
                                            {
                                                testResult = true;
                                                fl.TestLL = "";
                                                break;
                                            }
                                            else
                                            {
                                                testResult = false;
                                                fl.TestLL = "RTU not on network";
                                            }
                                            Application.DoEvents();
                                            Thread.Sleep(500);
                                            Application.DoEvents();
                                            setPgFeedback(0,181,lCnt);
                                        }
                                        try
                                        {
                                            lCnt = (int)(lCnt/2);
                                        }
                                        catch(Exception err)
                                        {
                                            System.Diagnostics.Debug.WriteLine(err.Message);
                                        }
                                        setPgFeedback(0,181,0);
                                        addResultChild("TimeToLogOn = " + lCnt.ToString());
                                        addResultChild("simIdType = " + tcv.tc.rc.simIdType.ToString());
                                        if (testResult == true)
                                        {
                                            displayMessage("RTU connected to GSM");
                                            if (tcv.tc.rc.manualPhoneNum == false)
                                            {
                                                if (tcv.tc.rc.simIdType == 33) //KPN
                                                {
                                                    int Cnt = 0;
                                                    for (Cnt = 0; Cnt < 3; Cnt++)
                                                    {
                                                        testValue = kpn.getPhoneNumber(tcv.results["simNumber"]);
                                                        tcv.results["phoneNumber"] = testValue;
                                                        if (tcv.results["phoneNumber"] == "")
                                                        {
                                                            fl.TestLL = "KPN Look up Failed";
                                                            displayMessage(fl.TestLL);
                                                            testResult = false;
                                                            Thread.Sleep(1000);
                                                        }
                                                        else
                                                        {
                                                            testResult = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else if (tcv.tc.rc.simIdType == 51) //Jersey
                                                {
                                                    //testValue = jerseyTelecomInterface.getPhoneNumberLookup(tcv.results["imsi"]);   
                                                    int Cnt = 0;
                                                    for (Cnt = 0; Cnt < 3; Cnt++)
                                                    {
                                                        testValue = AmlConnector.getJerseyPhoneNumber(tcv.results["imsi"]);
                                                        tcv.results["phoneNumber"] = testValue;
                                                        if (tcv.results["phoneNumber"] == "")
                                                        {
                                                            fl.TestLL = "JerseyTelecom Look up Failed";
                                                            displayMessage(fl.TestLL);
                                                            testResult = false;
                                                            Thread.Sleep(1000);
                                                        }
                                                        else
                                                        {
                                                            testResult = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    setPgFeedback(0,361,0);
                                                    displayMessage("Attempting to Read Phone Number");
                                                    for (int lcnt = 0; lcnt < 360; lcnt++)
                                                    {
                                                        testResult = tcv.test.readPhoneNumber(out testValue);
                                                        if (testResult == false)
                                                        {
                                                            Thread.Sleep(1000);
                                                        }
                                                        else
                                                        {
                                                            if (testValue == "")
                                                            {
                                                                Thread.Sleep(1000);
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }
                                                        }
                                                        if (globals.cancelTest == true)
                                                        {
                                                            testResult = false;
                                                            break;
                                                        }
                                                        setPgFeedback(0,361,lcnt);
                                                    }
                                                    setPgFeedback(0,361,0);
                                                    displayMessage("");
                                                }

                                                addResultChild("PhoneNumber = " + testValue.ToString());
                                                if (testResult == true)
                                                {
                                                    tcv.results["phoneNumber"] = testValue;
                                                    fl.TestLL = "";
                                                    fl.TestUL = "";
                                                    fl.TestResult = testValue.ToString();

                                                    testResult = tcv.test.writeTelephoneNumber(testValue);
                                                    if (testResult == false)
                                                    {
                                                        testResult = tcv.test.writeTelephoneNumber(testValue);
                                                    }
                                                    if (tcv.tc.rc.sendTestSms == true && testResult == true)
                                                    {
                                                        string intFormatPhone = "";
                                                        switch (tcv.tc.rc.simIdType)
                                                        {
                                                            case 33: //kpn
                                                                intFormatPhone = "31" + testValue.Substring(1);
                                                                break;
                                                            case 51: // Jersey Sim
                                                                if (testValue.Substring(0,2) == "88")   // Long phone number prefixed with 88
                                                                    intFormatPhone = testValue.Substring(0);
                                                                else
                                                                    intFormatPhone = "44" + testValue.Substring(1);
                                                                break;
                                                            default:
                                                                intFormatPhone = "44" + testValue.Substring(1);
                                                                break;

                                                        }
                                                        displayMessage("Sending SMS");
                                                        testResult = tcv.test.sendSms(testValue,intFormatPhone, timeoutSms, ref globals.cancelTest, fl, testPhoneNumber, setPgFeedback);
                                                        if (testResult == false)
                                                        {
                                                            testResult = tcv.test.sendSms(testValue, intFormatPhone, timeoutSms, ref globals.cancelTest, fl, testPhoneNumber, setPgFeedback);
                                                        }
                                                        if (testResult == true)
                                                        {
                                                            addResultChild("SendSms = True");
                                                        }
                                                        else
                                                        {
                                                            addResultChild("SendSms = False");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    fl.TestLL = "Unable to Get PhoneNumber";
                                                }

                                            }
                                            else
                                            {
                                                
                                                lock (jcLock)
                                                {
                                                   
                                                    displayMessage("Please scan phone number");

                                                    scanningPhoneNumber = true;
                                                    this.KeyPreview = true;
                                                    m_PhoneNumber = "";

                                                    DateTime timeout = DateTime.Now.AddSeconds(60);
                                                    testResult = false;

                                                    
                                                    while (true)
                                                    {
                                                        if (globals.cancelTest == true) break;

                                                        if (scanningPhoneNumber == false)
                                                        {
                                                            if (m_PhoneNumber.Length == numberLength)
                                                            {
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                m_PhoneNumber = "";
                                                               
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }

                                                    if (m_PhoneNumber.Length == numberLength)
                                                    {
                                                        testResult = true;

                                                        displayMessage("");
                                                        
                                                        addResultChild("PhoneNumber = " + testValue.ToString());

                                                        testResult = tcv.test.writeTelephoneNumber(m_PhoneNumber);
                                                        if (testResult == false)
                                                        {
                                                            testResult = tcv.test.writeTelephoneNumber(m_PhoneNumber);
                                                        }
                                                        if (tcv.tc.rc.sendTestSms == true && testResult == true)
                                                        {
                                                            string intFormatPhone = "";
                                                            switch (tcv.tc.rc.simIdType)
                                                            {
                                                                case 33: //kpn
                                                                    intFormatPhone = "31" + testValue.Substring(1);
                                                                    break;
                                                                default:
                                                                    intFormatPhone = "44" + testValue.Substring(1);
                                                                    break;

                                                            }
                                                            testResult = tcv.test.sendSms(m_PhoneNumber,intFormatPhone, timeoutSms, ref globals.cancelTest, fl, testPhoneNumber, setPgFeedback);
                                                            if(testResult == true)
                                                            {
                                                                addResultChild("SendSms = True");
                                                            }
                                                            else
                                                            {
                                                                addResultChild("SendSms = False");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        testResult = false;
                                                    }

                                                    testValue = m_PhoneNumber;
                                                    
                                                }

                                                tcv.results["phoneNumber"] = testValue;

                                                fl.TestResult = testValue.ToString();
                                            }


                                            if (testValue.StartsWith("0") == false)
                                            {
                                                if (testValue.StartsWith("88") == true)
                                                {
                                                    //JERSEY LONG NUMBERS
                                                }
                                                else if (testValue.StartsWith("58") == true)
                                                {
                                                    //telenor
                                                }
                                                else if (testValue.StartsWith("47") == true)
                                                {
                                                    //kpn
                                                }
                                                else if (testValue.StartsWith("60") == true)
                                                {
                                                    //kpn
                                                }
                                                else if (testValue.Length == 0)
                                                {
                                                    fl.TestLL = "Number Not Read";
                                                    testResult = false;
                                                }
                                                else
                                                {
                                                    fl.TestLL = "Number Format";
                                                    testResult = false;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case Tests.TestID.SendUdp:
                                    {
                                        #region
                                        string testValue = "";

                                        
                                        string testUdpIp = Properties.Settings.Default.UdpIP;
                                        string testUdpPort = Properties.Settings.Default.UdpPort;

                                        int numberLength = tcv.tc.rc.phoneNumberLength;
                                        int timeoutudp = 60;

                                        if (t.getParamValue("sendTimeout", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out timeoutudp) == false)
                                            {
                                                timeoutudp = 60;
                                            }
                                        }

                                        #endregion

                                        
                                        addResultChild("timeoutUdp = " + timeoutudp.ToString());
                                        addResultChild("testUdpIp = " + testUdpIp.ToString());
                                        addResultChild("testUdpPort = " + testUdpPort.ToString());

                                        fl.TestLL = "";
                                        fl.TestUL = "";

                                        setPgFeedback(0, 181, 0);
                                        displayMessage("Waiting for GSM connection");
                                        int lCnt = 0;
                                        for (lCnt = 0; lCnt < 180; lCnt++)
                                        {
                                            if (tcv.test._sc.isOnNetwork() == 1)
                                            {
                                                displayMessage("RTU connected to GSM");
                                                testResult = true;
                                                fl.TestLL = "";
                                                break;
                                            }
                                            else
                                            {
                                                testResult = false;
                                                fl.TestLL = "RTU not on network";
                                            }
                                            Application.DoEvents();
                                            Thread.Sleep(500);
                                            Application.DoEvents();
                                            setPgFeedback(0, 181, lCnt);
                                        }
                                        try
                                        {
                                            lCnt = (int)(lCnt / 2);
                                        }
                                        catch (Exception err)
                                        {
                                            System.Diagnostics.Debug.WriteLine(err.Message);
                                        }
                                        setPgFeedback(0, 181, 0);
                                        addResultChild("TimeToLogOn = " + lCnt.ToString());
                                        
                                        if (testResult == true)
                                        {   
                                            setPgFeedback(0, timeoutudp+1, 0);
                                            testResult = tcv.test.sendUdp((string)tcv.results["imei"], timeoutudp, ref globals.cancelTest, fl, testUdpIp, testUdpPort, setPgFeedback);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.sendUdp((string)tcv.results["imei"], timeoutudp, ref globals.cancelTest, fl, testUdpIp, testUdpPort, setPgFeedback);
                                            }
                                            setPgFeedback(0, timeoutudp + 1, 0);
                                        }
                                        break;
                                    }
                                case Tests.TestID.GPSStatus:
                                    {
                                        #region
                                        string testValue = "";
                                        int timeoutSeconds = 60;
                                        

                                        if (t.getParamValue("timeout", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out timeoutSeconds) == false)
                                            {
                                                timeoutSeconds = 60;
                                            }
                                        }
                                        
                                        #endregion

                                        //while (DateTime.Now.CompareTo(timeout) < 0)

                                        int gpsCnt = 0;
                                        setPgFeedback(0,timeoutSeconds+1,0);
                                        for( gpsCnt = 0; gpsCnt<timeoutSeconds;gpsCnt++)
                                        {
                                            testResult = tcv.test.readGPSStatus(out testValue);
                                            if (testResult == true)
                                            {
                                                break;
                                            }
                                            Application.DoEvents();
                                            for (int cnt = 0; cnt < 10; cnt++)
                                            {
                                                Thread.Sleep(100);
                                                Application.DoEvents();
                                            }
                                            if (globals.cancelTest == true) break;
                                            setPgFeedback(0,timeoutSeconds+1,gpsCnt);
                                        }
                                        setPgFeedback(0,timeoutSeconds+1,0);
                                        addResultChild("Time for GPS status = " + gpsCnt.ToString());
                                        tcv.results["gpsStatus"] = testValue;

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testValue.ToString();

                                        break;
                                    }
                                case Tests.TestID.GPSVoltage:
                                    {
                                        #region
                                        double pV = 12.0;
                                        double cL = 2.0;
                                        double ll = 2.4;
                                        double ul = 2.9;
                                        double lljig2 = 2.72;
                                        double uljig2 = 2.77;
                                        double llv6 = 3.1;
                                        double ulv6 = 3.2;
                                        double testValue;

                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 2.4;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 2.9;
                                            }
                                        }
                                        if (t.getParamValue("lljig2", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out lljig2) == false)
                                            {
                                                lljig2 = 2.72;
                                            }
                                        }
                                        if (t.getParamValue("uljig2", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out uljig2) == false)
                                            {
                                                uljig2 = 2.77;
                                            }
                                        }
                                        if (t.getParamValue("llv6", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out llv6) == false)
                                            {
                                                llv6 = 3.1;
                                            }
                                        }
                                        if (t.getParamValue("ulv6", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ulv6) == false)
                                            {
                                                ulv6 = 3.3;
                                            }
                                        }
                                        #endregion
                                        
                                        addResultChild("ll = " + ll.ToString());
                                        addResultChild("ul = " + ul.ToString());
                                        lock (jcLock)
                                        {
                                            

                                            try
                                            {
                                                pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                                            }
                                            catch (Exception err)
                                            {
                                                pcbVar = 255;
                                            }

                                           
                                            testResult = tcv.test.gpsVoltage(ll, ul, out testValue);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.gpsVoltage(ll, ul, out testValue);
                                            }
                                            fl.TestLL = ll.ToString();
                                            fl.TestUL = ul.ToString();
                                            fl.TestResult = testValue.ToString();

                                            addResultChild("Result = " + testValue.ToString());
                                               
                                        }
                                        tcv.results["gpsVoltage"] = testValue.ToString("F12");

                                        break;
                                    }
                                case Tests.TestID.KLineVoltageNoPullup:
                                    {
                                        #region
                                        double ll = 0.0;
                                        double ul = 0.5;
                                        double testValue;

                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 0.0;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 0.5;
                                            }
                                        }
                                        #endregion

                                        addResultChild("ll = " + ll.ToString());
                                        addResultChild("ul = " + ul.ToString());
                                        lock (jcLock)
                                        {
                                            
                                            testResult = tcv.test.KLineVoltageNoPullup(ll, ul, out testValue);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.KLineVoltageNoPullup(ll, ul, out testValue);
                                            }
                                        }
                                        addResultChild("kLineVoltNoPullup = " + testValue.ToString());

                                        tcv.results["kLineVoltageNoPullup"] = testValue.ToString("F12");
                                        fl.TestLL = ll.ToString();
                                        fl.TestUL = ul.ToString();
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.KLineVoltageWithPullup:
                                    {
                                        #region
                                        double ll = 1.75;
                                        double ul = 2.5;
                                        double testValue;

                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 1.75;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 2.5;
                                            }
                                        }
                                        #endregion

                                        addResultChild("ll = " + ll.ToString());
                                        addResultChild("ul = " + ul.ToString());
                                        lock (jcLock)
                                        {                                            
                                            testResult = tcv.test.KLineVoltageWithPullup(ll, ul, out testValue);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.KLineVoltageWithPullup(ll, ul, out testValue);
                                            }
                                        }
                                        addResultChild("kLineVoltWithPullup = " + testValue.ToString());
                                        
                                        tcv.results["kLineVoltageWithPullup"] = testValue.ToString("F12");
                                        fl.TestLL = ll.ToString();
                                        fl.TestUL = ul.ToString();
                                        fl.TestResult = testValue.ToString();
                                        break;
                                    }
                                case Tests.TestID.KLineCommsTest:
                                    {
                                        
                                        lock (jcLock)
                                        {                                            
                                            testResult = tcv.test.KLineCommsTest();
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.KLineCommsTest();
                                            }
                                        }

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "K Line Send/Receive Failed";
                                        break;
                                    }
                                case Tests.TestID.IgnitionLine:
                                    {
                                        #region
                                        int testValue = -2;
                                        int testValue1 = -2;
                                        #endregion

                                        
                                        lock (jcLock)
                                        {                                            
                                            testResult = tcv.test.ignitionLine(out testValue, out testValue1, fl);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.ignitionLine(out testValue, out testValue1, fl);
                                            }
                                        }

                                        tcv.results["IgnitionDisconnected"] = testValue.ToString();
                                        tcv.results["IgnitionConnected"] = testValue1.ToString();

                                        addResultChild("IgnDisconnected = " + testValue.ToString());
                                        addResultChild("IgnConnected = " + testValue.ToString());
                                        break;
                                    }
                                case Tests.TestID.AuxPowerSupply:
                                    {
                                        #region

                                        double ll = 12.8;
                                        double ul = 13.8;
                                        double testValue;

                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 0.00085;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (double.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 0.001;
                                            }
                                        }
                                        #endregion

                                        addResultChild("ll = " + ll.ToString());
                                        addResultChild("ul = " + ul.ToString());
                                        lock (jcLock)
                                        {
                                            
                                            testResult = tcv.test.AuxPowerSupply(ll, ul, out testValue);
                                            if (testResult == false)
                                            {
                                                testResult = tcv.test.AuxPowerSupply(ll, ul, out testValue);
                                            }
                                        }
                                        tcv.results["auxPowerSupply"] = testValue.ToString("F12");

                                        fl.TestLL = ll.ToString();
                                        fl.TestUL = ul.ToString();
                                        fl.TestResult = testValue.ToString();
                                        
                                        addResultChild("Result = " + testValue.ToString());
                                        

                                        break;
                                    }

                                case Tests.TestID.Accelerometer:
                                    {
                                        #region

                                        int xll = -5, yll = -5, zll = -5;
                                        int xul = 5, yul = 5, zul = 5;
                                        int HRxll = -5, HRyll = -5, HRzll = -5;
                                        int HRxul = 5, HRyul = 5, HRzul = 5;
                                        int testValue;
                                        int testValue1;
                                        int testValue2;

                                        if (t.getParamValue("xll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out xll) == false)
                                            {
                                                xll = -5;
                                            }
                                        }
                                        if (t.getParamValue("xul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out xul) == false)
                                            {
                                                xul = 5;
                                            }
                                        }
                                        if (t.getParamValue("yll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out yll) == false)
                                            {
                                                yll = -5;
                                            }
                                        }
                                        if (t.getParamValue("yul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out yul) == false)
                                            {
                                                yul = 5;
                                            }
                                        } if (t.getParamValue("zll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out zll) == false)
                                            {
                                                zll = -5;
                                            }
                                        }
                                        if (t.getParamValue("zul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out zul) == false)
                                            {
                                                zul = 5;
                                            }
                                        }
                                        if (t.getParamValue("HRxll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRxll) == false)
                                            {
                                                HRxll = -5;
                                            }
                                        }
                                        if (t.getParamValue("HRxul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRxul) == false)
                                            {
                                                HRxul = 5;
                                            }
                                        }
                                        if (t.getParamValue("HRyll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRyll) == false)
                                            {
                                                HRyll = -5;
                                            }
                                        }
                                        if (t.getParamValue("HRyul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRyul) == false)
                                            {
                                                HRyul = 5;
                                            }
                                        } if (t.getParamValue("HRzll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRzll) == false)
                                            {
                                                HRzll = -5;
                                            }
                                        }
                                        if (t.getParamValue("HRzul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out HRzul) == false)
                                            {
                                                HRzul = 5;
                                            }
                                        }
                                        #endregion

                                        addResultChild("HRxll = " + HRxll.ToString());
                                        addResultChild("HRxul = " + HRxul.ToString());
                                        addResultChild("HRyll = " + HRyll.ToString());
                                        addResultChild("HRyul = " + HRyul.ToString());
                                        addResultChild("HRzll = " + HRzll.ToString());
                                        addResultChild("HRzul = " + HRzul.ToString());

                                        testResult = tcv.test.accelerometer(xll, xul, yll, yul, zll, zul, HRxll, HRxul, HRyll, HRyul, HRzll, HRzul, out testValue, out testValue1, out testValue2, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.accelerometer(xll, xul, yll, yul, zll, zul, HRxll, HRxul, HRyll, HRyul, HRzll, HRzul, out testValue, out testValue1, out testValue2, fl);
                                        }
                                        tcv.results["xAxis"] = testValue.ToString();
                                        tcv.results["yAxis"] = testValue1.ToString();
                                        tcv.results["zAxis"] = testValue2.ToString();
                                        addResultChild("xAxis = " + testValue.ToString());
                                        addResultChild("yAxis = " + testValue1.ToString());
                                        addResultChild("zAxis = " + testValue2.ToString());
                                        break;
                                    }
                                                                    

                                case Tests.TestID.GPSNumSats:
                                    {
                                        #region
                                        int ll = 1;
                                        int ul = 255;
                                        int snr = 0;
                                        int numSats = 0;
                                        int timeoutSeconds = 60;
                                        DateTime timeout;

                                        if (t.getParamValue("timeout", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out timeoutSeconds) == false)
                                            {
                                                timeoutSeconds = 60;
                                            }
                                        }
                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 1;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 255;
                                            }
                                        }

                                        //timeoutSeconds = 5;

                                        timeout = DateTime.Now.AddSeconds(timeoutSeconds);

                                        #endregion

                                        addResultChild("ll = " + ll.ToString());
                                        addResultChild("ul = " + ul.ToString());

                                        int gTimeout = 0;
                                        setPgFeedback(0, timeoutSeconds+1, 0);
                                        for (int c = 0; c < 2; c++)
                                        {
                                            for (gTimeout = 0; gTimeout < (timeoutSeconds/2); gTimeout++)
                                            {
                                                testResult = tcv.test.readGpsSatsAndSnr(ll, ul, out numSats, out snr, fl);
                                                if (testResult == true)
                                                {
                                                    break;
                                                }
                                                Application.DoEvents();
                                                for (int cnt = 0; cnt < 10; cnt++)
                                                {
                                                    Thread.Sleep(100);
                                                    Application.DoEvents();
                                                }
                                                if (globals.cancelTest == true) break;

                                                setPgFeedback(0, timeoutSeconds + 1, gTimeout);
                                            }
                                            if (globals.cancelTest == true) break;
                                            if (testResult == true)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                tcv.test._jig.setVbatt(false);
                                                tcv.test._jig.setVbatt(true);
                                            }
                                        }
                                        addResultChild("GPS wait time = " + gTimeout.ToString());

                                        addResultChild("NumSats = " + numSats.ToString());
                                        addResultChild("Snr = " + snr.ToString());

                                        tcv.results["gpsNumSats"] = numSats.ToString();
                                        tcv.results["gpsSnr"] = snr.ToString();

                                        break;
                                    }
                                case Tests.TestID.ReadIctPcbID:
                                    {
                                        #region
                                        string id = "";
                                        #endregion




                                        testResult = tcv.test.readIctPcbIDFromFlash(out id);

                                        if (id == "ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ" || id == "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                                        {
                                            id = "";
                                            testResult = tcv.test.readIctPcbID(out id);
                                        }
                                        tcv.results["ictPcbId"] = id;
                                        addResultChild("ictPcbId = " + id);
                                        if (ignoreIctData == true) testResult = true;

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = id.ToString();
                                        if (id == "")
                                        {
                                            fl.UnitID = "Not Read";
                                        }
                                        else
                                        {
                                            if (id.Contains("ÿ"))
                                            {
                                                fl.UnitID = id.Substring(0, id.IndexOf("ÿ"));
                                            }
                                            else if (id.Contains("F"))
                                            {
                                                fl.UnitID = id.Substring(0, id.IndexOf("F"));
                                            }
                                            else
                                            {
                                                fl.UnitID = id;
                                            }
                                        }
                                        if ((writeDefaultICTData == true && testResult == false) || ((id == "ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ" || id == "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" || id == "") && writeDefaultICTData == true))
                                        {
                                            testResult = true;
                                            tcv.results["ictPcbId"] = "XXXX";
                                            addResultChild("ictPcbId = XXXX");
                                        }
                                        //testResult = false;
                                        break;
                                    }
                                case Tests.TestID.ReadIctPassFail:
                                    {
                                        #region
                                        int ll = 1;
                                        int ul = 1;



                                        if (t.getParamValue("ll", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out ll) == false)
                                            {
                                                ll = 1;
                                            }
                                        }
                                        if (t.getParamValue("ul", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out ul) == false)
                                            {
                                                ul = 1;
                                            }
                                        }
                                        int flag = 0; ;
                                        #endregion
                                        testResult = tcv.test.readIctPassFailFromFlash(ll, ul, out flag);
                                        addResultChild("ictPassFailFromFlash = " + flag.ToString());
                                        if (flag == 255)
                                        {
                                            flag = 0;
                                            testResult = tcv.test.readIctPassFail(ll, ul, out flag);
                                            addResultChild("ictPassFail = " + flag.ToString());
                                            if (writeDefaultICTData == true && testResult == false)
                                            {
                                                testResult = true;
                                                tcv.results["ictPcbPassFail"] = "1";
                                                addResultChild("ictPassFail defaulted to 1");
                                                break;
                                            }                                            
                                        }
                                        tcv.results["ictPcbPassFail"] = flag.ToString();

                                        fl.TestLL = ll.ToString();
                                        fl.TestUL = ul.ToString();
                                        fl.TestResult = flag.ToString();

                                        if (testResult == false)
                                        {
                                            //System.Diagnostics.Debugger.Break();
                                            //tcv.test.fullFlashErase();
                                        }

                                        if ((writeDefaultICTData == true && testResult == false) || (writeDefaultICTData == true && flag == 0))
                                        {
                                            testResult = true;
                                            tcv.results["ictPcbPassFail"] = "1";
                                            addResultChild("ictPassFail defaulted to 1");
                                            break;
                                        }

                                        break;
                                    }
                                case Tests.TestID.ReadIctFailCount:
                                    {
                                        #region
                                        int count = 0; ;
                                        #endregion

                                        testResult = tcv.test.readIctFailCountFromFlash(out count);
                                        addResultChild("ictFailCountFromFlash = " + count.ToString());
                                        if (count == 255)
                                        {
                                            count = 0;
                                            testResult = tcv.test.readIctFailCount(out count);
                                        }
                                        tcv.results["ictPcbFailCount"] = count.ToString();
                                        addResultChild("ictFailCount = " + count.ToString());
                                        if (ignoreIctData == true) testResult = true;

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = count.ToString();

                                        if ((writeDefaultICTData == true && testResult == false) || (writeDefaultICTData == true && count == 255))
                                        {
                                            testResult = true;
                                            tcv.results["ictPcbFailCount"] = "0";
                                            addResultChild("ictFailCount defaulted to 0");
                                        }

                                        break;
                                    }
                                case Tests.TestID.ReadIctTestDate:
                                    {
                                        #region
                                        string testDate = "";
                                        #endregion


                                        testResult = tcv.test.readIctTestDateFromFlash(out testDate);
                                        addResultChild("ictDateFromFlash = " + testDate.ToString());
                                        if (testDate == "ÿÿÿÿÿÿÿÿ" || testDate == "")
                                        {
                                            testDate = "";
                                            testResult = tcv.test.readIctTestDate(out testDate);
                                            addResultChild("ictDate = " + testDate.ToString());
                                        }
                                        tcv.results["ictPcbTestDate"] = testDate;
                                        if (ignoreIctData == true) testResult = true;

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testDate.ToString();

                                        if ((writeDefaultICTData == true && testResult == false) || (writeDefaultICTData == true && (testDate == "ÿÿÿÿÿÿÿÿ" || testDate == "" || testDate == "\0\0\0\0\0\0\0\0")))
                                        {
                                            testResult = true;
                                            tcv.results["ictPcbTestDate"] = "XXXXXXXX";
                                            addResultChild("ictDate defaulted to XXXXXXXX");
                                        }

                                        break;
                                    }
                                case Tests.TestID.ReadAteFailCountFromFlash:
                                    {
                                        #region
                                        int count = 0;
                                        #endregion
                                        testResult = tcv.test.readAteFailCountFromFlash(out count);

                                        tcv.results["ateFailCount"] = count.ToString();
                                        addResultChild("ateFailCount = " + count.ToString());
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = count.ToString();
                                        break;
                                    }

                                case Tests.TestID.WriteIctPcbID:
                                    {
                                        #region

                                        #endregion

                                        addResultChild("Value = " + (string)tcv.results["ictPcbId"]);
                                        testResult = tcv.test.writeIctPcbID((string)tcv.results["ictPcbId"]);

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteIctPassFail:
                                    {
                                        #region

                                        #endregion

                                        addResultChild("Value = " + (string)tcv.results["ictPcbPassFail"]);
                                        testResult = tcv.test.writeIctPassFail(Convert.ToInt32(tcv.results["ictPcbPassFail"]));
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteIctFailCount:
                                    {
                                        #region

                                        #endregion
                                        addResultChild("Value = " + (string)tcv.results["ictPcbFailCount"]);
                                        testResult = tcv.test.writeIctFailCount(Convert.ToInt32(tcv.results["ictPcbFailCount"]));
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteIctTestDate:
                                    {
                                        #region

                                        #endregion
                                        addResultChild("Value = " + (string)tcv.results["ictPcbTestDate"]);
                                        testResult = tcv.test.writeIctTestDate((string)tcv.results["ictPcbTestDate"]);
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteAteFailCount:
                                    {
                                        #region

                                        #endregion
                                        addResultChild("Value = " + (string)tcv.results["ateFailCount"]);
                                        testResult = tcv.test.writeAteFailCount(Convert.ToInt32(tcv.results["ateFailCount"]));
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteAtePassFail:
                                    {
                                        #region

                                        #endregion
                                        addResultChild("Value = 1");
                                        tcv.results["atePassFail"] = "1";
                                        testResult = tcv.test.writeAtePassFail(Convert.ToInt32(tcv.results["atePassFail"]));
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteAteTestDate:
                                    {
                                        #region

                                        #endregion
                                        tcv.results["ateTestDate"] = DateTime.UtcNow.Date.Day.ToString("d2") + DateTime.UtcNow.Date.Month.ToString("d2") + DateTime.UtcNow.Date.Year.ToString("d4");
                                        addResultChild("Value = " + (string)tcv.results["ateTestDate"]);
                                        testResult = tcv.test.writeAteTestDate((string)tcv.results["ateTestDate"]);
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.ScanSimNumber:
                                    {
                                        bool manualSimNumber = false;
                                        int simlen = tcv.tc.rc.simNumberLength;
                                        //Monitor.Enter(scannerLock);
                                        m_SimNumber = "";
                                        lock (jcLock)
                                        {
                                            string gsmStatus = "";
                                            int gsmStatusValue = 0xff;
                                            int lCnt;
                                            displayMessage("Waiting for GSM Connection");
                                            setPgFeedback(0, 361, 0);
                                            for ( lCnt = 0; lCnt < 180; lCnt++)
                                            {
                                                if (tcv.test._sc.isOnNetwork() == 1)
                                                {
                                                    testResult = true;
                                                    displayMessage("GSM Connected");
                                                    fl.TestLL = "";
                                                    break;
                                                }
                                                else
                                                {
                                                    testResult = false;
                                                    fl.TestLL = "RTU not on network";
                                                }
                                                Application.DoEvents();
                                                Thread.Sleep(1000);
                                                Application.DoEvents();
                                                setPgFeedback(0, 361, lCnt);
                                                if (globals.cancelTest == true)
                                                {
                                                    break;
                                                }
                                                gsmStatusValue = tcv.test._sc.getGsmStatus(out gsmStatus);

                                                if (gsmStatusValue > 0 && gsmStatusValue < 0xFF)
                                                {                                                    
                                                    testResult = false;
                                                    break;
                                                }
                                                
                                            }
                                            setPgFeedback(0, 361, 0);
                                            //displayMessage("");
                                            if (fl.TestLL != "")
                                            {
                                                addResultChild("GSM Not On Network");
                                            }
                                            if (testResult == true)
                                            {                                                
                                                int tempRes = 0;
                                                for (int cnt = 0; cnt < 9; cnt++)
                                                {
                                                    m_SimNumber = "";
                                                    tempRes = tcv.test._sc.readSimNumber(ref m_SimNumber);
                                                    if (tempRes > 0)
                                                    {
                                                        if (m_SimNumber.Length == simlen)
                                                        {
                                                            displayMessage(m_SimNumber);
                                                            manualSimNumber = false;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            displayMessage("Trying to read ICCID from SIM");
                                                            addResultChild("SimNumber length wrong expected " + simlen.ToString() + " received " + m_SimNumber.ToString());
                                                        }
                                                    }
                                                    else if (tempRes == -3)
                                                    {
                                                        //break;
                                                    }
                                                    Thread.Sleep(3000*(cnt+1));
                                                }

                                                if (manualSimNumber == true)
                                                {

                                                    displayMessage("Please scan sim number for UUT");

                                                    scanningSimNumber = true;
                                                    this.KeyPreview = true;
                                                    m_SimNumber = "";

                                                    DateTime timeout = DateTime.Now.AddSeconds(60);
                                                    testResult = false;


                                                    while (true)
                                                    {
                                                        if (globals.cancelTest == true) break;

                                                        if (scanningSimNumber == false)
                                                        {
                                                            if (m_SimNumber.Length == simlen)
                                                            {
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                m_SimNumber = "";
                                                                scanningSimNumber = true;
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }
                                                }

                                                if (m_SimNumber.Length == simlen)
                                                {
                                                    testResult = tcv.test.scanSimNumber(m_SimNumber);
                                                }
                                                else
                                                {
                                                    testResult = false;
                                                }

                                                tcv.results["simNumber"] = m_SimNumber;
                                            }
                                            else
                                            {
                                                addResultChild("GSM Status = " + gsmStatusValue.ToString());
                                                addResultChild("GSM Status = " + gsmStatus);
                                                fl.TestUL = gsmStatus;
                                            }

                                            displayMessage("");

                                        }

                                        //fl.TestLL = "";
                                        //fl.TestUL = "";
                                        fl.TestResult = (string)tcv.results["simNumber"];
                                        addResultChild("simNumber = " + m_SimNumber);

                                        break;
                                    }
                                case Tests.TestID.StoreParameters:
                                    {
                                        testResult = tcv.test.storeParameters();
                                                                                
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.VerifyIMEI:
                                    {
                                        //testResult = tcv.test.fullFlashErase();
                                        //testResult = tcv.test.writeIMEINumber((string)tcv.results["imei"]);
                                        //testResult = tcv.test.storeParameters();
                                        testResult = tcv.test.verifyIMEINumber((string)tcv.results["imei"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIMEINumber((string)tcv.results["imei"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["imei"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyIMSI:
                                    {
                                        testResult = tcv.test.verifyIMSINumber((string)tcv.results["imsi"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIMSINumber((string)tcv.results["imsi"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["imsi"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifySimNumber:
                                    {
                                        testResult = tcv.test.verifySIMNumber((string)tcv.results["simNumber"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifySIMNumber((string)tcv.results["simNumber"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["simNumber"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyPhonenumber:
                                    {
                                        testResult = tcv.test.verifyTelephoneNumber((string)tcv.results["phoneNumber"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyTelephoneNumber((string)tcv.results["phoneNumber"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["phoneNumber"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyHWVariant:
                                    {
                                        testResult = tcv.test.verifyHwVariant(tcv.tc.rc.hardwareVersion, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyHwVariant(tcv.tc.rc.hardwareVersion, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.hardwareVersion);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyPartNumber:
                                    {
                                        testResult = tcv.test.verifyPartNumber(tcv.tc.rc.partNumber, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyPartNumber(tcv.tc.rc.partNumber, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.partNumber );
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyIctPcbID:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyIctPcbID((string)tcv.results["ictPcbId"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIctPcbID((string)tcv.results["ictPcbId"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ictPcbId"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyIctPassFail:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyIctPassFail(Convert.ToInt32(tcv.results["ictPcbPassFail"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIctPassFail(Convert.ToInt32(tcv.results["ictPcbPassFail"]), fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ictPcbPassFail"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyIctFailCount:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyIctFailCount(Convert.ToInt32(tcv.results["ictPcbFailCount"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIctFailCount(Convert.ToInt32(tcv.results["ictPcbFailCount"]), fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ictPcbFailCount"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyIctTestDate:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyIctTestDate((string)tcv.results["ictPcbTestDate"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIctTestDate((string)tcv.results["ictPcbTestDate"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ictPcbTestDate"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyAteFailCount:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyAteFailCount(Convert.ToInt32(tcv.results["ateFailCount"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyAteFailCount(Convert.ToInt32(tcv.results["ateFailCount"]), fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ateFailCount"]);
                                        addResultChild("Result " + fl.TestResult);

                                        break;
                                    }
                                case Tests.TestID.VerifyAtePassFail:
                                    {
                                        #region

                                        #endregion

                                        testResult = tcv.test.verifyAtePassFail(Convert.ToInt32(tcv.results["atePassFail"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyAtePassFail(Convert.ToInt32(tcv.results["atePassFail"]), fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["atePassFail"]);
                                        addResultChild("Result " + fl.TestResult);

                                        break;
                                    }
                                case Tests.TestID.VerifyAteTestDate:
                                    {
                                        #region

                                        #endregion

                                        testResult = tcv.test.verifyAteTestDate((string)tcv.results["ateTestDate"], fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyAteTestDate((string)tcv.results["ateTestDate"], fl);
                                        }
                                        addResultChild("Verifying " + (string)tcv.results["ateTestDate"]);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyFirmwareID:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyFirmwareID(tcv.tc.rc.firmwareId, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyFirmwareID(tcv.tc.rc.firmwareId, fl);
                                        }
                                        addResultChild("Verifying " +tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyAdLow:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyADLow(Convert.ToInt32(tcv.results["adBatteryLow"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyADLow(Convert.ToInt32(tcv.results["adBatteryLow"]), fl);
                                        }
                                        addResultChild("Verifying " + Convert.ToInt32(tcv.results["adBatteryLow"]).ToString());
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyAdMid:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyADMid(Convert.ToInt32(tcv.results["adBatteryMid"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyADMid(Convert.ToInt32(tcv.results["adBatteryMid"]), fl);
                                        }
                                        addResultChild("Verifying " + Convert.ToInt32(tcv.results["adBatteryMid"]).ToString());
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyAdHigh:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyADHigh(Convert.ToInt32(tcv.results["adBatteryHigh"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyADHigh(Convert.ToInt32(tcv.results["adBatteryHigh"]), fl);
                                        }
                                        addResultChild("Verifying " + Convert.ToInt32(tcv.results["adBatteryHigh"]).ToString());
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifySmsOnly:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifySmsOnly(tcv.tc.rc.smsOnly, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifySmsOnly(tcv.tc.rc.smsOnly, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.smsOnly);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifySmsServerPhoneNumber:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifySmsServerPhoneNumber(tcv.tc.rc.smsServerPhoneNumber, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifySmsServerPhoneNumber(tcv.tc.rc.smsServerPhoneNumber, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.smsServerPhoneNumber);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyGprsPort:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsPortNumber(tcv.tc.rc.gprsPortNumber, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsPortNumber(tcv.tc.rc.gprsPortNumber, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.gprsPortNumber);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyGprsIP:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsServerIP(tcv.tc.rc.gprsServerIP, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsServerIP(tcv.tc.rc.gprsServerIP, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.gprsServerIP);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyGprsAPN:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsAPN(tcv.tc.rc.gprsApn, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsAPN(tcv.tc.rc.gprsApn, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.gprsApn);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyGprsUserName:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsUserID(tcv.tc.rc.gprsNetworkUserID, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsUserID(tcv.tc.rc.gprsNetworkUserID, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.gprsNetworkUserID);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyGprsPassword:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsPassword(tcv.tc.rc.gprsNetworkPassword, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsPassword(tcv.tc.rc.gprsNetworkPassword, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.gprsNetworkPassword);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyBootLoaderVersion:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyBootloaderVersion(tcv.tc.rc.bootloaderVersion, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyBootloaderVersion(tcv.tc.rc.bootloaderVersion, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.bootloaderVersion);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.VerifyFirmwareFileName:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyFirmwareFileName(tcv.tc.rc.firmwareFileName, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyFirmwareFileName(tcv.tc.rc.firmwareFileName, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareFileName);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyServerTelNumberSecondary:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyServerTelNumberSecondary(tcv.tc.rc.ServerTelNumberSecondary, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyServerTelNumberSecondary(tcv.tc.rc.ServerTelNumberSecondary, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyGprsPrimaryServer:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsPrimaryServer(tcv.tc.rc.GprsPrimaryServer, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsPrimaryServer(tcv.tc.rc.GprsPrimaryServer, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyGprsSecondaryServer:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsSecondaryServer(tcv.tc.rc.GprsSecondaryServer, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsSecondaryServer(tcv.tc.rc.GprsSecondaryServer, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                case Tests.TestID.VerifyGprsEngineeringServer:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsEngineeringServer(tcv.tc.rc.GprsEngineeringServer, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsEngineeringServer(tcv.tc.rc.GprsEngineeringServer, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }


                                case Tests.TestID.VerifyGprsAUXServer:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyGprsAUXServer(tcv.tc.rc.GprsAUXServer, fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyGprsAUXServer(tcv.tc.rc.GprsAUXServer, fl);
                                        }
                                        addResultChild("Verifying " + tcv.tc.rc.firmwareId);
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }





                                case Tests.TestID.WriteIMEI:
                                    {
                                        #region

                                        #endregion
                                        //testResult = tcv.test.writeIMEINumber("123456789054321");
                                        testResult = tcv.test.writeIMEINumber((string)tcv.results["imei"]);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeIMEINumber((string)tcv.results["imei"]);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteIMSI:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeIMSINumber((string)tcv.results["imsi"]);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeIMSINumber((string)tcv.results["imsi"]);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteSimNumber:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeSIMNumber((string)tcv.results["simNumber"]);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeSIMNumber((string)tcv.results["simNumber"]);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WritePhoneNumber:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeTelephoneNumber((string)tcv.results["phoneNumber"]);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeTelephoneNumber((string)tcv.results["phoneNumber"]);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteHwNumber:
                                    {

                                        testResult = tcv.test.writeHwVariant(tcv.tc.rc.hardwareVersion);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeHwVariant(tcv.tc.rc.hardwareVersion);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WritePartNumber:
                                    {

                                        if (t.getParamValue("partNumber", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.partNumber = paramValue;
                                        }

                                        switch (pcbVar)
                                        {
                                            case 0x20:
                                                tcv.tc.rc.partNumber += "/2G/USB";
                                                break;
                                            case 0x21:
                                                tcv.tc.rc.partNumber += "/2G";
                                                break;
                                            case 0x22:
                                                tcv.tc.rc.partNumber += "/LTE-M/USB";
                                                break;
                                            case 0x23:
                                                tcv.tc.rc.partNumber += "/2G/USB/SD";
                                                break;
                                            case 0x24:
                                                tcv.tc.rc.partNumber += "/LTE-M/USB/SD";
                                                break;
                                        }
                                        
                                        testResult = tcv.test.writePartNumber(tcv.tc.rc.partNumber);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writePartNumber(tcv.tc.rc.partNumber);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteFirmwareID:
                                    {
                                        #region
                                        if (t.getParamValue("firmwareId", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.firmwareId = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeFirmwareID(tcv.tc.rc.firmwareId);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeFirmwareID(tcv.tc.rc.firmwareId);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }

                                case Tests.TestID.WriteServerTelNumberSecondary:
                                    {
                                        #region
                                        if (t.getParamValue("ServerTelNumberSecondary", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.ServerTelNumberSecondary = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeServerTelNumberSecondary(tcv.tc.rc.ServerTelNumberSecondary);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeServerTelNumberSecondary(tcv.tc.rc.ServerTelNumberSecondary);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteGprsPrimaryServer:
                                    {
                                        #region
                                        if (t.getParamValue("GprsPrimaryServer", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.GprsPrimaryServer = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsPrimaryServer(tcv.tc.rc.GprsPrimaryServer);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsPrimaryServer(tcv.tc.rc.GprsPrimaryServer);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteGprsSecondaryServer:
                                    {
                                        #region
                                        if (t.getParamValue("GprsSecondaryServer", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.GprsSecondaryServer = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsSecondaryServer(tcv.tc.rc.GprsSecondaryServer);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsSecondaryServer(tcv.tc.rc.GprsSecondaryServer);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteGprsEngineeringServer:
                                    {
                                        #region
                                        if (t.getParamValue("GprsEngineeringServer", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.GprsEngineeringServer = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsEngineeringServer(tcv.tc.rc.GprsEngineeringServer);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsEngineeringServer(tcv.tc.rc.GprsEngineeringServer);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteGprsAUXServer:
                                    {
                                        #region
                                        if (t.getParamValue("GprsAUXServer", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.GprsAUXServer = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsAUXServer(tcv.tc.rc.GprsAUXServer);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsAUXServer(tcv.tc.rc.GprsAUXServer);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                case Tests.TestID.WriteAdLow:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeAdLow(Convert.ToInt32(tcv.results["adBatteryLow"]));
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeAdLow(Convert.ToInt32(tcv.results["adBatteryLow"]));
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteAdMid:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeAdMid(Convert.ToInt32(tcv.results["adBatteryMid"]));
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeAdMid(Convert.ToInt32(tcv.results["adBatteryMid"]));
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteAdHigh:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeAdHigh(Convert.ToInt32(tcv.results["adBatteryHigh"]));
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeAdHigh(Convert.ToInt32(tcv.results["adBatteryHigh"]));
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteSmsOnly:
                                    {
                                        #region
                                        if (t.getParamValue("smsOnlyFlag", ref paramValue) == true)
                                        {
                                            if (bool.TryParse(paramValue, out tcv.tc.rc.smsOnly) == false)
                                            {
                                                tcv.tc.rc.smsOnly = false;
                                            }
                                        }
                                        #endregion
                                        testResult = tcv.test.writeSmsOnly(tcv.tc.rc.smsOnly);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeSmsOnly(tcv.tc.rc.smsOnly);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.WriteSmsServerPhoneNumber:
                                    {
                                        #region
                                        if (t.getParamValue("smsServerPhoneNumber", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.smsServerPhoneNumber = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeSmsServerPhoneNumber(tcv.tc.rc.smsServerPhoneNumber);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeSmsServerPhoneNumber(tcv.tc.rc.smsServerPhoneNumber);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteGprsPort:
                                    {
                                        #region
                                        if (t.getParamValue("gprsPortNumber", ref paramValue) == true)
                                        {
                                            if (int.TryParse(paramValue, out tcv.tc.rc.gprsPortNumber) == false)
                                            {
                                                tcv.tc.rc.gprsPortNumber = 5556;
                                            }
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsPortNumber(tcv.tc.rc.gprsPortNumber);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsPortNumber(tcv.tc.rc.gprsPortNumber);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteGprsIP:
                                    {
                                        #region
                                        if (t.getParamValue("gprsServerIP", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.gprsServerIP = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeGprsServerIP(tcv.tc.rc.gprsServerIP);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsServerIP(tcv.tc.rc.gprsServerIP);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteGprsAPN:
                                    {
                                        #region

                                        #endregion

                                        testResult = tcv.test.writeGprsAPN(tcv.tc.rc.gprsApn);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsAPN(tcv.tc.rc.gprsApn);
                                        }

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteGprsUserName:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeGprsUserID(tcv.tc.rc.gprsNetworkUserID);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsUserID(tcv.tc.rc.gprsNetworkUserID);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteGprsPassword:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.writeGprsPassword(tcv.tc.rc.gprsNetworkPassword);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeGprsPassword(tcv.tc.rc.gprsNetworkPassword);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteBootLoaderVersion:
                                    {
                                        #region
                                        if (t.getParamValue("bootloaderVersion", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.bootloaderVersion = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeBootloaderVersion(tcv.tc.rc.bootloaderVersion);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeBootloaderVersion(tcv.tc.rc.bootloaderVersion);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }
                                case Tests.TestID.WriteFirmwareFileName:
                                    {
                                        #region
                                        if (t.getParamValue("firmwareFileName", ref paramValue) == true)
                                        {
                                            tcv.tc.rc.firmwareFileName = paramValue;
                                        }
                                        #endregion
                                        testResult = tcv.test.writeFirmwareFileName(tcv.tc.rc.firmwareFileName);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeFirmwareFileName(tcv.tc.rc.firmwareFileName);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";
                                        break;
                                    }

                                

                                case Tests.TestID.LoadAppFirmwareJlink:
                                    {
                                        #region

                                        string firmwarePath = "";

                                        if (t.getParamValue("appFirmwarePath", ref paramValue) == true)
                                        {
                                            firmwarePath = filePath + paramValue;
                                        }

                                        #endregion

                                        int iCnt;
                                        int tResult = -1;
                                        addResultChild(firmwarePath);
                                        fl.TestLL = "Check Alignment";
                                        fl.TestUL = "";
                                        lock (jcLock)
                                        {

                                            tcv.test._jig.jl.progressStatus += jl_progressStatus;
                                            for (iCnt = 0; iCnt < 2; iCnt++)
                                            {
                                                tcv.test._jig.setPowerSupply(13.2, globals.defaultCurrent );
                                                Thread.Sleep(1000);
                                                tcv.test._jig.setPowerSupplyState(true);
                                                Thread.Sleep(1000);
                                                tcv.test._jig.setVbatt(true);
                                                Thread.Sleep(1500);

                                                tcv.test._jig.jl.connect(Properties.Settings.Default.Processor);
                                                
                                                tResult=tcv.test._jig.jl.program(firmwarePath,true);
                                                if (tResult < 0)
                                                {
                                                    testResult = false;
                                                    fl.TestUL = "Err Code " + tResult.ToString();
                                                }
                                                else
                                                {
                                                    testResult = true;
                                                }
                                                tcv.results["appCodePath"] = firmwarePath;
                                                if (testResult == true)
                                                {
                                                    if (tcv.test._jig.jl._SecureChip() != 0)
                                                    {
                                                        testResult = false;
                                                        fl.TestLL = "Secure Chip Fail";
                                                    }
                                                    break;
                                                }
                                                tcv.test._jig.jl.close();
                                                if (testResult == true) break;

                                                Application.DoEvents();
                                                Thread.Sleep(100);
                                                tcv.test._jig.setVbatt(false);
                                            }
                                            tcv.test._jig.jl.progressStatus -= new reportProgress(jl_progressStatus);


                                            


                                        }

                                        tcv.results["appCodePathLoopCount"] = iCnt.ToString();
                                        addResultChild("AttemptCnt = " + iCnt.ToString());
                                        fl.TestResult = "Retry cnt = " + iCnt;
                                        break;
                                    }
                                case Tests.TestID.PrintLabel:
                                    {
                                        Label l = new Label();
                                        LabelPrintReturnCodes lprc;
                                                                                                                   
                                        lock (jcLock)
                                        {                                            
                                            if (tcv.tc.rc.simIdType == 33)
                                            {
                                                lprc = l.Print((string)tcv.results["imei"], DateTime.UtcNow.Date.ToShortDateString(), tcv.tc.rc.hardwareVersion, tcv.tc.rc.firmwareFileName, tcv.tc.rc.partNumber, "0031" + (string)tcv.results["phoneNumber"].Substring(1), tcv.tc.labelTemplate, filePath);
                                            }
                                            else
                                            {
                                                lprc = l.Print((string)tcv.results["imei"], DateTime.UtcNow.Date.ToShortDateString(), tcv.tc.rc.hardwareVersion, tcv.tc.rc.firmwareFileName, tcv.tc.rc.partNumber, (string)tcv.results["phoneNumber"], tcv.tc.labelTemplate, filePath);
                                            }
                                            if (lprc == LabelPrintReturnCodes.NO_ERROR)
                                            {
                                                

                                                
                                                displayMessage("Please scan label for UUT");
                                                

                                                scanningLabel = true;
                                                this.KeyPreview = true;
                                                m_LabelSerialNum = "";

                                                

                                                DateTime timeout = DateTime.Now.AddSeconds(60);
                                                testResult = false;
                                                
                                                clearScanBox();
                                                ScanBoxVisible(true);

                                                //while (DateTime.Now.CompareTo(timeout) < 0)
                                                while (true)
                                                {
                                                    if (globals.cancelTest == true) break;

                                                    if (scanningLabel == false) break;

                                                    Application.DoEvents();
                                                }
                                                this.KeyPreview = false;
                                                scanningLabel = false;
                                                ScanBoxVisible(false);

                                                if (globals.cancelTest == false)
                                                {
                                                    if (m_LabelSerialNum == (string)tcv.results["imei"])
                                                    {
                                                        testResult = true;
                                                    }
                                                    else
                                                    {
                                                        testResult = false;
                                                    }
                                                }
                                                else
                                                {
                                                    testResult = false;
                                                }

                                                addResultChild("Scanned imei = " + m_LabelSerialNum);

                                            }
                                            else
                                            {
                                                testResult = false;
                                            }
                                            //MessageBox.Show("Attach Label to unit in JIG", "", MessageBoxButtons.OK);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = testResult.ToString();
                                        break;
                                    }
                                
                                case Tests.TestID.ReadPcbVariant:
                                    {
                                        #region
                                        int var = 0; 
                                        int tempVar = 0;
                                        string validList = "";
                                        List<int> validPcbVar = new List<int>();


                                        foreach (TestParameter ltp in t.testParams)
                                        {
                                            if (ltp.description.StartsWith("pcb_Valid") == true)
                                            {
                                                if(int.TryParse(ltp.value,out tempVar)==true)
                                                { 
                                                    validPcbVar.Add(tempVar);
                                                    addResultChild("Valid PCB Var = " + tempVar.ToString());
                                                    validList += tempVar.ToString() + ",";
                                                }
                                            }
                                        }
                                        #endregion

                                        testResult = tcv.test.readPcbVariant(out var);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.readPcbVariant(out var);
                                        }

                                        //if (var == 0)
                                        //{
                                        //    tcv.test._sc.fullFlashErase();
                                        //    string id = "";
                                        //    testResult = tcv.test.readIctPcbID(out id);
                                        //    testResult = tcv.test.readIctPassFail(1, 1, out var);
                                        //    testResult = tcv.test.readPcbVariant(out var);
                                        //    var = 0x10;
                                        //}
                                        
                                        if(globals.forcePcbVariant == true)
                                        {
                                            var = globals.forcedPcbVariant;
                                        }

                                        tcv.results["pcbVariant1"] = var.ToString();
                                        pcbVar = var;

                                        testResult = false;
                                        foreach(int i in validPcbVar)
                                        {
                                            if ((pcbVar == i) || (globals.forcePcbVariant == true))
                                            {
                                                testResult = true;
                                                break;
                                            }
                                        }
                                        
                                        


                                        addResultChild("Result = " + var.ToString());
                                        fl.TestLL = "";
                                        fl.TestUL = validList;
                                        fl.TestResult = var.ToString();

                                        break;
                                    }
                                case Tests.TestID.WritePcbVariant:
                                    {
                                        #region

                                        #endregion

                                        testResult = tcv.test.writePcbVariant(Convert.ToInt32(tcv.results["pcbVariant1"]), true);


                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.VerifyPcbVariant:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyPcbVariant(Convert.ToInt32(tcv.results["pcbVariant1"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyPcbVariant(Convert.ToInt32(tcv.results["pcbVariant1"]), fl);
                                        }
                                        addResultChild("Verifying " + (Convert.ToInt32(tcv.results["pcbVariant1"]).ToString()));
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.WriteVehicleVariant:
                                    {
                                        #region

                                        #endregion


                                        testResult = tcv.test.writeVehicleVariant(Convert.ToInt32(tcv.results["vehicleVariant"]));
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeVehicleVariant(Convert.ToInt32(tcv.results["vehicleVariant"]));
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";

                                        break;
                                    }
                                case Tests.TestID.VerifyVehicleVariant:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyVehicleVariant(Convert.ToInt32(tcv.results["vehicleVariant"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyVehicleVariant(Convert.ToInt32(tcv.results["vehicleVariant"]), fl);
                                        }
                                        addResultChild("Verifying " + (Convert.ToInt32(tcv.results["vehicleVariant"]).ToString()));
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }

                                
                                case Tests.TestID.WriteVehicleFlags:
                                    {
                                        #region

                                        #endregion


                                        int opt = Convert.ToInt32(tcv.results["vehicleFlags"]);

                                        testResult = tcv.test.writeVehicleFlags(opt);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeVehicleFlags(opt);
                                        }

                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";



                                        break;
                                    }
                                case Tests.TestID.VerifyVehicleFlags:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyVehicleFlags(Convert.ToInt32(tcv.results["vehicleFlags"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyVehicleFlags(Convert.ToInt32(tcv.results["vehicleFlags"]), fl);
                                        }
                                        addResultChild("Verifying " + (Convert.ToInt32(tcv.results["vehicleFlags"]).ToString()));
                                        addResultChild("Result " + fl.TestResult);
                                        break;
                                    }
                                case Tests.TestID.CrankTest:
                                    {
                                        testResult = tcv.test.crankDetectTest(fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.crankDetectTest(fl);
                                        }
                                        break;
                                    }
                                case Tests.TestID.Can2RtuTest:
                                    {
                                        testResult = tcv.test.can2RtuTest(fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.can2RtuTest(fl);
                                        }
                                        break;
                                    }
                                case Tests.TestID.Can2PassThruRtuTest:
                                    {
                                        testResult = tcv.test.Can2PassThruRtuTest();
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.Can2PassThruRtuTest();
                                        }
                                        break;
                                    }
                                case Tests.TestID.EraseEEprom:
                                    {
                                        testResult = tcv.test.fullEEpromErase();
                                        if(testResult==false)
                                        {                                            
                                            testResult = tcv.test.fullEEpromErase();
                                        }
                                        break;
                                    }
                                case Tests.TestID.SDcardCheck:
                                    {
                                        try
                                        {
                                            pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                                        }
                                        catch (Exception err)
                                        {
                                            pcbVar = 255;
                                        }
                                        int sdReadvalue = 0;
                                        addResultChild("pcbVar = " + pcbVar.ToString());
                                        switch (pcbVar)
                                        {
                                            case 0x20:
                                                {
                                                    sdReadvalue = tcv.test.sdCardCheck();
                                                    if (sdReadvalue == 0x7f)
                                                    {
                                                        testResult = true;
                                                    }
                                                    else
                                                    {
                                                        testResult = false;
                                                        fl.TestUL = "SD Card Present";
                                                    }
                                                    break;
                                                }
                                            case 0x21:
                                                {
                                                    sdReadvalue = tcv.test.sdCardCheck();
                                                    if (sdReadvalue == 0x7f)
                                                    {
                                                        testResult = true;
                                                    }
                                                    else
                                                    {
                                                        testResult = false;
                                                        fl.TestUL = "SD Card Present";
                                                    }
                                                    break;
                                                }
                                            case 0x22:
                                                {
                                                    sdReadvalue = tcv.test.sdCardCheck();
                                                    if (sdReadvalue == 0x7f)
                                                    {
                                                        testResult = true;
                                                    }
                                                    else
                                                    {
                                                        testResult = false;
                                                        fl.TestUL = "SD Card Present";
                                                    }
                                                    break;
                                                }
                                            case 0x23:
                                            case 0x24:
                                                {
                                                    sdReadvalue = tcv.test.sdCardCheck();
                                                    switch (sdReadvalue)
                                                    {
                                                        case 1://WEU maps
                                                            {
                                                                testResult = true;
                                                                vl.SoftwareVersion += " WEU SD";
                                                                break;
                                                            }
                                                        case 2://GBR maps
                                                            {
                                                                testResult = true;
                                                                vl.SoftwareVersion += " GBR SD";
                                                                break;
                                                            }
                                                        case 3://NAM maps
                                                            {
                                                                testResult = true;
                                                                vl.SoftwareVersion += " NAM SD";
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                testResult = false;
                                                                break;
                                                            }
                                                    }
                                                    
                                                    
                                                    break;
                                                }
                                            
                                            default:
                                                {
                                                    testResult = false;
                                                    break;
                                                }
                                        }
                                        addResultChild("sdCardRes = " + sdReadvalue.ToString());
                                        break;
                                    }
                                case Tests.TestID.WriteIOConfig:
                                    {
                                        tcv.results["ioConfig"] = "0";

                                        int io = Convert.ToInt32(tcv.results["ioConfig"]);

                                        
                                        testResult = tcv.test.writeIOConfig(io);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.writeIOConfig(io);
                                        }
                                        fl.TestLL = "";
                                        fl.TestUL = "";
                                        fl.TestResult = "";



                                        break;
                                    }
                                case Tests.TestID.VerifyIOConfig:
                                    {
                                        #region

                                        #endregion
                                        testResult = tcv.test.verifyIOConfig(Convert.ToInt32(tcv.results["ioConfig"]), fl);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyIOConfig(Convert.ToInt32(tcv.results["ioConfig"]), fl);
                                        }

                                        break;
                                    }

                                case Tests.TestID.LoadWhiteList:
                                    {
                                        testResult = tcv.test.loadWhiteList("Driver_ID.bin", setPgFeedback);
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.loadWhiteList("Driver_ID.bin", setPgFeedback);
                                        }
                                        break;
                                    }
                                case Tests.TestID.VerifyWhiteList:
                                    {
                                        testResult = tcv.test.verifyWhiteList();
                                        if (testResult == false)
                                        {
                                            testResult = tcv.test.verifyWhiteList();
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        testResult = false;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            testResult = true;
                        }

                        if (globals.cancelTest == true)
                        {
                            testResult = false;
                            break;
                        }

                        if (testResult == true)
                        {
                            //Continue to next test item

                            //Application.DoEvents();
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                catch (Exception localErr)
                {
                    testResult = false;
                }
            }


            lock (jcLock)
            {
                if ((testResult == false) || (globals.cancelTest == true))
                {
                    int tempAteFail = Convert.ToInt32(tcv.results["ateFailCount"]);
                    tcv.results["ateFailCount"] = (++tempAteFail).ToString();
                    if (lastTest == Tests.TestID.CurrentDrawA.ToString() || lastTest == Tests.TestID.LoadTestFirmwareJlink.ToString() || lastTest == Tests.TestID.LatchRemoved.ToString() || lastTest == Tests.TestID.ReadProdFirmwareVersion.ToString() || lastTest == Tests.TestID.PrintLabel.ToString() || lastTest == Tests.TestID.LoadAppFirmwareJlink.ToString() || lastTest == Tests.TestID.ReadIctTestDate.ToString() || lastTest == Tests.TestID.ReadIctFailCount.ToString() || lastTest == Tests.TestID.ReadIctPassFail.ToString() || lastTest == Tests.TestID.ReadIctPcbID.ToString() || lastTest == Tests.TestID.ReadPcbVariant.ToString() || lastTest == Tests.TestID.readGyroID.ToString() || lastTest == Tests.TestID.GPSVoltage.ToString() || lastTest == Tests.TestID.BatteryADReadings.ToString() || lastTest == "ScanBoxID")
                    {
                    }
                    else
                    {
                        try
                        {
                            pcbVar = Convert.ToInt32(tcv.results["pcbVariant1"]);
                        }
                        catch (Exception err)
                        {
                            pcbVar = 255;
                        }

                        tcv.test.writeFailureData(Convert.ToInt32(tcv.results["ateFailCount"]), (string)tcv.results["ictPcbId"], Convert.ToInt32(tcv.results["ictPcbFailCount"]), Convert.ToInt32(tcv.results["ictPcbPassFail"]), (string)tcv.results["ictPcbTestDate"], pcbVar, testSoftVer);
                    }
                    if (MessageBox.Show("Do you want an error label? " + fl.JigIdent + " : " + fl.TestID, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (globals.cancelTest == true)
                        {
                            fl.TestID = fl.TestID + " - CANCELLED";
                        }
                        fl.Print();
                    }
                }
                

                tcv.results["simTypeCode"] = tcv.tc.rc.simIdType.ToString();

                if (testResult == true)
                {
                    tcv.results["pass"] = "1";
                    pass++;
                    try
                    {
                        AmlConnector.insertProductionData(tcv.results["imei"], tcv.results["imsi"], tcv.results["timeStamp"], tcv.tc.rc.partNumber, tcv.results["phoneNumber"], tcv.results["simNumber"], tcv.results["adBatteryLow"], tcv.results["vehicleVariant"], tcv.tc.rc.simIdType.ToString(), boxid.ToString());
                    }
                    catch (Exception err)
                    {
                        System.Diagnostics.Debug.WriteLine(err.Message);
                    }
                    
                    vl.Print(false);
                    jc.resetJig(false);
                }
                else
                {
                    tcv.results["pass"] = "0";
                    fail++;
                    jc.resetJig(false);
                }

                tcv.results["testTime"] = ((int)DateTime.Now.Subtract(startTimeJig).TotalSeconds).ToString();

                tcv.results["testLog"] = createTestLog(this.tvTests);
                System.IO.File.WriteAllText(System.Windows.Forms.Application.StartupPath + @"\testLogs\" + tcv.results["boxID"] + "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmss") + ".txt", tcv.results["testLog"]);
                
                
                AmlConnector.insertProductionTestData(tcv.results);
            }

            e.Result = testResult;
        }

        void jl_progressStatus(object sender, jLinkProgressEventArgs e)
        {
            setPgFeedback(0,101, e.Percentage);
            displayMessage(e.Msg);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            
            cBSim.Enabled = false;
            cbVariant.Enabled = false;
            btnSelectCustomer.Enabled = false;
            btnSelectScript.Enabled = false;

            jc.resetJig(false);

            if (jc.isJigShut())
            {
                jc.lockJig(true);

                displayMessage("Processing Database Updates Please Wait");
                AmlConnector.processFailedInsertProductionTestData(Application.StartupPath + @"\failedInsertProductionTestResuls", this.pgFeedback );
                AmlConnector.processFailedInsertProductionData(Application.StartupPath + @"\failedInsertProductionData", this.pgFeedback);
                displayMessage("");

                testPathJig.labelTemplate = (string)dtCustomers[this.cmbSelectCust.SelectedIndex].labelFile;

                testPathJig.rc = new ReleaseConfig();

                for (int i = 0; i < dtSimList.Count; i++)
                {
                    if (cBSim.SelectedIndex == -1) break;

                    if ((string)this.cBSim.Items[cBSim.SelectedIndex] == (string)(dtSimList[i].description))
                    {
                        testPathJig.rc.gprsApn = (string)(dtSimList[i].apn);

                        testPathJig.rc.gprsNetworkUserID = (string)(dtSimList[i].user);

                        testPathJig.rc.gprsNetworkPassword = (string)(dtSimList[i].pass);

                        testPathJig.rc.phoneNumberLength = (int)(Int64)(dtSimList[i].phoneNumLen);

                        testPathJig.rc.manualPhoneNum = (bool)(dtSimList[i].manualPhoneNum);

                        testPathJig.rc.simNumberLength = (int)(Int64)(dtSimList[i].simNumLen);

                        testPathJig.rc.sendTestSms = (bool)(dtSimList[i].sendTestSms);

                        testPathJig.rc.simIdType = (int)(Int64)(dtSimList[i].typeCode);

                        break;

                    }
                }


                #region Jig1 Configuration




                startTimeJig = DateTime.Now;

                Tests testJig1 = new Tests(jc, Properties.Settings.Default.JigIdent, scJig);

                globals.initResults(globals.resultsJig1);

                tcvTempJig = new TestControlVariables(testPathJig, testJig1, globals.resultsJig1, this.backgroundWorker1, largeMicro);



                globals.resultsJig1["jigIdent"] = ((int)testJig1.JigIdent).ToString();
                globals.resultsJig1["testSequenceFileName"] = testSequenceFileName;
                globals.resultsJig1["configFilename"] = releaseConfigFileName;

                globals.resultsJig1["vehicleVariant"] = "0";
                for (int i = 0; i < dtVariantID.Count; i++)
                {
                    if (cbVariant.SelectedIndex == -1) break;
                    if ((string)this.cbVariant.Items[cbVariant.SelectedIndex] == (string)(dtVariantID[i].description))
                    {
                        globals.resultsJig1["vehicleVariant"] = dtVariantID[i].varID.ToString();
                        voidLabelCommentJig1 = dtVariantID[i].description;
                        if (testPathJig.labelTemplate.ToLower().Contains("modrive"))
                        {
                            voidLabelCommentJig1 = "STANDARD";
                        }
                    }
                }

                globals.resultsJig1["vehicleFlags"] = "0";
                
                #endregion
                globals.cancelTest = false;
                this.lblTestPass.Visible = false;

                if (globals.forcePcbVariant == true)
                {
                    if (MessageBox.Show("WARNING Forcing PCB Variant. Do you want to continue?", "V3 Test Equipment", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        jigRunning = true;
                        this.backgroundWorker1.RunWorkerAsync(tcvTempJig);
                    }
                    else
                    {
                        cBSim.Enabled = true;
                        cbVariant.Enabled = true;
                        btnSelectCustomer.Enabled = true;
                        btnSelectScript.Enabled = true;
                    }
                }
                else
                {
                    jigRunning = true;
                    this.backgroundWorker1.RunWorkerAsync(tcvTempJig);
                }
            }
            else
            {
                cBSim.Enabled = true;
                cbVariant.Enabled = true;
                btnSelectCustomer.Enabled = true;
                btnSelectScript.Enabled = true;
            }
        }

        private void frmMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "\r")
            {
                if (scanningLabel)
                {
                    if (m_LabelSerialNum == "")
                    {
                        m_LabelSerialNum = this.txtScanData.Text.Trim();
                    }
                }

                if (scanningSimNumber)
                {
                    if (m_SimNumber == "")
                    {
                        m_SimNumber = this.txtScanData.Text.Trim();
                    }

                }

                if (scanningPhoneNumber)
                {
                    if (m_PhoneNumber == "")
                    {
                        m_PhoneNumber = this.txtScanData.Text.Trim();
                    }
                }

                if (scanningBoxID)
                {
                    long temp = 0;
                    if (m_BoxID == "" || long.TryParse(m_BoxID, out temp) == false)
                    {
                        m_BoxID = this.txtScanData.Text.Trim();
                    }
                }

                scanningLabel = false;
                scanningSimNumber = false;
                scanningPhoneNumber = false;
                scanningBoxID = false;

                e.Handled = true;
            }

            if (scanningLabel)
            {
                m_LabelSerialNum += e.KeyChar.ToString();
            }

            if (scanningSimNumber)
            {
                if (e.KeyChar.ToString() == "\r")
                { }
                else
                {
                    m_SimNumber += e.KeyChar.ToString();
                }
            }

            if (scanningPhoneNumber)
            {
                if (e.KeyChar.ToString() == "\b")
                {
                }
                else
                {
                    m_PhoneNumber += e.KeyChar.ToString();
                }
            }

            if (scanningBoxID)
            {
                m_BoxID += e.KeyChar.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //addTestNode("scanBoxID");
            //addResultChild("min = 1");
            //addResultChild("res = sadf");
            //addTestNode("currentTest");
            //addResultChild("min = 1");
            //addResultChild("res = sadf");

            //for (int i = 1; i < 5; i++)
            //{
            //    addTestNode("currentTest" + i.ToString());
            //    addResultChild("min = 1");
            //    addResultChild("res = sadf");
            //    System.Threading.Thread.Sleep(250);
            //    Application.DoEvents();
            //}

            //Label l = new Label();

            //l.Print("123", "12/12/12", "HW 8000", "SW 8.4", "Part 123", "07786673212", "amrV3_2015.prn", Application.StartupPath);

            ////RawPrinterHelper.SendFileToPrinter("Datamax-O'Neil E-4204B Mark III", @"c:\test\pf2015v3.prn"); 

            //voidLabelZebra vl;

            //vl = new voidLabelZebra(voidLabelSoftVerJig1, voidLabelCommentJig1);
            //vl.Print();

            //jerseyTelecomInterface.getPhoneNumber("8944502211130254722");


            //Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //IPAddress ip = IPAddress.Parse("5.79.57.67");
            //IPEndPoint ep = new IPEndPoint(ip, 5555);
            //byte[] test = new byte[50];
            //test[0] = 0x31;
            //test[1] = 0x32;
            //test[2] = 0x33;
            //int res = s.SendTo(test, ep);
            AmlConnector.processFailedInsertProductionData(Application.StartupPath + @"\failedInsertProductionData", this.pgFeedback);
            AmlConnector.processFailedInsertProductionTestData(Application.StartupPath + @"\failedInsertProductionTestResuls", this.pgFeedback);
        }

        private void btnConfigureHardware_Click(object sender, EventArgs e)
        {
            btnConfigureHardware.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            if (can.Init() == 0)
            {
                jc = new JigControl(Properties.Settings.Default.PsuComPort, Properties.Settings.Default.RelayComPort);
                scJig = new SoftwareCommands(can, 1);

                jc.setPowerSupply(13.2, globals.defaultCurrent);

                loadTestConfigsFromDatabase();

                if (globals.rs == null)
                {
                    // rik new kpn 10.188.2.130 500
                    // rik old voda 192.168.0.13 500
                    globals.rs = new rikServer(Properties.Settings.Default.ServerIP, int.Parse(Properties.Settings.Default.ServerPort));
                }

                this.btnSelectCustomer.Enabled = true;
                this.Cursor = Cursors.Default;
            }
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("There was an error with the CAN card");
                this.Close();
            }
        }

        public bool loadValidConfigsFromDatabase(long custId)
        {
            bool retVal = false;
            List<customerToConfigV2> temp = new List<customerToConfigV2>();
            try
            {                
                dtValidConfigs = new List<customerToConfigV2>();
                temp = AmlConnector.getCustomerToConfigs();

                foreach (customerToConfigV2 c in temp)
                {
                    if (c.fkCustomerID == custId)
                    {
                        dtValidConfigs.Add(c);
                    }
                }

                retVal = true;

            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                retVal = false;
            }

            return retVal;
        }
        public bool loadTestConfigsFromDatabase()
        {
            bool retVal = false;
            bool loadConfig = false;
            try
            {
                
                loadValidConfigsFromDatabase((long)dtCustomers[this.cmbSelectCust.SelectedIndex].id);

                dtTesTConfigs = AmlConnector.getConfigs();

                this.comboBox1.Text = "";
                this.comboBox1.Items.Clear();

                for (int i = 0; i < dtTesTConfigs.Count; i++)
                {
                    loadConfig = false;

                    foreach (customerToConfigV2 c in dtValidConfigs)
                    {
                        if ((long)c.fkConfigV2 == (long)dtTesTConfigs[i].id)
                        {
                            loadConfig = true;
                            break;
                        }
                    }

                    if ((((bool)dtTesTConfigs[i].v3_5Production == true) && loadConfig == true) || globals.adminMode == true)
                    {
                        this.comboBox1.Items.Add((string)(dtTesTConfigs[i].displayDescription));
                    }

                }

                loadSimOptions();

            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                retVal = false;
            }

            return retVal;
        }
        private bool loadSimOptions()
        {
            bool retVal = false;

            try
            {
                dtSimList = AmlConnector.getSims();
                this.cBSim.Items.Clear();

                for (int i = 0; i < dtSimList.Count; i++)
                {
                    if (((bool)(dtSimList[i].enable) == true) || globals.adminMode == true)
                    {
                        this.cBSim.Items.Add((string)(dtSimList[i].description));
                    }

                }
               

            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                retVal = false;
            }

            return retVal;
        }

        public bool loadCustomersFromDatabase()
        {
            bool retVal = false;

            try
            {
                dtCustomers = AmlConnector.getCustomers();
                this.cmbSelectCust.Items.Clear();

                for (int i = 0; i < dtCustomers.Count; i++)
                {
                    this.cmbSelectCust.Items.Add(dtCustomers[i].name);
                }

            }
            catch (Exception err)
            {
                System.Diagnostics.Debug.WriteLine(err.Message);
                retVal = false;
            }

            return retVal;
        }
        private void cmbSelectCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbSelectCust.Enabled = false;
            this.btnSelectCustomer.Enabled = true;
            this.btnSelectScript.Enabled = false;
            this.btnSelectScript_Click(sender, new EventArgs());
        }

        private void btnSelectScript_Click(object sender, EventArgs e)
        {
            if (jigRunning == false)
            {
                this.btnSelectScript.Enabled = false;
                
                loadTestConfigsFromDatabase();                
                
                this.comboBox1.Enabled = true;

            }
        }

        private void btnSelectCustomer_Click(object sender, EventArgs e)
        {
            if (jigRunning == false)
            {
                

                this.btnSelectCustomer.Enabled = false;
                this.btnSelectScript.Enabled = false;
                
                loadCustomersFromDatabase();

                this.cmbSelectCust.Enabled = true;
                                               
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlReader myFileStream;
            XmlSerializer mySerializer = new XmlSerializer(typeof(TestConfig));
            XmlSerializer mySerializerRC = new XmlSerializer(typeof(ReleaseConfig));

            this.comboBox1.Enabled = false;

            this.label1.Text = "";

            this.Cursor = Cursors.WaitCursor;

            //for (int i = 0; i < dtTesTConfigs.Rows.Count; i++)
            for (int i = 0; i < dtTesTConfigs.Count; i++)
            {
                //if ((string)this.comboBox1.Items[comboBox1.SelectedIndex] == (string)(dtTesTConfigs.Rows[i]["displayDescription"]))
                if ((string)this.comboBox1.Items[comboBox1.SelectedIndex] == dtTesTConfigs[i].displayDescription)
                {
                    try
                    {
                        //testSequenceFileName = Application.StartupPath + @"\" + (string)(dtTesTConfigs.Rows[i]["testSequence"]);
                        testSequenceFileName = Application.StartupPath + @"\" + (string)(dtTesTConfigs[i].testSequence);
                        largeMicro = (bool)(dtTesTConfigs[i].largeMicro);

                        //if (dtTesTConfigs.Rows[i].ItemArray[1] != null)
                        //{
                        //    releaseConfigFileName = Application.StartupPath + @"\" + (string)(dtTesTConfigs.Rows[i].ItemArray[1]);
                        //}
                        //else
                        //{
                        releaseConfigFileName = "No RC File";
                        //}

                        //if ((bool)(dtTesTConfigs.Rows[i]["allowVariantSelection"]))
                        if ((bool)(dtTesTConfigs[i].allowVariantSelection))
                        {
                            //varIdMapping = (int)(Int64)(dtTesTConfigs.Rows[i]["fwVarMappingID"]);
                            varIdMapping = (int)(Int64)(dtTesTConfigs[i].fwVarMappingID);

                            //dtVariantConfig = testResultDB.GetTable("firmwareVariantMapping");
                            dtVariantConfig = AmlConnector.getFwToVariantMapping();
                            //for (int ij = 0; ij < dtVariantConfig.Rows.Count; ij++)
                            for (int ij = 0; ij < dtVariantConfig.Count; ij++)
                            {
                                //if ((int)(Int64)dtVariantConfig.Rows[ij]["id"] == varIdMapping)
                                if ((int)(Int64)dtVariantConfig[ij].id == varIdMapping)
                                {
                                    //varIdMap =(string)(dtVariantConfig.Rows[ij]["varIDList"]);
                                    varIdMap = (string)(dtVariantConfig[ij].varIDList);
                                    break;
                                }
                            }
                            List<int> listVars = new List<int>();
                            string[] x = varIdMap.Split(new char[] { ',' });
                            foreach (string s in x)
                            {
                                listVars.Add(int.Parse(s));
                            }

                            //dtVariantConfig = testResultDB.GetTable("variantID");                            
                            dtVariantID = AmlConnector.getVariantIdList();
                            this.cbVariant.Items.Clear();

                            //for (int ij = 0; ij < dtVariantConfig.Rows.Count; ij++)
                            for (int ij = 0; ij < dtVariantID.Count; ij++)
                            {
                                //if (listVars.Contains((int)(Int64)dtVariantConfig.Rows[ij]["varId"]) || adminMode == true)
                                if (listVars.Contains((int)(Int64)dtVariantID[ij].varID) || globals.adminMode == true)
                                {
                                    //this.cbVariant.Items.Add((string)(dtVariantConfig.Rows[ij].ItemArray[1]));
                                    this.cbVariant.Items.Add((string)(dtVariantID[ij].description));
                                }
                            }

                            this.cbVariant.SelectedIndex = 0;
                            this.cbVariant.Visible = true;
                            this.lblSelVar.Visible = true;

                        }
                        else
                        {
                            this.cbVariant.Visible = false;

                        }

                        

                        voidLabelSoftVerJig1 = (string)(dtTesTConfigs[i].voidSoftware);
                        

                        allowVehicleFlags = (bool)(dtTesTConfigs[i].allowVehicleFlags);
                        allowLite = (bool)(dtTesTConfigs[i].allowLite);

                        
                        
                        myFileStream = new XmlTextReader(new FileStream(testSequenceFileName, FileMode.Open));
                        if (mySerializer.CanDeserialize(myFileStream))
                        {
                            testPathJig = (TestConfig)mySerializer.Deserialize(myFileStream);

                            myFileStream.Close();
                                
                                

                            this.comboBox1.Visible = true;
                            this.comboBox1.Enabled = false;

                            this.label1.Text = (string)this.comboBox1.Items[comboBox1.SelectedIndex];
                            this.label1.Visible = true;

                            globals.flyingLead = false;
                            if (label1.Text.Contains("Flying") == true)
                            {
                                globals.flyingLead = true;
                            }

                            this.btnSelectScript.Enabled = true;


                            this.lblSimSel.Visible = true;

                            this.cBSim.SelectedIndex = 0;
                            this.cBSim.Visible = true;



                               
                        }
                        else
                        {
                            Application.DoEvents();
                            MessageBox.Show("Error with Test Script. Contact Administrator");
                        }
                        
                        
                    }
                    catch (Exception localErr)
                    {
                        System.Diagnostics.Debug.WriteLine(localErr.Message);
                        btnSelectScript.Enabled = true;
                    }
                    break;
                }
            }

            this.comboBox1.Enabled = false;
            this.Cursor = Cursors.Default;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((bool)e.Result == true)
            {
                this.lblTestPass.Text = "PASS";
                this.lblTestPass.BackColor = Color.LightGreen;
            }
            else
            {
                this.lblTestPass.Text = "FAIL";
                this.lblTestPass.BackColor = Color.Red;
            }

            this.lblTestPass.Visible = true;

            lblPass.Text = "Pass" + Environment.NewLine + pass.ToString();
            lblFail.Text = "Fail" + Environment.NewLine + fail.ToString();
            lblYield.Text = "Yield" + Environment.NewLine + (((double)pass / (pass + fail)) * 100).ToString("N2");

            jigRunning = false;

            //jc.resetJig();

            cBSim.Enabled = true;
            cbVariant.Enabled = true;
            btnSelectCustomer.Enabled = true;
            btnSelectScript.Enabled = true;

            autoTestCnt = 1500;
            if (autoTestCnt < 1500)
            {
                autoTestCnt++;
                this.timer1.Enabled = true;
            }
            else
            {
                tmrHandle.Enabled = true;
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (jigRunning == true)
            {
                MessageBox.Show("To prevent loss of data please wait for test to finish before closing program", "RTU Tester", MessageBoxButtons.OK);
                e.Cancel = true;
            }
            else
            {
                try
                {
                    can.CloseInterface();
                    can = null;
                }
                catch (Exception err)
                {

                    System.Diagnostics.Debug.WriteLine(err.Message);
                }
            }
        }

        private string createTestLog(TreeView t)
        {
            string retVal = "";            
            foreach (TreeNode sub in t.Nodes)
            {                
                retVal += sub.Text + "___" +  getChildren(sub);
            }

            retVal = retVal.Replace(" ", "");
            System.Diagnostics.Debug.WriteLine(retVal);
            return retVal;
        }

        string getChildren(TreeNode tn)
        {

            string retVal = "";
            foreach (TreeNode sub in tn.Nodes)
            {
                retVal += "__" +sub.Text + "___"  +  getChildren(sub);
            }
            return retVal;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.btnStart_Click(this, new EventArgs());
            timer1.Enabled = false;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (can.Init() == 0)
            {
                jc = new JigControl(Properties.Settings.Default.PsuComPort, Properties.Settings.Default.RelayComPort);
                
                scJig = new SoftwareCommands(can, 1);

                loadTestConfigsFromDatabase();

                if (globals.rs == null)
                {
                    // rik new kpn 10.188.2.130 500
                    // rik old voda 192.168.0.13 500
                    globals.rs = new rikServer(Properties.Settings.Default.ServerIP, int.Parse(Properties.Settings.Default.ServerPort));
                }

                this.btnSelectCustomer.Enabled = true;

                tmrHandle.Enabled = true;
                this.Cursor = Cursors.Default;
            }
            else
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("There was an error with the CAN card");
                this.Close();
            }
        }
        private bool newUnit = false;

        private void tmrHandle_Tick(object sender, EventArgs e)
        {
            if (jigRunning == false)
            {
                if ((this.cmbSelectCust.SelectedIndex > -1) && (this.comboBox1.SelectedIndex > -1) && (this.cBSim.SelectedIndex > -1) && (this.cbVariant.SelectedIndex > -1))
                {
                    if (jc.isJigOpen())
                    {
                        newUnit = true;
                        lblTestPass.Visible = false;
                        pbTests.Value = 0;
                        pgFeedback.Value = 0;
                        lblMessage.Text = "Place new Unit in Jig and close the handle";
                        label1.Text = "";
                        txtScanData.Text = "";
                        txtScanData.Visible = false;
                        tvTests.Nodes.Clear();
                    }

                    if (newUnit == true)
                    {

                        if (jc.isJigShut())
                        {
                            newUnit = false;
                            this.btnStart_Click(this, new EventArgs());
                        }
                    }
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            pass = 10;
            fail = 1;
            
            lblPass.Text = "Pass" + Environment.NewLine + pass.ToString();
            lblFail.Text = "Fail" + Environment.NewLine + fail.ToString();
            lblYield.Text = "Yield" + Environment.NewLine + "0.00";
        }

        private void lblTestPass_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void adminToolStripMenuItem_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                globals.cancelTest = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            jerseyTelecomInterface.getPhoneNumberLookup("234507093025472");
            jerseyTelecomInterface.getPhoneNumberLookup("234507093104514");

            string phone = AmlConnector.getJerseyPhoneNumber("234507093104602");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            voidLabelZebra vl = new voidLabelZebra("SW V3 9.0.1 DFU SD", "FORD/VOLVO/LR", "3080217123");
            vl.Print(true);
        }

        private void advancedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword fp = new frmPassword(loginRequest.advanced);
            fp.ShowDialog();
            if (fp.PasswordValidated == true)
            {
                frmAdvanced fa = new frmAdvanced();

                fa.ShowDialog();
                if(fa.forcePcbVariant > 0)
                {
                    globals.forcePcbVariant = true;
                    globals.forcedPcbVariant = fa.forcePcbVariant;
                    advancedToolStripMenuItem.BackColor = Color.Red;
                }
                else
                {
                    globals.forcePcbVariant = false;
                    globals.forcedPcbVariant = 0;
                    advancedToolStripMenuItem.BackColor = adminToolStripMenuItem.BackColor;
                }
            }
        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Label l = new Label();
            LabelPrintReturnCodes lprc;
            
            lprc = l.Print("357111111111113", @"XX/XX/XXXX", "X000X000", "1000000000000", "V3", "07544545454", "modriveV3_2016_right.prn", filePath);
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            this.displayMessage("Please wait Jig Config in progress");
            Application.DoEvents();
            this.Enabled = false;
            jc.resetJig(true);
            this.displayMessage("");
            this.Enabled = true;
        }

       
    }
}
