﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace V3ProductionTest
{
    /// <summary>
    /// Provides functionality to log and record data, user actions, exceptions etc...
    /// </summary>
    public class LogFile
    {

        /// <summary>
        /// Writes the specified message to the log
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logType">Type of the log.</param>
        private static void Write(StreamWriter stream, string message)
        {
            // add a time stamp to the message
            message = string.Format("{0} {1}", DateTime.Now, message);
            Console.WriteLine(message);

            //todo: encrypt the message
            string cryptedMessage = message;

            // write the message to the file
            stream.WriteLine(cryptedMessage);
            stream.Flush();

        }


        /// <summary>
        /// Writes a message to the log using the 'General' style
        /// </summary>
        /// <param name="message">The message.</param>
        public static void WriteGeneral(string message)
        {
            StreamWriter sw = new StreamWriter("debug.log", true);
            Write(sw, message);
            sw.Close();
        }


        /// <summary>
        /// Writes an exception message to the log
        /// </summary>
        /// <param name="message">The message.</param>
        public static void WriteError(Exception message)
        {
            string msg = "Message: " + message.Message;
            msg += "\nStackTrace: " + message.StackTrace;
            WriteError(msg);
        }


        /// <summary>
        /// Writes an error message to the log
        /// </summary>
        /// <param name="message">The message.</param>
        public static void WriteError(string message)
        {
            StreamWriter sw = new StreamWriter("error.log", true);
            Write(sw, message);
            sw.Close();
        }
    }
}
