﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace V3ProductionTest
{
    public static class globals
    {
        public static bool flyingLead = false;

        public static Dictionary<string, string> resultsJig1 = new Dictionary<string, string>();
        

        public static rikServer rs;

        public static bool adminMode = false;

        public static bool cancelTest = false;

        public static double defaultCurrent = 5.0;

        public static bool forcePcbVariant = false;
        public static byte forcedPcbVariant = 0;

        public static void initResults(Dictionary<string, string> res)
        {
            if (res.Keys.Count > 0)
            {
                res.Clear();
            }
            res.Add("imei", "");
            res.Add("pass", "");
            res.Add("timeStamp", DateTime.UtcNow.ToString("yyyyMMddHHmmss"));
            res.Add("testTime", "");
            res.Add("testSequence", "");
            res.Add("testSequenceFileName", "");
            res.Add("prodTestFwVersion", "");
            res.Add("configFilename", "");
            res.Add("processorVersion", "");
            res.Add("currentDrawA", "");

            res.Add("batterySwitchPointUp", "");
            res.Add("batterySwitchPointdown", "");
            res.Add("adBatteryLow", "");
            res.Add("adBatteryMid", "");
            res.Add("adBatteryHigh", "");
            res.Add("simNumber", "");
            res.Add("imsi", "");
            res.Add("simTypeCode", "");
            res.Add("modemType", "");
            res.Add("modemFirmwareVersion", "");
            res.Add("phoneNumber", "");

            res.Add("gpsStatus", "");
            res.Add("gpsVoltage", "");
            res.Add("IgnitionConnected", "");
            res.Add("IgnitionDisconnected", "");
            res.Add("xAxis", "");
            res.Add("yAxis", "");
            res.Add("zAxis", "");
            res.Add("auxpowerSupply", "");
            res.Add("kLineVoltageNoPullup", "");
            res.Add("kLineVoltageWithPullup", "");
            res.Add("ioOffVoltage", "");

            res.Add("ioOnVoltage", "");
            res.Add("ioConfig", "");
            res.Add("gpsNumSats", "");
            res.Add("gpsSnr", "");
            res.Add("ictPcbId", "");
            res.Add("ictPcbPassFail", "");
            res.Add("ictPcbFailCount", "");
            res.Add("ictPcbTestDate", "");
            res.Add("appCodePath", "");
            res.Add("appCodePathLoopCount", "");
            res.Add("jigIdent", "");

            res.Add("atePassFail", "");
            res.Add("ateFailCount", "");
            res.Add("ateTestDate", "");
            res.Add("retestScanImei", "");
            res.Add("sleepCurrent", "");
            res.Add("pcbVariant1", "");
            res.Add("vehicleVariant", "");
            res.Add("hardwareVariant", "");
            res.Add("boxID", "");
            res.Add("gyroID", "");
            res.Add("gyroX", "");
            res.Add("gyroY", "");
            res.Add("gyroZ", "");
            res.Add("vehicleFlags", "");
            res.Add("testLog", "");
        }


    }
}
