﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace V3ProductionTest
{
    public class JigControl
    {
        private LabJack _lj;
        private RelayBoard _rs;
        private PSUControl _pc;
        public jLinkAPI jl;

        private DateTime jig1PowerOn;
        private DateTime jig1PowerOff;        

        public JigControl(int psuComPort, int relayComPort)
        {
            _lj = new LabJack();
            _rs = new RelayBoard();
            _pc = new PSUControl((PsuType)Properties.Settings.Default.PsuType);
            jl = new jLinkAPI();

            _lj.configure();
            _rs.connect(relayComPort);
            _pc.init(psuComPort);

            //jl.connect(Properties.Settings.Default.Processor);
            //jl.close();

            jig1PowerOn = DateTime.MinValue;
            jig1PowerOff = jig1PowerOn;
            
        }

        #region PowerSupply
        public bool setPowerSupply(double voltage, double currentLimit)
        {
            bool retVal = false;
            try
            {
                if (_pc.getOutputVoltage() != voltage)
                {
                    _pc.setOutputVoltage(voltage);
                }
                if (_pc.getOutputCurrentLimit() != currentLimit)
                {
                    _pc.setOutputCurrentLimit(currentLimit);
                }
                retVal = true;
            }
            catch (Exception err)
            {
                LogFile.WriteError(err);
            }
            return retVal;
        }

        public bool getPowerSupplySettings(ref double voltage, ref double currentLimit)
        {            
            bool retVal = true;

            try
            {
                voltage = _pc.getOutputVoltage();
                currentLimit = _pc.getOutputCurrent();                
            }
            catch (Exception err)
            {
                LogFile.WriteError(err);
                retVal = false;
            }
            return retVal;
        }

        public bool setPowerSupplyState(bool on)
        {
            bool retVal = false;

            try
            {
                _pc.output(on);
                
                retVal = true;
            }
            catch (Exception err)
            {
                LogFile.WriteError(err);
                retVal = false;
            }
            return retVal;
        }

        public bool setPowerSupplyVoltage(double voltage)
        {
            bool retVal = false;
            try
            {
                _pc.setOutputVoltage(voltage);
                retVal = true;
            }
            catch (Exception err)
            {
                LogFile.WriteError(err);
                retVal = false;
            }

            return retVal;
        }

        public bool measureCurrent(ref double current)
        {
            bool retVal = false;
            try
            {
                current = _pc.getOutputCurrent();
                current *= 1000;
                retVal = true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

            return retVal;
        }

        #endregion

        #region DMM
        

        public bool measureVoltage(ref double voltage)
        {           
            bool retVal = true;
            voltage = _lj.ReadAnalogChannel(0, 100);
            return retVal;
        }

        #endregion

        #region Relay Control

        public bool resetJig(bool fullReset)
        {
            bool retVal = false;
                        
            _rs.setRelayState(RelayBoard.relayStates.RelayAll_OFF);
            //fullReset = false;
            if (fullReset)
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay1_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay1_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay2_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay2_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay3_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay3_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay4_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay4_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay5_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay5_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay6_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay6_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay7_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay7_OFF);

                _rs.setRelayState(RelayBoard.relayStates.Relay8_ON);
                _rs.setRelayState(RelayBoard.relayStates.Relay8_OFF);
            }
            return retVal;
        }
        public bool setVbatt( Boolean enable)
        {
            if (enable == true)
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay1_ON);
                if (jig1PowerOn == DateTime.MinValue)
                {
                    jig1PowerOn = DateTime.Now;
                }
            }
            else
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay1_OFF);
                jig1PowerOff = DateTime.Now;
                jig1PowerOn = DateTime.MinValue;
            }

            return true;
        }

        public bool setVIgn(Boolean enable)
        {
            
            if (enable == true)
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay2_ON);
            }
            else
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay2_OFF);
            }
            
            return true;
        }

        public bool set10R( Boolean enable)
        {

            return false;
        }

        public bool setBatSup(Boolean enable)
        {
            //if (jig == JigIdent.Jig1)
            //{
            //    if (enable == true)
            //    {
            //        pio0_portA = pio0_portA | relay4_BatSup;
            //    }
            //    else
            //    {
            //        pio0_portA = pio0_portA & ~relay4_BatSup;
            //    }
            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortAHandle, (byte)pio0_portA);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortAHandle, PIO.INFINITE);
            //    }
            //}
            //else
            //{
            //    if (enable == true)
            //    {
            //        pio0_portB = pio0_portB | relay16_BatSup;
            //    }
            //    else
            //    {
            //        pio0_portB = pio0_portB & ~relay16_BatSup;
            //    }

            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortBHandle, (byte)pio0_portB);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortBHandle, PIO.INFINITE);
            //    }
            //}

            //if (nStatus == PIO.BCT_OK)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        public bool setBatSupDMM(Boolean enable)
        {
            //if (jig == JigIdent.Jig1)
            //{
            //    if (enable == true)
            //    {
            //        pio0_portA = pio0_portA | relay5_BatSupDmm;
            //    }
            //    else
            //    {
            //        pio0_portA = pio0_portA & ~relay5_BatSupDmm;
            //    }
            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortAHandle, (byte)pio0_portA);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortAHandle, PIO.INFINITE);
            //    }
            //}
            //else
            //{
            //    if (enable == true)
            //    {
            //        pio0_portB = pio0_portB | relay17_BatSupDmm;
            //    }
            //    else
            //    {
            //        pio0_portB = pio0_portB & ~relay17_BatSupDmm;
            //    }

            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortBHandle, (byte)pio0_portB);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortBHandle, PIO.INFINITE);
            //    }
            //}

            //if (nStatus == PIO.BCT_OK)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        public bool set10RDMM( Boolean enable)
        {
            //if (jig == JigIdent.Jig1)
            //{
            //    if (enable == true)
            //    {
            //        pio0_portA = pio0_portA | relay6_10RDMM;
            //    }
            //    else
            //    {
            //        pio0_portA = pio0_portA & ~relay6_10RDMM;
            //    }
            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortAHandle, (byte)pio0_portA);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortAHandle, PIO.INFINITE);
            //    }
            //}
            //else
            //{
            //    if (enable == true)
            //    {
            //        pio0_portC = pio0_portC | relay18_10RDMM;
            //    }
            //    else
            //    {
            //        pio0_portC = pio0_portC & ~relay18_10RDMM;
            //    }

            //    nStatus = PIO.BCTWritePort(ref nPciPio0PortCHandle, (byte)pio0_portC);
            //    if (nStatus == PIO.BCT_IO_PENDING)
            //    {
            //        nStatus = PIO.BCTWait(ref nPciPio0PortCHandle, PIO.INFINITE);
            //    }
            //}

            //if (nStatus == PIO.BCT_OK)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        public bool setKLineDMM( Boolean enable)
        {
            
                if (enable == true)
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay6_ON);
                }
                else
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay6_OFF);
                }
                            
                return true;
           
        }

        public bool setGPS( Boolean enable)
        {
            
                if (enable == true)
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay8_ON);
                }
                else
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay8_OFF);
                }
                           
                return true;
           
        }

        public bool setIO( Boolean enable)
        {
            
                return false;
           
        }

        public bool setIODMM( Boolean enable)
        {
           
                return false;
            
        }

        public bool setCAN( Boolean enable)
        {
            if (enable == true)
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay7_ON);
            }
            else
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay7_OFF);
            }
            
                return true;
            
        }

        public bool setKLine( Boolean enable)
        {
            
                if (enable == true)
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay4_ON);
                }
                else
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay4_OFF);
                }

               
            
                return true;
            
        }

        public bool setKLineReflectorPower(Boolean enable)
        {

            if (enable == true)
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay5_ON);
            }
            else
            {
                _rs.setRelayState(RelayBoard.relayStates.Relay5_OFF);
            }



            return true;

        }

        public bool lockJig( Boolean enable)
        {            
                if (enable == true)
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay3_ON);
                }
                else
                {
                    _rs.setRelayState(RelayBoard.relayStates.Relay3_OFF);
                }
            
                return true;
           
        }

        public bool lockLabel( Boolean enable)
        {
           
                return true;
            
        }

        public bool isLabelLockClosed()
        {          

            return true;

        }


        public bool enableUlink()
        {
            
                return false;
           
        }

        public bool isJigShut()
        {
            bool retVal = false;
            double temp = _lj.ReadAnalogChannel(2, 10);
            if (temp > 2.0)
            {
                retVal = true;
            }
            return retVal;
        }
        public bool isJigOpen()
        {
            bool retVal = false;
            double temp = _lj.ReadAnalogChannel(3, 10);
            if (temp > 2.0)
            {
                retVal = true;
            }
            return retVal;
        }
        #endregion
    }
}
