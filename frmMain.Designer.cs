﻿namespace V3ProductionTest
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtScanData = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tvTests = new System.Windows.Forms.TreeView();
            this.pbTests = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.pgFeedback = new System.Windows.Forms.ProgressBar();
            this.btnConfigureHardware = new System.Windows.Forms.Button();
            this.cmbSelectCust = new System.Windows.Forms.ComboBox();
            this.btnSelectCustomer = new System.Windows.Forms.Button();
            this.lblSelVar = new System.Windows.Forms.Label();
            this.cbVariant = new System.Windows.Forms.ComboBox();
            this.cBSim = new System.Windows.Forms.ComboBox();
            this.lblSimSel = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnSelectScript = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblTestPass = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrHandle = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblYield = new System.Windows.Forms.Label();
            this.lblFail = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(170, 530);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(338, 23);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtScanData
            // 
            this.txtScanData.Location = new System.Drawing.Point(238, 569);
            this.txtScanData.Name = "txtScanData";
            this.txtScanData.Size = new System.Drawing.Size(202, 20);
            this.txtScanData.TabIndex = 1;
            this.txtScanData.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(12, 244);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(103, 55);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tvTests
            // 
            this.tvTests.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvTests.ItemHeight = 30;
            this.tvTests.Location = new System.Drawing.Point(162, 339);
            this.tvTests.Name = "tvTests";
            this.tvTests.Size = new System.Drawing.Size(354, 97);
            this.tvTests.TabIndex = 3;
            // 
            // pbTests
            // 
            this.pbTests.Location = new System.Drawing.Point(11, 491);
            this.pbTests.Name = "pbTests";
            this.pbTests.Size = new System.Drawing.Size(655, 23);
            this.pbTests.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 306);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "AML Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pgFeedback
            // 
            this.pgFeedback.Location = new System.Drawing.Point(12, 452);
            this.pgFeedback.Name = "pgFeedback";
            this.pgFeedback.Size = new System.Drawing.Size(655, 23);
            this.pgFeedback.TabIndex = 6;
            // 
            // btnConfigureHardware
            // 
            this.btnConfigureHardware.Location = new System.Drawing.Point(12, 197);
            this.btnConfigureHardware.Name = "btnConfigureHardware";
            this.btnConfigureHardware.Size = new System.Drawing.Size(75, 41);
            this.btnConfigureHardware.TabIndex = 7;
            this.btnConfigureHardware.Text = "Configure Hardware";
            this.btnConfigureHardware.UseVisualStyleBackColor = true;
            this.btnConfigureHardware.Visible = false;
            this.btnConfigureHardware.Click += new System.EventHandler(this.btnConfigureHardware_Click);
            // 
            // cmbSelectCust
            // 
            this.cmbSelectCust.Enabled = false;
            this.cmbSelectCust.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSelectCust.FormattingEnabled = true;
            this.cmbSelectCust.Location = new System.Drawing.Point(252, 32);
            this.cmbSelectCust.Name = "cmbSelectCust";
            this.cmbSelectCust.Size = new System.Drawing.Size(420, 28);
            this.cmbSelectCust.TabIndex = 54;
            this.cmbSelectCust.SelectedIndexChanged += new System.EventHandler(this.cmbSelectCust_SelectedIndexChanged);
            // 
            // btnSelectCustomer
            // 
            this.btnSelectCustomer.Enabled = false;
            this.btnSelectCustomer.Location = new System.Drawing.Point(118, 30);
            this.btnSelectCustomer.Name = "btnSelectCustomer";
            this.btnSelectCustomer.Size = new System.Drawing.Size(128, 32);
            this.btnSelectCustomer.TabIndex = 53;
            this.btnSelectCustomer.Text = "Select Label";
            this.btnSelectCustomer.UseVisualStyleBackColor = true;
            this.btnSelectCustomer.Click += new System.EventHandler(this.btnSelectCustomer_Click);
            // 
            // lblSelVar
            // 
            this.lblSelVar.AutoSize = true;
            this.lblSelVar.Location = new System.Drawing.Point(249, 127);
            this.lblSelVar.Name = "lblSelVar";
            this.lblSelVar.Size = new System.Drawing.Size(73, 13);
            this.lblSelVar.TabIndex = 52;
            this.lblSelVar.Text = "Select Variant";
            this.lblSelVar.Visible = false;
            // 
            // cbVariant
            // 
            this.cbVariant.FormattingEnabled = true;
            this.cbVariant.Location = new System.Drawing.Point(356, 124);
            this.cbVariant.Name = "cbVariant";
            this.cbVariant.Size = new System.Drawing.Size(316, 21);
            this.cbVariant.TabIndex = 49;
            // 
            // cBSim
            // 
            this.cBSim.FormattingEnabled = true;
            this.cBSim.Location = new System.Drawing.Point(356, 97);
            this.cBSim.Name = "cBSim";
            this.cBSim.Size = new System.Drawing.Size(316, 21);
            this.cBSim.TabIndex = 51;
            // 
            // lblSimSel
            // 
            this.lblSimSel.AutoSize = true;
            this.lblSimSel.Location = new System.Drawing.Point(249, 100);
            this.lblSimSel.Name = "lblSimSel";
            this.lblSimSel.Size = new System.Drawing.Size(101, 13);
            this.lblSimSel.TabIndex = 50;
            this.lblSimSel.Text = "Select SIM Provider";
            this.lblSimSel.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(252, 66);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(420, 24);
            this.comboBox1.TabIndex = 48;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnSelectScript
            // 
            this.btnSelectScript.Enabled = false;
            this.btnSelectScript.Location = new System.Drawing.Point(119, 62);
            this.btnSelectScript.Name = "btnSelectScript";
            this.btnSelectScript.Size = new System.Drawing.Size(127, 34);
            this.btnSelectScript.TabIndex = 47;
            this.btnSelectScript.Text = "Select Firmware";
            this.btnSelectScript.UseVisualStyleBackColor = true;
            this.btnSelectScript.Click += new System.EventHandler(this.btnSelectScript_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(324, 306);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 57;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.runTestSequence);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // lblTestPass
            // 
            this.lblTestPass.AutoSize = true;
            this.lblTestPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestPass.Location = new System.Drawing.Point(246, 217);
            this.lblTestPass.Name = "lblTestPass";
            this.lblTestPass.Size = new System.Drawing.Size(208, 73);
            this.lblTestPass.TabIndex = 58;
            this.lblTestPass.Text = "PASS";
            this.lblTestPass.Visible = false;
            this.lblTestPass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblTestPass_MouseClick);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adminToolStripMenuItem,
            this.advancedToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(679, 24);
            this.menuStrip1.TabIndex = 59;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            this.adminToolStripMenuItem.Click += new System.EventHandler(this.adminToolStripMenuItem_Click);
            this.adminToolStripMenuItem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.adminToolStripMenuItem_MouseUp);
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.advancedToolStripMenuItem.Text = "Advanced";
            this.advancedToolStripMenuItem.Click += new System.EventHandler(this.advancedToolStripMenuItem_Click);
            // 
            // tmrHandle
            // 
            this.tmrHandle.Interval = 1000;
            this.tmrHandle.Tick += new System.EventHandler(this.tmrHandle_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.lblYield);
            this.panel1.Controls.Add(this.lblFail);
            this.panel1.Controls.Add(this.lblPass);
            this.panel1.Location = new System.Drawing.Point(12, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(84, 150);
            this.panel1.TabIndex = 60;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(4, 123);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 61;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblYield
            // 
            this.lblYield.Location = new System.Drawing.Point(5, 80);
            this.lblYield.Name = "lblYield";
            this.lblYield.Size = new System.Drawing.Size(75, 37);
            this.lblYield.TabIndex = 2;
            this.lblYield.Text = "Yield";
            this.lblYield.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFail
            // 
            this.lblFail.Location = new System.Drawing.Point(5, 43);
            this.lblFail.Name = "lblFail";
            this.lblFail.Size = new System.Drawing.Size(75, 37);
            this.lblFail.TabIndex = 1;
            this.lblFail.Text = "Fail";
            this.lblFail.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPass
            // 
            this.lblPass.Location = new System.Drawing.Point(5, 6);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(75, 37);
            this.lblPass.TabIndex = 0;
            this.lblPass.Text = "Pass";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 335);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 61;
            this.button2.Text = "Jersey";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(11, 364);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 62;
            this.button3.Text = "print vl";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(17, 393);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 63;
            this.button4.Text = "test label";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(679, 595);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblTestPass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbSelectCust);
            this.Controls.Add(this.btnSelectCustomer);
            this.Controls.Add(this.lblSelVar);
            this.Controls.Add(this.cbVariant);
            this.Controls.Add(this.cBSim);
            this.Controls.Add(this.lblSimSel);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnSelectScript);
            this.Controls.Add(this.btnConfigureHardware);
            this.Controls.Add(this.pgFeedback);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pbTests);
            this.Controls.Add(this.tvTests);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txtScanData);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "V3.5 RTU Production Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmMain_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtScanData;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TreeView tvTests;
        private System.Windows.Forms.ProgressBar pbTests;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar pgFeedback;
        private System.Windows.Forms.Button btnConfigureHardware;
        private System.Windows.Forms.ComboBox cmbSelectCust;
        private System.Windows.Forms.Button btnSelectCustomer;
        private System.Windows.Forms.Label lblSelVar;
        private System.Windows.Forms.ComboBox cbVariant;
        private System.Windows.Forms.ComboBox cBSim;
        private System.Windows.Forms.Label lblSimSel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnSelectScript;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lblTestPass;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.Timer tmrHandle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblYield;
        private System.Windows.Forms.Label lblFail;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.Button button4;

    }
}

