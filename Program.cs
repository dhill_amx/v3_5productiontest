﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace V3ProductionTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //kpn.getPhoneNumber("8931086113127781181");

            
            if (MessageBox.Show("Do you want debug mode?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Run(new frmDebug());
            }
            else 
            { 
                Application.Run(new frmMain()); 
            }
        }
    }
}
