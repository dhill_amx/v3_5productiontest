﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace V3ProductionTest
{
    public partial class frmAdvanced : Form
    {
        public byte forcePcbVariant { get; set; }
        public frmAdvanced()
        {
            InitializeComponent();
            forcePcbVariant = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(rb1.Checked == true)
            {
                forcePcbVariant = 0x20;
            }
            else if (rb2.Checked == true)
            {
                forcePcbVariant = 0x21;
            }
            else if (rb3.Checked == true)
            {
                forcePcbVariant = 0x22;
            }
            else if (rb4.Checked == true)
            {
                forcePcbVariant = 0x23;
            }
            else if (rb5.Checked == true)
            {
                forcePcbVariant = 0x24;
            }
            else if (rb6.Checked == true)
            {
                forcePcbVariant = 0;
            }
            else
            {
                forcePcbVariant = 0;
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            forcePcbVariant = 0;

            this.Close();
        }

        private void frmAdvanced_Load(object sender, EventArgs e)
        {
            if(globals.forcePcbVariant== true)
            {
                switch (globals.forcedPcbVariant)
                {
                    case 0:
                        { rb6.Checked = true; break; }
                    case 0x20:
                        { rb1.Checked = true; break; }
                    case 0x21:
                        { rb2.Checked = true; break; }
                    case 0x22:
                        { rb3.Checked = true; break; }
                    case 0x23:
                        { rb4.Checked = true; break; }
                    case 0x24:
                        { rb5.Checked = true; break; }
                }
            }
            else
            {
                rb6.Checked = true;
            }
        }
    }
}
