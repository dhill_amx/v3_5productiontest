﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Web;
using System.Net;

namespace V3ProductionTest
{
    public static class AmlConnector
    {
        private static List<Customer> customers;
        private static List<configV2> configs;
        private static List<customerToConfigV2> custToConfigs;
        private static List<firmwareVariantMapping> fwToVariantMapping;
        private static List<simDetails> sims;
        private static List<variantID> variantIdList;
        private static List<Id> idList;

        private static string amlServer = "http://aml.airmaxgroup.net:8015";
        private static string amlProductionTestInsert = "http://aml.airmaxgroup.net:8015/Services/ByName/REST/Peers/ByName/Control/Actions/ByName/InsertProductionTest?username=productiontest&userpass=dfhdews_9387GRR5&rolename=operator&rolepass=rolepasswd";
        private static string amlRtuProductionTestInsert = "http://aml.airmaxgroup.net:8015/Services/ByName/REST/Peers/ByName/Control/Actions/ByName/InsertRtuProductionTest?username=productiontest&userpass=dfhdews_9387GRR5&rolename=operator&rolepass=rolepasswd";
        private static string amlLink = "http://aml.airmaxgroup.net:8015/Services/ByName/REST/Peers/ByName/Control/Views/ByName/{0}/?username=productiontest&userpass=dfhdews_9387GRR5";
        private static string amrNameSpace = "http://www.airmaxremote.com/meta/ns/amr";
        private static string amlNameSpace = "http://www.airmaxremote.com/meta/ns/aml";
        private static string amlIncomingSMS = "https://api.airmaxremote.com/Services/ByName/REST/Peers/ByName/Control/Views/ByName/RTUProductionInboundSMS?username=test&userpass=userpasswd&sourcenumber={0}";
        private static string amlUdpReceived = "https://api.airmaxremote.com/Services/ByName/REST/Peers/ByName/Control/Views/ByName/RTUProductionInboundUDP?username=test&userpass=userpasswd&unitimei={0}";
        private static string amlJerseyMsisdn = "https://api.airmaxremote.com/Services/ByName/REST/Peers/ByName/Control/Views/ByName/RTUProductionJerseyTelecomSIMs?username=test&userpass=userpasswd&simimsi={0}";

        private static XElement getData(string table)
        {
            XName transferitem = XName.Get("transferitem", amrNameSpace);

            XElement root = null;
            try
            {
                string request = string.Format(amlLink, table);
                
                root = XElement.Load(request);

                foreach (var transferitemelement in root.Descendants(transferitem))
                {
                    string type = transferitemelement.Attribute("type").Value;
                    if (type.Equals("ErrorRecord"))
                    {
                        return null;
                    }
                }
                root.Save(table + ".xml");
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                root = XElement.Load(table + ".xml");
            }
            return root;
        }

        public static List<Id> getIds()
        {
            if (idList  == null)
            {
                XElement root = getData("RTUProductionIDs");
                XName x = XName.Get("id", amrNameSpace);

                idList = new List<Id>();

                foreach (var cust in root.Descendants(x))
                {
                    Id c = new Id();
                    
                    foreach (var rec in cust.Descendants(XName.Get("productionvalue", amrNameSpace)))
                    {
                        if (rec.Attribute("interpretation").Value == "Password")
                        {
                            c.password = rec.Attribute("value").Value;
                        }
                        
                    }
                    idList.Add(c);
                }
            }
            return idList;
        }

        public static List<Customer> getCustomers()
        {
            if (customers == null)
            {
                XElement root = getData("RTUProductionCustomers");
                XName x = XName.Get("customer", amrNameSpace);
                
                customers = new List<Customer>();

                foreach (var cust in root.Descendants(x))
                {
                    Customer c = new Customer();
                    c.id = int.Parse(cust.Attribute("key").Value);

                    foreach (var rec in cust.Descendants(XName.Get("productionvalue",amrNameSpace)))
                    {
                        if (rec.Attribute("interpretation").Value == "CustomerName")
                        {
                            c.name = rec.Attribute("value").Value;
                        }
                        if (rec.Attribute("interpretation").Value == "LabelPRNFile")
                        {
                            c.labelFile = rec.Attribute("value").Value;
                        }
                                             
                    }
                    if(c.name.Contains("V3"))
                    {
                        customers.Add(c);
                    }
                }
            }
            return customers;
        }

        public static List<configV2> getConfigs()
        {
            if (configs == null)
            {
                XElement root = getData("RTUProductionConfigV2s");
                XName x = XName.Get("configv2", amrNameSpace);

                configs = new List<configV2>();

                foreach (var conf in root.Descendants(x))
                {
                    configV2 c = new configV2();
                    c.id = int.Parse(conf.Attribute("key").Value);

                    foreach (var rec in conf.Descendants(XName.Get("productionvalue", amrNameSpace)))
                    {
                        switch (rec.Attribute("interpretation").Value)
                        {
                            case "TestSequence":
                                {
                                    c.testSequence = rec.Attribute("value").Value;
                                    break;
                                }
                            case "DisplayDescription":
                                {
                                    c.displayDescription = rec.Attribute("value").Value;
                                    break;
                                }
                            case "Production":
                                {
                                    c.production = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                            case "VoidSoftware":
                                {
                                    c.voidSoftware = rec.Attribute("value").Value;
                                    break;
                                }
                            case "AllowVariantSelection":
                                {
                                    c.allowVariantSelection = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                            case "FirmwareVariantMappingId":
                                {
                                    c.fwVarMappingID = int.Parse(rec.Attribute("value").Value);
                                    break;
                                }
                            case "AllowVehicleFlags":
                                {
                                    c.allowVehicleFlags = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                            case "AllowLite":
                                {
                                    c.allowLite = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                            case "LargeMicro":
                                {
                                    c.largeMicro = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                            case "V3_5Production":
                                {
                                    try
                                    {
                                        c.v3_5Production = (rec.Attribute("value").Value == "1") ? true : false;                                        
                                    }
                                    catch (Exception er1)
                                    {
                                        c.v3_5Production = false;
                                    }
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }

                        

                    }
                    if (c.v3_5Production == true)
                    {
                        configs.Add(c);
                    }
                }
            }
            return configs;
        }

        public static List<customerToConfigV2> getCustomerToConfigs()
        {
            if (custToConfigs == null)
            {
                XElement root = getData("RTUProductionCustomerToConfigV2s");
                XName x = XName.Get("customertoconfigv2", amrNameSpace);

                custToConfigs = new List<customerToConfigV2>();

                foreach (var conf in root.Descendants(x))
                {
                    customerToConfigV2 c = new customerToConfigV2();
                    c.id = int.Parse(conf.Attribute("key").Value);

                    foreach (var rec in conf.Descendants(XName.Get("productionvalue", amrNameSpace)))
                    {
                        switch (rec.Attribute("interpretation").Value)
                        {
                            case "CustomerId":
                                {
                                    c.fkCustomerID  = int.Parse(rec.Attribute("value").Value);
                                    break;
                                }
                            case "ConfigV2Id":
                                {
                                    c.fkConfigV2 = int.Parse(rec.Attribute("value").Value);
                                    break;
                                }
                            
                        }



                    }
                    custToConfigs.Add(c);
                }
            }
            return custToConfigs;
        }

        public static List<firmwareVariantMapping> getFwToVariantMapping()
        {
            if (fwToVariantMapping == null)
            {
                XElement root = getData("RTUProductionFirmwareVariantMappings");
                XName x = XName.Get("firmwarevariantmapping", amrNameSpace);

                fwToVariantMapping = new List<firmwareVariantMapping>();

                foreach (var conf in root.Descendants(x))
                {
                    firmwareVariantMapping c = new firmwareVariantMapping();
                    c.id = int.Parse(conf.Attribute("key").Value);

                    foreach (var rec in conf.Descendants(XName.Get("productionvalue", amrNameSpace)))
                    {
                        switch (rec.Attribute("interpretation").Value)
                        {
                            case "Description":
                                {
                                    c.description = rec.Attribute("value").Value;
                                    break;
                                }
                            case "VariantIdList":
                                {
                                    c.varIDList = rec.Attribute("value").Value;
                                    break;
                                }
                        }
                    }
                    fwToVariantMapping.Add(c);
                }
            }
            return fwToVariantMapping;
        }

        public static List<simDetails> getSims()
        {
            if (sims == null)
            {
                try
                {
                    XElement root = getData("RTUProductionSIMLists");
                    XName x = XName.Get("simlist", amrNameSpace);

                    sims = new List<simDetails>();

                    foreach (var conf in root.Descendants(x))
                    {
                        simDetails c = new simDetails();
                        c.id = int.Parse(conf.Attribute("key").Value);

                        foreach (var rec in conf.Descendants(XName.Get("productionvalue", amrNameSpace)))
                        {
                            switch (rec.Attribute("interpretation").Value)
                            {
                                case "Description":
                                    {
                                        c.description = rec.Attribute("value").Value;
                                        break;
                                    }
                                case "TypeCode":
                                    {
                                        c.typeCode = int.Parse(rec.Attribute("value").Value);
                                        break;
                                    }
                                case "Enable":
                                    {
                                        c.enable = (rec.Attribute("value").Value == "1") ? true : false;
                                        break;
                                    }
                                case "APN":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.apn = rec.Attribute("value").Value;
                                        }                                        
                                        break;
                                    }
                                case "User":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.user = rec.Attribute("value").Value;
                                        }
                                        break;
                                    }
                                case "Pass":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.pass = rec.Attribute("value").Value;
                                        }
                                        break;
                                    }
                                case "PhoneNumberLength":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.phoneNumLen = int.Parse(rec.Attribute("value").Value);
                                        }
                                        break;
                                    }
                                case "SIMNumberLength":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.simNumLen = int.Parse(rec.Attribute("value").Value);
                                        }
                                        break;
                                    }
                                case "ManualPhoneNumber":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.manualPhoneNum = (rec.Attribute("value").Value == "1") ? true : false;
                                        }
                                        break;
                                    }
                                case "SendTestSMS":
                                    {
                                        if (rec.Attribute("value") != null)
                                        {
                                            c.sendTestSms = (rec.Attribute("value").Value == "1") ? true : false;
                                        }
                                        break;
                                    }
                            }

                        }
                        sims.Add(c);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
            return sims;
        }

        public static List<variantID> getVariantIdList()
        {
            if (variantIdList == null)
            {
                XElement root = getData("RTUProductionVariantIDs");
                XName x = XName.Get("variantid", amrNameSpace);

                variantIdList = new List<variantID>();

                foreach (var conf in root.Descendants(x))
                {
                    variantID c = new variantID();
                    c.id = int.Parse(conf.Attribute("key").Value);

                    foreach (var rec in conf.Descendants(XName.Get("productionvalue", amrNameSpace)))
                    {
                        switch (rec.Attribute("interpretation").Value)
                        {
                            case "Description":
                                {
                                    c.description = rec.Attribute("value").Value;
                                    break;
                                }
                            case "VariantId":
                                {
                                    c.varID = int.Parse(rec.Attribute("value").Value);
                                    break;
                                }
                            case "Enabled":
                                {
                                    c.enabled = (rec.Attribute("value").Value == "1") ? true : false;
                                    break;
                                }
                        }
                    }
                    variantIdList.Add(c);
                }
            }
            return variantIdList;
        }

        public static void processFailedInsertProductionData(string path, System.Windows.Forms.ProgressBar pb)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] fi = di.GetFiles("*.xml");
            pb.Minimum = 0;
            pb.Maximum = fi.Length;
            int errCnt = 0;
            for (int i = 0; i < fi.Length; i++)
            {
                
                for (int y = 0; y < 2; y++)
                {
                    if (insertProductionData(fi[i].FullName) == true)
                    {
                        break;
                    }
                    else
                    {
                        errCnt++;
                        System.Threading.Thread.Sleep(3000);
                    }
                }
                pb.Value = i;
                if (errCnt > 4) break;
            }

        }
        public static bool insertProductionData(string path)
        {
            bool retVal = false;
            string temp = "";

            try
            {

                temp = File.ReadAllText(path);
                temp += "   ";

                var servicePoint = ServicePointManager.FindServicePoint(new Uri(amlServer));
                servicePoint.Expect100Continue = false;
                var client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                string responsedata = client.UploadString(amlProductionTestInsert, temp);
                System.Diagnostics.Debug.WriteLine(responsedata);

                XElement root = XElement.Parse(responsedata);

                foreach (var rec in root.Descendants(XName.Get("productiontestrecord", amlNameSpace)))
                {
                    //foreach (var r in rec.Attributes())
                    //{
                    //    System.Diagnostics.Debug.WriteLine(r.Name );
                    //}

                    switch (rec.Attribute(XName.Get("interpretation", amlNameSpace)).Value)
                    {
                        case "InsertedRecord":
                            {
                                retVal = true;
                                break;
                            }
                    }
                }

            }
            catch (Exception e)
            {
                retVal = false;
            }
            finally
            {
                if (retVal == true)
                {
                    File.Delete(path);
                }
            }
            return retVal;
        }

        public static bool insertProductionData(string IMEI, string IMSI, string testDate, string variant, string phoneNumber, string simNumber, string batteryAD, string variantConfig, string connectionTariffTypeCode, string boxID)
        {
            bool retVal = false;
            string temp = "";

            try
            {

                temp = File.ReadAllText("productionTest.xml");

                temp = temp.Replace("[unt_ime]", IMEI);
                temp = temp.Replace("[tst_tme]", testDate);
                temp = temp.Replace("[tst_var]", variant);
                temp = temp.Replace("[tel_num]", phoneNumber);
                temp = temp.Replace("[sim_num]", simNumber);
                temp = temp.Replace("[sim_ims]", IMSI);
                temp = temp.Replace("[sim_tme]", "");
                temp = temp.Replace("[bat_adr]", batteryAD.ToString());
                temp = temp.Replace("[cfg_var]", variantConfig.ToString());
                temp = temp.Replace("[con_ttc]", connectionTariffTypeCode.ToString());
                temp = temp.Replace("[box_ref]", boxID.ToString());


                var servicePoint = ServicePointManager.FindServicePoint(new Uri(amlServer));
                servicePoint.Expect100Continue = false;
                var client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                string responsedata = client.UploadString(amlProductionTestInsert, temp);


                XElement root = XElement.Parse(responsedata);

                foreach (var rec in root.Descendants(XName.Get("productiontestrecord", amlNameSpace)))
                {
                    //foreach (var r in rec.Attributes())
                    //{
                    //    System.Diagnostics.Debug.WriteLine(r.Name );
                    //}

                    switch (rec.Attribute(XName.Get("interpretation", amlNameSpace)).Value)
                    {
                        case "InsertedRecord":
                            {
                                retVal = true;
                                break;
                            }
                    }
                }

            }
            catch (Exception e)
            {
                retVal = false;
            }
            finally
            {
                if (retVal == false)
                {
                    File.WriteAllText(System.Windows.Forms.Application.StartupPath + @"\failedInsertProductionData\" + IMEI + "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmss") + ".xml", temp);
                }
            }
            return retVal;
        }

        

        public static bool insertProductionTestData(Dictionary<string,string> data)
        {
            bool retVal = false;
            string temp = "";

            try
            {

                temp = File.ReadAllText("testResults.xml");

                temp = temp.Replace("[unt_ime]", data["imei"]);
                temp = temp.Replace("[tst_pss]", data["pass"]);
                temp = temp.Replace("[tst_tsp]", data["timeStamp"]);
                temp = temp.Replace("[tst_tme]", data["testTime"]);
                temp = temp.Replace("[tst_seq]", data["testSequence"]);
                temp = temp.Replace("[tst_fln]", data["testSequenceFileName"]);
                temp = temp.Replace("[tst_fwv]", data["prodTestFwVersion"]);
                temp = temp.Replace("[cfg_fln]", data["configFilename"]);
                temp = temp.Replace("[prc_ver]", data["processorVersion"]);
                temp = temp.Replace("[cur_dra]", data["currentDrawA"]);
                
                temp = temp.Replace("[bat_swu]", data["batterySwitchPointUp"]);
                temp = temp.Replace("[bat_swd]", data["batterySwitchPointdown"]);
                temp = temp.Replace("[adb_low]", data["adBatteryLow"]);
                temp = temp.Replace("[adb_mid]", data["adBatteryMid"]);
                temp = temp.Replace("[adb_hgh]", data["adBatteryHigh"]);
                temp = temp.Replace("[sim_num]", data["simNumber"]);
                temp = temp.Replace("[sim_ims]", data["imsi"]);
                temp = temp.Replace("[sim_tcd]", data["simTypeCode"]);
                temp = temp.Replace("[mdm_typ]", data["modemType"]);
                temp = temp.Replace("[mdm_fwv]", data["modemFirmwareVersion"]);
                temp = temp.Replace("[tel_num]", data["phoneNumber"]);
                
                temp = temp.Replace("[gps_sts]", data["gpsStatus"]);
                temp = temp.Replace("[gps_vlt]", data["gpsVoltage"]);
                temp = temp.Replace("[ign_cnd]", data["IgnitionConnected"]);
                temp = temp.Replace("[ign_dcn]", data["IgnitionDisconnected"]);
                temp = temp.Replace("[tst_xax]", data["xAxis"]);
                temp = temp.Replace("[tst_yax]", data["yAxis"]);
                temp = temp.Replace("[tst_zax]", data["zAxis"]);
                temp = temp.Replace("[aux_pws]", data["auxpowerSupply"]);
                temp = temp.Replace("[kln_vnp]", data["kLineVoltageNoPullup"]);
                temp = temp.Replace("[kln_vwp]", data["kLineVoltageWithPullup"]);
                temp = temp.Replace("[iop_ofv]", data["ioOffVoltage"]);
               
                temp = temp.Replace("[iop_onv]", data["ioOnVoltage"]);
                temp = temp.Replace("[iop_cfg]", data["ioConfig"]);
                temp = temp.Replace("[gps_nms]", data["gpsNumSats"]);
                temp = temp.Replace("[gps_snr]", data["gpsSnr"]);
                temp = temp.Replace("[ict_ref]", data["ictPcbId"]);
                temp = temp.Replace("[ict_psf]", data["ictPcbPassFail"]);
                temp = temp.Replace("[ict_flc]", data["ictPcbFailCount"]);
                temp = temp.Replace("[ict_tsd]", data["ictPcbTestDate"]);
                temp = temp.Replace("[app_pth]", data["appCodePath"]);
                temp = temp.Replace("[app_plc]", data["appCodePathLoopCount"]);
                temp = temp.Replace("[jig_ref]", data["jigIdent"]);
       
                temp = temp.Replace("[ate_psf]", data["atePassFail"]);
                temp = temp.Replace("[ate_flc]", data["ateFailCount"]);
                temp = temp.Replace("[ate_tsd]", data["ateTestDate"]);
                temp = temp.Replace("[rts_ime]", data["retestScanImei"]);
                temp = temp.Replace("[slp_cur]", data["sleepCurrent"]);
                temp = temp.Replace("[pcb_var]", data["pcbVariant1"]);
                temp = temp.Replace("[veh_var]", data["vehicleVariant"]);
                temp = temp.Replace("[hwr_var]", data["hardwareVariant"]);
                temp = temp.Replace("[box_ref]", data["boxID"]);
                temp = temp.Replace("[gyr_ref]", data["gyroID"]);
                temp = temp.Replace("[gyr_xax]", data["gyroX"]);
                temp = temp.Replace("[gyr_yax]", data["gyroY"]);
                temp = temp.Replace("[gyr_zax]", data["gyroZ"]);
                temp = temp.Replace("[veh_flg]", data["vehicleFlags"]);
                temp = temp.Replace("[tst_log]", data["testLog"]);

                
               

                var servicePoint = ServicePointManager.FindServicePoint(new Uri(amlServer));
                servicePoint.Expect100Continue = false;
                var client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                string responsedata = client.UploadString(amlRtuProductionTestInsert, temp);


                XElement root = XElement.Parse(responsedata);

                foreach (var rec in root.Descendants(XName.Get("rtuproductiontestrecord", amlNameSpace)))
                {                    
                    switch (rec.Attribute(XName.Get("interpretation",amlNameSpace)).Value)
                    {
                        case "InsertedRecord":
                            {
                                retVal = true;
                                break;
                            }
                    }
                }

            }
            catch (Exception e)
            {
                retVal = false;
            }
            finally
            {
                if (retVal == false)
                {
                    File.WriteAllText(System.Windows.Forms.Application.StartupPath + @"\failedInsertProductionTestResuls\" + data["boxID"] + "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmss") + ".xml", temp);
                }
            }
            return retVal;
        }

        public static void processFailedInsertProductionTestData(string path, System.Windows.Forms.ProgressBar pb)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] fi = di.GetFiles("*.xml");
            int errCnt = 0;
            pb.Minimum = 0;
            pb.Maximum = fi.Length;
            for (int i = 0; i < fi.Length; i++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (insertProductionTestData(fi[i].FullName) == true)
                    {
                        break;
                    }
                    else
                    {
                        errCnt++;
                    }
                }
                pb.Value = i;
                if (errCnt > 9) break;
            }
           
        }

        public static bool insertProductionTestData(string filePath)
        {
            bool retVal = false;
            string temp = "";
            string responsedata = "";

            try
            {
                if (filePath.Contains("-1_") == true)
                {
                    retVal = true;
                }
                else
                {
                    temp = File.ReadAllText(filePath);

                    var servicePoint = ServicePointManager.FindServicePoint(new Uri(amlServer));
                    servicePoint.Expect100Continue = false;
                    var client = new WebClient();
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    responsedata = client.UploadString(amlRtuProductionTestInsert, temp);

                    XElement root = XElement.Parse(responsedata);

                    foreach (var rec in root.Descendants(XName.Get("rtuproductiontestrecord", amlNameSpace)))
                    {
                        switch (rec.Attribute(XName.Get("interpretation", amlNameSpace)).Value)
                        {
                            case "InsertedRecord":
                                {
                                    retVal = true;
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                retVal = false;
            }
            finally
            {
                if (retVal == true)
                {
                    File.Delete(filePath);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(responsedata);
                }
            }
            return retVal;
        }

        public static bool smsReceived(string phoneNumber, string payload)
        {
            bool retval = false;
            XName transferitem = XName.Get("transferitem", amrNameSpace);
            XName x = XName.Get("transferitem", amrNameSpace);

            byte[] temp = ASCIIEncoding.ASCII.GetBytes(payload);
            string p = "";
            foreach (byte b in temp)
            {
                p += b.ToString("x2");
            }


            XElement root = null;
            try
            {
                string request = string.Format(amlIncomingSMS, phoneNumber);

                root = XElement.Load(request);

                foreach (var conf in root.Descendants(x))
                {
                    foreach (var rec in conf.Descendants(XName.Get("inboundsms", amrNameSpace)))
                    {
                        foreach (var val in conf.Descendants(XName.Get("hexstring", amrNameSpace)))
                        {
                            switch (val.Attribute("interpretation").Value)
                            {
                                case "PacketPayload":
                                    {
                                        if (val.Attribute("value").Value == p)
                                        {
                                            retval = true;
                                        }
                                        break;
                                    }
                            }
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retval = false;
            }
            return retval;
        }
        
        public static bool udpReceived(string imei, string payload)
        {
            bool retval = false;
            XName transferitem = XName.Get("transferitem", amrNameSpace);
            XName x = XName.Get("transferitem", amrNameSpace);

            byte[] temp = ASCIIEncoding.ASCII.GetBytes(payload);
            string p = "";
            foreach (byte b in temp)
            {
                p += b.ToString("x2");
            }


            XElement root = null;
            try
            {
                //"357803040648483"
                //"357803045355258"
                string request = string.Format(amlUdpReceived, imei);

                root = XElement.Load(request);

                foreach (var conf in root.Descendants(x))
                {
                    foreach (var pr in conf.Descendants(XName.Get("productionrecord", amrNameSpace)))
                    {
                        foreach (var rec in pr.Descendants(XName.Get("InboundUDP", amrNameSpace)))
                        {
                            foreach (var val in rec.Descendants(XName.Get("hexstring", amrNameSpace)))
                            {
                                switch (val.Attribute("interpretation").Value)
                                {
                                    case "PacketPayload":
                                        {
                                            if (val.Attribute("value").Value.Contains(payload + "0A0D"))
                                            {
                                                retval = true;
                                            }
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retval = false;
            }
            return retval;
        }

        public static string getJerseyPhoneNumber(string imsi)
        {
            string retval = "";
            XName transferitem = XName.Get("transferitem", amrNameSpace);
            XName x = XName.Get("transferitem", amrNameSpace);

            XElement root = null;
            try
            {
                string request = string.Format(amlJerseyMsisdn, imsi);

                root = XElement.Load(request);

                foreach (var conf in root.Descendants(x))
                {
                    foreach (var rec in conf.Descendants(XName.Get("JerseyTelecomSIM", amrNameSpace)))
                    {
                        foreach (var val in conf.Descendants(XName.Get("sim", amrNameSpace)))
                        {
                            switch (val.Attribute("interpretation").Value)
                            {
                                case "JerseyTelecomSIM":
                                    {
                                        retval = val.Attribute("msisdn").Value;
                                        break;
                                    }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                retval = "";
            }
            if (retval.StartsWith("44")) retval = "0" + retval.Substring(2);
            return retval;
        }
    
    }
}
