﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace V3ProductionTest
{
    public class rikServer
    {
        private string _ipAddress = "";
        private int _port = 500;

        private string serverTime = "TestMachineGetCurrentTime";
        private string confirmPhonenumber = "TestMachineConfirmPhoneNum";
        private string confirmPhonenumberAndPayload = "TestMachineConfirmPhoneNumAndPayload";
        private string confirmPhonenumberAndPayloadContains = "TestMachineConfirmPhoneNumAndPayloadContains";

        public rikServer(string ipAddress, int port)
        {
            _ipAddress = ipAddress;
            _port = port;
        }

        public string getServerTime()
        {
            string retVal = "";
            byte[] data;
            TcpClient tc = new TcpClient();
            NetworkStream ns;
            try
            {
                 
                tc.Connect(IPAddress.Parse(_ipAddress), _port);

                data = System.Text.Encoding.ASCII.GetBytes(serverTime);

                ns = tc.GetStream();

                ns.ReadTimeout = 10000;

                ns.Write(data, 0, data.Length);

                data = new byte[256];

                string temp = "";
                int bytesReceived = 0;

                do
                {
                    bytesReceived = ns.Read(data, 0, 256);
                    temp += System.Text.Encoding.ASCII.GetString(data, 0, bytesReceived);

                } while (ns.DataAvailable);

                ns.Close();
                tc.Close();

                retVal = temp;


            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            finally
            {
                
            }


            return retVal;
        }

        public string confirmPhoneNumber(string phoneNumber)
        {
            string retVal = "";
            byte[] data;

            try
            {
                TcpClient tc = new TcpClient();
                tc.Connect(IPAddress.Parse(_ipAddress), _port);

                data = System.Text.Encoding.ASCII.GetBytes(confirmPhonenumber + ":" + phoneNumber);

                NetworkStream ns = tc.GetStream();

                ns.Write(data, 0, data.Length);

                data = new byte[256];

                string temp = "";
                int bytesReceived = 0;

                do
                {
                    bytesReceived = ns.Read(data, 0, 256);
                    temp += System.Text.Encoding.ASCII.GetString(data, 0, bytesReceived);

                } while (ns.DataAvailable);

                ns.Close();
                tc.Close();

                retVal = temp;


            }
            catch (Exception e)
            { }

            return retVal;
        }

        public string confirmPhoneNumberAndPayload(string phoneNumber, string payload)
        {
            string retVal = "";
            byte[] data;

            try
            {
                TcpClient tc = new TcpClient();
                tc.Connect(IPAddress.Parse(_ipAddress), _port);

                data = System.Text.Encoding.ASCII.GetBytes(confirmPhonenumberAndPayload + ":" + phoneNumber + ":" + payload);

                NetworkStream ns = tc.GetStream();

                ns.Write(data, 0, data.Length);

                data = new byte[256];

                string temp = "";
                int bytesReceived = 0;

                do
                {
                    bytesReceived = ns.Read(data, 0, 256);
                    temp += System.Text.Encoding.ASCII.GetString(data, 0, bytesReceived);

                } while (ns.DataAvailable);

                ns.Close();
                tc.Close();

                retVal = temp;


            }
            catch (Exception e)
            { }

            return retVal;
        }
        public string confirmPhoneNumberAndContainsPayload(string phoneNumber, string payload)
        {
            string retVal = "";
            byte[] data;

            try
            {
                TcpClient tc = new TcpClient();
                tc.Connect(IPAddress.Parse(_ipAddress), _port);

                data = System.Text.Encoding.ASCII.GetBytes(confirmPhonenumberAndPayloadContains + ":" + phoneNumber + ":" + payload);

                NetworkStream ns = tc.GetStream();

                ns.Write(data, 0, data.Length);

                data = new byte[256];

                string temp = "";
                int bytesReceived = 0;

                do
                {
                    bytesReceived = ns.Read(data, 0, 256);
                    temp += System.Text.Encoding.ASCII.GetString(data, 0, bytesReceived);

                } while (ns.DataAvailable);

                ns.Close();
                tc.Close();

                retVal = temp;


            }
            catch (Exception e)
            { }

            return retVal;
        }
    }
}
